{
  'includes': [
    'common.gypi',
  ],

  'variables': {
    'local_root': '<(platform_root)/frameworks/native/services/surfaceflinger',
  },

  'targets': [
    {
      'target_name': 'frameworks-surfaceflinger',
      'product_name': 'surfaceflinger',
      'type': 'static_library',

      'dependencies': [
        '<!(<(dependency) hardware-libhardware)',
        '<!(<(dependency) frameworks-include)',
        '<!(<(dependency) frameworks-opengl)',
        '<!(<(dependency) frameworks-opengl-egl)',
        '<!(<(dependency) frameworks-binder)',
        '<!(<(dependency) frameworks-ui)',
        '<!(<(dependency) frameworks-gui)',
        '<!(<(dependency) system-liblog)',
        '<!(<(dependency) system-libutils)',
        '<!(<(dependency) system-libcutils)',
      ],

      'defines': [
        'LOG_TAG="SurfaceFlinger"',
        'GL_GLEXT_PROTOTYPES',
        'EGL_EGLEXT_PROTOTYPES',
        'TARGET_DISABLE_TRIPLE_BUFFERING',
        'NUM_FRAMEBUFFER_SURFACE_BUFFERS=16',
        'RUNNING_WITHOUT_SYNC_FRAMEWORK',
        'VSYNC_EVENT_PHASE_OFFSET_NS=0',
        'SF_VSYNC_EVENT_PHASE_OFFSET_NS=0',
        'PRESENT_TIME_OFFSET_FROM_VSYNC_NS=0',
      ],

      'sources': [
        '<(local_root)/init_surfaceflinger.cpp',
        '<(local_root)/Client.cpp',
        '<(local_root)/DisplayDevice.cpp',
        '<(local_root)/DispSync.cpp',
        '<(local_root)/EventControlThread.cpp',
        '<(local_root)/EventThread.cpp',
        '<(local_root)/FrameTracker.cpp',
        '<(local_root)/Layer.cpp',
        '<(local_root)/LayerDim.cpp',
        '<(local_root)/MessageQueue.cpp',
        '<(local_root)/SurfaceFlinger.cpp',
        '<(local_root)/SurfaceFlingerConsumer.cpp',
        '<(local_root)/SurfaceTextureLayer.cpp',
        '<(local_root)/Transform.cpp',
        '<(local_root)/DisplayHardware/FramebufferSurface.cpp',
        '<(local_root)/DisplayHardware/HWComposer.cpp',
        '<(local_root)/DisplayHardware/PowerHAL.cpp',
        '<(local_root)/DisplayHardware/VirtualDisplaySurface.cpp',
        '<(local_root)/Effects/Daltonizer.cpp',
        '<(local_root)/EventLog/EventLogTags.logtags',
        '<(local_root)/EventLog/EventLog.cpp',
        '<(local_root)/RenderEngine/Description.cpp',
        '<(local_root)/RenderEngine/Mesh.cpp',
        '<(local_root)/RenderEngine/Program.cpp',
        '<(local_root)/RenderEngine/ProgramCache.cpp',
        '<(local_root)/RenderEngine/GLExtensions.cpp',
        '<(local_root)/RenderEngine/RenderEngine.cpp',
        '<(local_root)/RenderEngine/Texture.cpp',
        '<(local_root)/RenderEngine/GLES10RenderEngine.cpp',
        '<(local_root)/RenderEngine/GLES11RenderEngine.cpp',
        #'<(local_root)/RenderEngine/GLES20RenderEngine.cpp',
      ],
    },
  ],
}
