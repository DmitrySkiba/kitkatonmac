{
  'target_defaults': {
    'target_conditions': [
      [ '_type != "none"', {
        'mac_bundle': 1,
      }]
    ],

    'dependencies': [
      # Must be first to initialize String8
      '<!(<(dependency) system-libutils)',

      # Implicit dependencies
      '<!(<(dependency) host-gles)',
      '<!(<(dependency) opengl-rc-gles1)',
      '<!(<(dependency) opengl-rc-RenderControl)',
      '<!(<(dependency) opengl-rc-egl)',
      '<!(<(dependency) opengl-rc-gralloc)',
    ],

    'xcode_settings': {
      'OTHER_LDFLAGS': [
        #'-u ', '_<(host_gles_prefix)eglInitialize',
        # ^ this also works (and adds whole library where symbol is defined).
        # However, it requires cherry-picking all needed symbols, so for now we just
        # compile everything in:
        '-all_load',
      ],
    },

    'link_settings': {
      'libraries': [
        '$(SDKROOT)/System/Library/Frameworks/Cocoa.framework',
      ]
    },
  },
}
