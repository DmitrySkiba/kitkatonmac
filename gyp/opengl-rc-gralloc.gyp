{
  'includes': [
    'common.gypi',
  ],

  'variables': {
    'rc_root': '<(platform_root)/opengl/rc',
    'local_root': '<(rc_root)/gralloc',
  },

  'targets': [
    {
      'target_name': 'opengl-rc-gralloc',
      'product_name': 'rc-gralloc',
      'type': 'static_library',

      'dependencies': [
        '<!(<(dependency) system-libcutils)',
        '<!(<(dependency) system-liblog)',
        '<!(<(dependency) frameworks-opengl)',
        '<!(<(dependency) opengl-rc-shared)',
        '<!(<(dependency) opengl-rc-utils)',
        'hardware-libhardware.gyp:hardware-libhardware-common',
      ],

      'defines': [
        'LOG_TAG="gralloc"',
        'STATIC_HAL_MODULE_ID=gralloc', # Must match GRALLOC_HARDWARE_MODULE_ID
      ],

      'sources': [
        '<(local_root)/gralloc.cpp',
      ],
    },
  ],
}
