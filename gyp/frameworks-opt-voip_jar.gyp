{
  'includes': [
    'common.gypi',
    'java-common.gypi',
  ],

  'variables': {
    'local_root': '<(platform_root)/frameworks/opt/net/voip',
    'java_root': '<(local_root)/src/java',
  },

  'targets': [
    {
      'target_name': 'frameworks-opt-voip_jar',
      'type': 'none',

      'dependencies': [
        '<!(<(dependency) libcore-rt_jar)',
        '<!(<(dependency) frameworks-base-framework_jar)',
        '<!(<(dependency) external-ext_jar)',
      ],

      'variables': {
        'jar_path': '<!(<(jar_path_v) voip)',
        'dex_path': '<!(<(dex_path_v) voip)',

        #logtags
        'logtags.inputs': [ '<(java_root)' ],

        # aidl
        'aidl.inputs': [ '<(java_root)' ],
        'aidl.includes': [
          '<(java_root)',
          '>(framework_jar.aidl_include)',
        ],

        # compile_jar
        'compile_jar.source_paths': [
          '<(logtags.output_path)',
          '<(aidl.output_path)',
          '<(java_root)',
        ],
        'compile_jar.boot_class_paths': '>(dependencies.jar_files)',
        'compile_jar.jar_file': '<(jar_path)',
        'compile_jar.extra_action_inputs': [
          '<(logtags.stamp_file)',
          '<(aidl.stamp_file)',
        ],

        # dex
        'dex.jar_file': '<(jar_path)',
        'dex.dex_file': '<(dex_path)',
      },
      'includes': [
        'utils/logtags.gypi',
        'utils/aidl.gypi',
        'utils/compile_jar.gypi',
        'utils/dex.gypi'
      ],
    }
  ]
}
