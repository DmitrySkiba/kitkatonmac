{ #TODO: rename to frameworks-android-policy_jar
  'includes': [
    'common.gypi',
    'java-common.gypi',
  ],

  'variables': {
    'local_root': '<(platform_root)/frameworks/base/policy',
  },

  'targets': [
    {
      'target_name': 'frameworks-android.policy_jar',
      'type': 'none',

      'dependencies': [
        '<!(<(dependency) libcore-rt_jar)',
        '<!(<(dependency) frameworks-base-framework_jar)',
      ],

      'variables': {
        'jar_path': '<!(<(jar_path_v) android.policy)',
        'dex_path': '<!(<(dex_path_v) android.policy)',

        'compile_jar.source_paths': [
          '<(local_root)/src',
        ],
        'compile_jar.boot_class_paths': '>(dependencies.jar_files)',
        'compile_jar.jar_file': '<(jar_path)',

        'dex.jar_file': '<(jar_path)',
        'dex.dex_file': '<(dex_path)',
      },
      'includes': [ 'utils/compile_jar.gypi', 'utils/dex.gypi' ],
    }
  ]
}
