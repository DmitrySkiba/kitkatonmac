{
  'includes': [
    'common.gypi',
    'java-common.gypi',
    'testapps-common.gypi',
  ],

  'variables': {
    'main_target_name': 'testapps-HelloAndroid',

    'local_root': '<(platform_root)/testapps/HelloAndroid',
  },

  'targets': [
    {
      'target_name': '<(main_target_name)',
      'product_name': 'HelloAndroid',
      'type': 'executable',

      'dependencies': [
        '<!(<(dependency) boot_oat)',
        '<!(<(dependency) frameworks-surfaceflinger)',
        '<!(<(dependency) frameworks-sensorservice)',
        '<!(<(dependency) frameworks-base-include)',
        '<!(<(dependency) frameworks-base-core)',
        '<!(<(dependency) frameworks-include)',
        '<!(<(dependency) frameworks-input)',
        '<!(<(dependency) frameworks-opengl-egl)',
        '<!(<(dependency) libnativehelper)',
        '<!(<(dependency) art-compiler)',
        '<!(<(dependency) art-runtime)',
        '<!(<(dependency) art-run_java)',
        '<!(<(dependency) frameworks-base-services)',
      ],

      'xcode_settings': {
        'INFOPLIST_FILE': '<(local_root)/Info.plist',
      },

      'mac_bundle_resources': [
        '<(local_root)/Window.xib',
        '<(android_fs_root)',
      ],

      'sources': [
        '<(local_root)/main.m',
        '<(local_root)/AppDelegate.mm',
      ],
    },
  ],
}
