#include <GLES/gl.h>
#include <GLES/glext.h>

// f(ReturnType, Name, Arguments, ArgumentNames)
#define GLES1_ENUMERATE_FUNCTIONS(f) \
    f(void, glAlphaFunc, (GLenum func, GLclampf ref), (func, ref)) \
    f(void, glClearColor, (GLclampf red, GLclampf green, GLclampf blue, GLclampf alpha), (red, green, blue, alpha)) \
    f(void, glClearDepthf, (GLclampf depth), (depth)) \
    f(void, glClipPlanef, (GLenum plane, const GLfloat* equation), (plane, equation)) \
    f(void, glColor4f, (GLfloat red, GLfloat green, GLfloat blue, GLfloat alpha), (red, green, blue, alpha)) \
    f(void, glDepthRangef, (GLclampf zNear, GLclampf zFar), (zNear, zFar)) \
    f(void, glFogf, (GLenum pname, GLfloat param), (pname, param)) \
    f(void, glFogfv, (GLenum pname, const GLfloat* params), (pname, params)) \
    f(void, glFrustumf, (GLfloat left, GLfloat right, GLfloat bottom, GLfloat top, GLfloat zNear, GLfloat zFar), (left, right, bottom, top, zNear, zFar)) \
    f(void, glGetClipPlanef, (GLenum pname, GLfloat* eqn), (pname, eqn)) \
    f(void, glGetFloatv, (GLenum pname, GLfloat* params), (pname, params)) \
    f(void, glGetLightfv, (GLenum light, GLenum pname, GLfloat* params), (light, pname, params)) \
    f(void, glGetMaterialfv, (GLenum face, GLenum pname, GLfloat* params), (face, pname, params)) \
    f(void, glGetTexEnvfv, (GLenum env, GLenum pname, GLfloat* params), (env, pname, params)) \
    f(void, glGetTexParameterfv, (GLenum target, GLenum pname, GLfloat* params), (target, pname, params)) \
    f(void, glLightModelf, (GLenum pname, GLfloat param), (pname, param)) \
    f(void, glLightModelfv, (GLenum pname, const GLfloat* params), (pname, params)) \
    f(void, glLightf, (GLenum light, GLenum pname, GLfloat param), (light, pname, param)) \
    f(void, glLightfv, (GLenum light, GLenum pname, const GLfloat* params), (light, pname, params)) \
    f(void, glLineWidth, (GLfloat width), (width)) \
    f(void, glLoadMatrixf, (const GLfloat* m), (m)) \
    f(void, glMaterialf, (GLenum face, GLenum pname, GLfloat param), (face, pname, param)) \
    f(void, glMaterialfv, (GLenum face, GLenum pname, const GLfloat* params), (face, pname, params)) \
    f(void, glMultMatrixf, (const GLfloat* m), (m)) \
    f(void, glMultiTexCoord4f, (GLenum target, GLfloat s, GLfloat t, GLfloat r, GLfloat q), (target, s, t, r, q)) \
    f(void, glNormal3f, (GLfloat nx, GLfloat ny, GLfloat nz), (nx, ny, nz)) \
    f(void, glOrthof, (GLfloat left, GLfloat right, GLfloat bottom, GLfloat top, GLfloat zNear, GLfloat zFar), (left, right, bottom, top, zNear, zFar)) \
    f(void, glPointParameterf, (GLenum pname, GLfloat param), (pname, param)) \
    f(void, glPointParameterfv, (GLenum pname, const GLfloat* params), (pname, params)) \
    f(void, glPointSize, (GLfloat size), (size)) \
    f(void, glPolygonOffset, (GLfloat factor, GLfloat units), (factor, units)) \
    f(void, glRotatef, (GLfloat angle, GLfloat x, GLfloat y, GLfloat z), (angle, x, y, z)) \
    f(void, glScalef, (GLfloat x, GLfloat y, GLfloat z), (x, y, z)) \
    f(void, glTexEnvf, (GLenum target, GLenum pname, GLfloat param), (target, pname, param)) \
    f(void, glTexEnvfv, (GLenum target, GLenum pname, const GLfloat* params), (target, pname, params)) \
    f(void, glTexParameterf, (GLenum target, GLenum pname, GLfloat param), (target, pname, param)) \
    f(void, glTexParameterfv, (GLenum target, GLenum pname, const GLfloat* params), (target, pname, params)) \
    f(void, glTranslatef, (GLfloat x, GLfloat y, GLfloat z), (x, y, z)) \
    f(void, glActiveTexture, (GLenum texture), (texture)) \
    f(void, glAlphaFuncx, (GLenum func, GLclampx ref), (func, ref)) \
    f(void, glBindBuffer, (GLenum target, GLuint buffer), (target, buffer)) \
    f(void, glBindTexture, (GLenum target, GLuint texture), (target, texture)) \
    f(void, glBlendFunc, (GLenum sfactor, GLenum dfactor), (sfactor, dfactor)) \
    f(void, glBufferData, (GLenum target, GLsizeiptr size, const GLvoid* data, GLenum usage), (target, size, data, usage)) \
    f(void, glBufferSubData, (GLenum target, GLintptr offset, GLsizeiptr size, const GLvoid* data), (target, offset, size, data)) \
    f(void, glClear, (GLbitfield mask), (mask)) \
    f(void, glClearColorx, (GLclampx red, GLclampx green, GLclampx blue, GLclampx alpha), (red, green, blue, alpha)) \
    f(void, glClearDepthx, (GLclampx depth), (depth)) \
    f(void, glClearStencil, (GLint s), (s)) \
    f(void, glClientActiveTexture, (GLenum texture), (texture)) \
    f(void, glColor4ub, (GLubyte red, GLubyte green, GLubyte blue, GLubyte alpha), (red, green, blue, alpha)) \
    f(void, glColor4x, (GLfixed red, GLfixed green, GLfixed blue, GLfixed alpha), (red, green, blue, alpha)) \
    f(void, glColorMask, (GLboolean red, GLboolean green, GLboolean blue, GLboolean alpha), (red, green, blue, alpha)) \
    f(void, glColorPointer, (GLint size, GLenum type, GLsizei stride, const GLvoid* pointer), (size, type, stride, pointer)) \
    f(void, glCompressedTexImage2D, (GLenum target, GLint level, GLenum internalformat, GLsizei width, GLsizei height, GLint border, GLsizei imageSize, const GLvoid* data), (target, level, internalformat, width, height, border, imageSize, data)) \
    f(void, glCompressedTexSubImage2D, (GLenum target, GLint level, GLint xoffset, GLint yoffset, GLsizei width, GLsizei height, GLenum format, GLsizei imageSize, const GLvoid* data), (target, level, xoffset, yoffset, width, height, format, imageSize, data)) \
    f(void, glCopyTexImage2D, (GLenum target, GLint level, GLenum internalformat, GLint x, GLint y, GLsizei width, GLsizei height, GLint border), (target, level, internalformat, x, y, width, height, border)) \
    f(void, glCopyTexSubImage2D, (GLenum target, GLint level, GLint xoffset, GLint yoffset, GLint x, GLint y, GLsizei width, GLsizei height), (target, level, xoffset, yoffset, x, y, width, height)) \
    f(void, glCullFace, (GLenum mode), (mode)) \
    f(void, glDeleteBuffers, (GLsizei n, const GLuint* buffers), (n, buffers)) \
    f(void, glDeleteTextures, (GLsizei n, const GLuint* textures), (n, textures)) \
    f(void, glDepthFunc, (GLenum func), (func)) \
    f(void, glDepthMask, (GLboolean flag), (flag)) \
    f(void, glDepthRangex, (GLclampx zNear, GLclampx zFar), (zNear, zFar)) \
    f(void, glDisable, (GLenum cap), (cap)) \
    f(void, glDisableClientState, (GLenum array), (array)) \
    f(void, glDrawArrays, (GLenum mode, GLint first, GLsizei count), (mode, first, count)) \
    f(void, glDrawElements, (GLenum mode, GLsizei count, GLenum type, const GLvoid* indices), (mode, count, type, indices)) \
    f(void, glEnable, (GLenum cap), (cap)) \
    f(void, glEnableClientState, (GLenum array), (array)) \
    f(void, glFinish, (), ()) \
    f(void, glFlush, (), ()) \
    f(void, glFogx, (GLenum pname, GLfixed param), (pname, param)) \
    f(void, glFogxv, (GLenum pname, const GLfixed* params), (pname, params)) \
    f(void, glFrontFace, (GLenum mode), (mode)) \
    f(void, glFrustumx, (GLfixed left, GLfixed right, GLfixed bottom, GLfixed top, GLfixed zNear, GLfixed zFar), (left, right, bottom, top, zNear, zFar)) \
    f(void, glGetBooleanv, (GLenum pname, GLboolean* params), (pname, params)) \
    f(void, glGetBufferParameteriv, (GLenum target, GLenum pname, GLint* params), (target, pname, params)) \
    f(void, glClipPlanex, (GLenum pname, const GLfixed* eqn), (pname, eqn)) \
    f(void, glGenBuffers, (GLsizei n, GLuint* buffers), (n, buffers)) \
    f(void, glGenTextures, (GLsizei n, GLuint* textures), (n, textures)) \
    f(GLenum, glGetError, (), ()) \
    f(void, glGetFixedv, (GLenum pname, GLfixed* params), (pname, params)) \
    f(void, glGetIntegerv, (GLenum pname, GLint* params), (pname, params)) \
    f(void, glGetLightxv, (GLenum light, GLenum pname, GLfixed* params), (light, pname, params)) \
    f(void, glGetMaterialxv, (GLenum face, GLenum pname, GLfixed* params), (face, pname, params)) \
    f(void, glGetPointerv, (GLenum pname, GLvoid** params), (pname, params)) \
    f(const GLubyte*, glGetString, (GLenum name), (name)) \
    f(void, glGetTexEnviv, (GLenum env, GLenum pname, GLint* params), (env, pname, params)) \
    f(void, glGetTexEnvxv, (GLenum env, GLenum pname, GLfixed* params), (env, pname, params)) \
    f(void, glGetTexParameteriv, (GLenum target, GLenum pname, GLint* params), (target, pname, params)) \
    f(void, glGetTexParameterxv, (GLenum target, GLenum pname, GLfixed* params), (target, pname, params)) \
    f(void, glHint, (GLenum target, GLenum mode), (target, mode)) \
    f(GLboolean, glIsBuffer, (GLuint buffer), (buffer)) \
    f(GLboolean, glIsEnabled, (GLenum cap), (cap)) \
    f(GLboolean, glIsTexture, (GLuint texture), (texture)) \
    f(void, glLightModelx, (GLenum pname, GLfixed param), (pname, param)) \
    f(void, glLightModelxv, (GLenum pname, const GLfixed* params), (pname, params)) \
    f(void, glLightx, (GLenum light, GLenum pname, GLfixed param), (light, pname, param)) \
    f(void, glLightxv, (GLenum light, GLenum pname, const GLfixed* params), (light, pname, params)) \
    f(void, glLineWidthx, (GLfixed width), (width)) \
    f(void, glLoadIdentity, (), ()) \
    f(void, glLoadMatrixx, (const GLfixed* m), (m)) \
    f(void, glLogicOp, (GLenum opcode), (opcode)) \
    f(void, glMaterialx, (GLenum face, GLenum pname, GLfixed param), (face, pname, param)) \
    f(void, glMaterialxv, (GLenum face, GLenum pname, const GLfixed* params), (face, pname, params)) \
    f(void, glMatrixMode, (GLenum mode), (mode)) \
    f(void, glMultMatrixx, (const GLfixed* m), (m)) \
    f(void, glMultiTexCoord4x, (GLenum target, GLfixed s, GLfixed t, GLfixed r, GLfixed q), (target, s, t, r, q)) \
    f(void, glNormal3x, (GLfixed nx, GLfixed ny, GLfixed nz), (nx, ny, nz)) \
    f(void, glNormalPointer, (GLenum type, GLsizei stride, const GLvoid* pointer), (type, stride, pointer)) \
    f(void, glOrthox, (GLfixed left, GLfixed right, GLfixed bottom, GLfixed top, GLfixed zNear, GLfixed zFar), (left, right, bottom, top, zNear, zFar)) \
    f(void, glPixelStorei, (GLenum pname, GLint param), (pname, param)) \
    f(void, glPointParameterx, (GLenum pname, GLfixed param), (pname, param)) \
    f(void, glPointParameterxv, (GLenum pname, const GLfixed* params), (pname, params)) \
    f(void, glPointSizex, (GLfixed size), (size)) \
    f(void, glPolygonOffsetx, (GLfixed factor, GLfixed units), (factor, units)) \
    f(void, glPopMatrix, (), ()) \
    f(void, glPushMatrix, (), ()) \
    f(void, glReadPixels, (GLint x, GLint y, GLsizei width, GLsizei height, GLenum format, GLenum type, GLvoid* pixels), (x, y, width, height, format, type, pixels)) \
    f(void, glRotatex, (GLfixed angle, GLfixed x, GLfixed y, GLfixed z), (angle, x, y, z)) \
    f(void, glSampleCoverage, (GLclampf value, GLboolean invert), (value, invert)) \
    f(void, glSampleCoveragex, (GLclampx value, GLboolean invert), (value, invert)) \
    f(void, glScalex, (GLfixed x, GLfixed y, GLfixed z), (x, y, z)) \
    f(void, glScissor, (GLint x, GLint y, GLsizei width, GLsizei height), (x, y, width, height)) \
    f(void, glShadeModel, (GLenum mode), (mode)) \
    f(void, glStencilFunc, (GLenum func, GLint ref, GLuint mask), (func, ref, mask)) \
    f(void, glStencilMask, (GLuint mask), (mask)) \
    f(void, glStencilOp, (GLenum fail, GLenum zfail, GLenum zpass), (fail, zfail, zpass)) \
    f(void, glTexCoordPointer, (GLint size, GLenum type, GLsizei stride, const GLvoid* pointer), (size, type, stride, pointer)) \
    f(void, glTexEnvi, (GLenum target, GLenum pname, GLint param), (target, pname, param)) \
    f(void, glTexEnvx, (GLenum target, GLenum pname, GLfixed param), (target, pname, param)) \
    f(void, glTexEnviv, (GLenum target, GLenum pname, const GLint* params), (target, pname, params)) \
    f(void, glTexEnvxv, (GLenum target, GLenum pname, const GLfixed* params), (target, pname, params)) \
    f(void, glTexImage2D, (GLenum target, GLint level, GLint internalformat, GLsizei width, GLsizei height, GLint border, GLenum format, GLenum type, const GLvoid* pixels), (target, level, internalformat, width, height, border, format, type, pixels)) \
    f(void, glTexParameteri, (GLenum target, GLenum pname, GLint param), (target, pname, param)) \
    f(void, glTexParameterx, (GLenum target, GLenum pname, GLfixed param), (target, pname, param)) \
    f(void, glTexParameteriv, (GLenum target, GLenum pname, const GLint* params), (target, pname, params)) \
    f(void, glTexParameterxv, (GLenum target, GLenum pname, const GLfixed* params), (target, pname, params)) \
    f(void, glTexSubImage2D, (GLenum target, GLint level, GLint xoffset, GLint yoffset, GLsizei width, GLsizei height, GLenum format, GLenum type, const GLvoid* pixels), (target, level, xoffset, yoffset, width, height, format, type, pixels)) \
    f(void, glTranslatex, (GLfixed x, GLfixed y, GLfixed z), (x, y, z)) \
    f(void, glVertexPointer, (GLint size, GLenum type, GLsizei stride, const GLvoid* pointer), (size, type, stride, pointer)) \
    f(void, glViewport, (GLint x, GLint y, GLsizei width, GLsizei height), (x, y, width, height)) \
    f(void, glPointSizePointerOES, (GLenum type, GLsizei stride, const GLvoid* pointer), (type, stride, pointer)) \
    f(void, glVertexPointerOffset, (GLint size, GLenum type, GLsizei stride, GLuint offset), (size, type, stride, offset)) \
    f(void, glColorPointerOffset, (GLint size, GLenum type, GLsizei stride, GLuint offset), (size, type, stride, offset)) \
    f(void, glNormalPointerOffset, (GLenum type, GLsizei stride, GLuint offset), (type, stride, offset)) \
    f(void, glPointSizePointerOffset, (GLenum type, GLsizei stride, GLuint offset), (type, stride, offset)) \
    f(void, glTexCoordPointerOffset, (GLint size, GLenum type, GLsizei stride, GLuint offset), (size, type, stride, offset)) \
    f(void, glWeightPointerOffset, (GLint size, GLenum type, GLsizei stride, GLuint offset), (size, type, stride, offset)) \
    f(void, glMatrixIndexPointerOffset, (GLint size, GLenum type, GLsizei stride, GLuint offset), (size, type, stride, offset)) \
    f(void, glVertexPointerData, (GLint size, GLenum type, GLsizei stride, void* data, GLuint datalen), (size, type, stride, data, datalen)) \
    f(void, glColorPointerData, (GLint size, GLenum type, GLsizei stride, void* data, GLuint datalen), (size, type, stride, data, datalen)) \
    f(void, glNormalPointerData, (GLenum type, GLsizei stride, void* data, GLuint datalen), (type, stride, data, datalen)) \
    f(void, glTexCoordPointerData, (GLint unit, GLint size, GLenum type, GLsizei stride, void* data, GLuint datalen), (unit, size, type, stride, data, datalen)) \
    f(void, glPointSizePointerData, (GLenum type, GLsizei stride, void* data, GLuint datalen), (type, stride, data, datalen)) \
    f(void, glWeightPointerData, (GLint size, GLenum type, GLsizei stride, void* data, GLuint datalen), (size, type, stride, data, datalen)) \
    f(void, glMatrixIndexPointerData, (GLint size, GLenum type, GLsizei stride, void* data, GLuint datalen), (size, type, stride, data, datalen)) \
    f(void, glDrawElementsOffset, (GLenum mode, GLsizei count, GLenum type, GLuint offset), (mode, count, type, offset)) \
    f(void, glDrawElementsData, (GLenum mode, GLsizei count, GLenum type, void* data, GLuint datalen), (mode, count, type, data, datalen)) \
    f(void, glGetCompressedTextureFormats, (int count, GLint* formats), (count, formats)) \
    f(int, glFinishRoundTrip, (), ()) \
    f(void, glBlendEquationSeparateOES, (GLenum modeRGB, GLenum modeAlpha), (modeRGB, modeAlpha)) \
    f(void, glBlendFuncSeparateOES, (GLenum srcRGB, GLenum dstRGB, GLenum srcAlpha, GLenum dstAlpha), (srcRGB, dstRGB, srcAlpha, dstAlpha)) \
    f(void, glBlendEquationOES, (GLenum mode), (mode)) \
    f(void, glDrawTexsOES, (GLshort x, GLshort y, GLshort z, GLshort width, GLshort height), (x, y, z, width, height)) \
    f(void, glDrawTexiOES, (GLint x, GLint y, GLint z, GLint width, GLint height), (x, y, z, width, height)) \
    f(void, glDrawTexxOES, (GLfixed x, GLfixed y, GLfixed z, GLfixed width, GLfixed height), (x, y, z, width, height)) \
    f(void, glDrawTexsvOES, (const GLshort* coords), (coords)) \
    f(void, glDrawTexivOES, (const GLint* coords), (coords)) \
    f(void, glDrawTexxvOES, (const GLfixed* coords), (coords)) \
    f(void, glDrawTexfOES, (GLfloat x, GLfloat y, GLfloat z, GLfloat width, GLfloat height), (x, y, z, width, height)) \
    f(void, glDrawTexfvOES, (const GLfloat* coords), (coords)) \
    f(void, glEGLImageTargetTexture2DOES, (GLenum target, GLeglImageOES image), (target, image)) \
    f(void, glEGLImageTargetRenderbufferStorageOES, (GLenum target, GLeglImageOES image), (target, image)) \
    f(void, glAlphaFuncxOES, (GLenum func, GLclampx ref), (func, ref)) \
    f(void, glClearColorxOES, (GLclampx red, GLclampx green, GLclampx blue, GLclampx alpha), (red, green, blue, alpha)) \
    f(void, glClearDepthxOES, (GLclampx depth), (depth)) \
    f(void, glClipPlanexOES, (GLenum plane, const GLfixed* equation), (plane, equation)) \
    f(void, glClipPlanexIMG, (GLenum plane, const GLfixed* equation), (plane, equation)) \
    f(void, glColor4xOES, (GLfixed red, GLfixed green, GLfixed blue, GLfixed alpha), (red, green, blue, alpha)) \
    f(void, glDepthRangexOES, (GLclampx zNear, GLclampx zFar), (zNear, zFar)) \
    f(void, glFogxOES, (GLenum pname, GLfixed param), (pname, param)) \
    f(void, glFogxvOES, (GLenum pname, const GLfixed* params), (pname, params)) \
    f(void, glFrustumxOES, (GLfixed left, GLfixed right, GLfixed bottom, GLfixed top, GLfixed zNear, GLfixed zFar), (left, right, bottom, top, zNear, zFar)) \
    f(void, glGetClipPlanexOES, (GLenum pname, GLfixed* eqn), (pname, eqn)) \
    f(void, glGetClipPlanex, (GLenum pname, GLfixed* eqn), (pname, eqn)) \
    f(void, glGetFixedvOES, (GLenum pname, GLfixed* params), (pname, params)) \
    f(void, glGetLightxvOES, (GLenum light, GLenum pname, GLfixed* params), (light, pname, params)) \
    f(void, glGetMaterialxvOES, (GLenum face, GLenum pname, GLfixed* params), (face, pname, params)) \
    f(void, glGetTexEnvxvOES, (GLenum env, GLenum pname, GLfixed* params), (env, pname, params)) \
    f(void, glGetTexParameterxvOES, (GLenum target, GLenum pname, GLfixed* params), (target, pname, params)) \
    f(void, glLightModelxOES, (GLenum pname, GLfixed param), (pname, param)) \
    f(void, glLightModelxvOES, (GLenum pname, const GLfixed* params), (pname, params)) \
    f(void, glLightxOES, (GLenum light, GLenum pname, GLfixed param), (light, pname, param)) \
    f(void, glLightxvOES, (GLenum light, GLenum pname, const GLfixed* params), (light, pname, params)) \
    f(void, glLineWidthxOES, (GLfixed width), (width)) \
    f(void, glLoadMatrixxOES, (const GLfixed* m), (m)) \
    f(void, glMaterialxOES, (GLenum face, GLenum pname, GLfixed param), (face, pname, param)) \
    f(void, glMaterialxvOES, (GLenum face, GLenum pname, const GLfixed* params), (face, pname, params)) \
    f(void, glMultMatrixxOES, (const GLfixed* m), (m)) \
    f(void, glMultiTexCoord4xOES, (GLenum target, GLfixed s, GLfixed t, GLfixed r, GLfixed q), (target, s, t, r, q)) \
    f(void, glNormal3xOES, (GLfixed nx, GLfixed ny, GLfixed nz), (nx, ny, nz)) \
    f(void, glOrthoxOES, (GLfixed left, GLfixed right, GLfixed bottom, GLfixed top, GLfixed zNear, GLfixed zFar), (left, right, bottom, top, zNear, zFar)) \
    f(void, glPointParameterxOES, (GLenum pname, GLfixed param), (pname, param)) \
    f(void, glPointParameterxvOES, (GLenum pname, const GLfixed* params), (pname, params)) \
    f(void, glPointSizexOES, (GLfixed size), (size)) \
    f(void, glPolygonOffsetxOES, (GLfixed factor, GLfixed units), (factor, units)) \
    f(void, glRotatexOES, (GLfixed angle, GLfixed x, GLfixed y, GLfixed z), (angle, x, y, z)) \
    f(void, glSampleCoveragexOES, (GLclampx value, GLboolean invert), (value, invert)) \
    f(void, glScalexOES, (GLfixed x, GLfixed y, GLfixed z), (x, y, z)) \
    f(void, glTexEnvxOES, (GLenum target, GLenum pname, GLfixed param), (target, pname, param)) \
    f(void, glTexEnvxvOES, (GLenum target, GLenum pname, const GLfixed* params), (target, pname, params)) \
    f(void, glTexParameterxOES, (GLenum target, GLenum pname, GLfixed param), (target, pname, param)) \
    f(void, glTexParameterxvOES, (GLenum target, GLenum pname, const GLfixed* params), (target, pname, params)) \
    f(void, glTranslatexOES, (GLfixed x, GLfixed y, GLfixed z), (x, y, z)) \
    f(GLboolean, glIsRenderbufferOES, (GLuint renderbuffer), (renderbuffer)) \
    f(void, glBindRenderbufferOES, (GLenum target, GLuint renderbuffer), (target, renderbuffer)) \
    f(void, glDeleteRenderbuffersOES, (GLsizei n, const GLuint* renderbuffers), (n, renderbuffers)) \
    f(void, glGenRenderbuffersOES, (GLsizei n, GLuint* renderbuffers), (n, renderbuffers)) \
    f(void, glRenderbufferStorageOES, (GLenum target, GLenum internalformat, GLsizei width, GLsizei height), (target, internalformat, width, height)) \
    f(void, glGetRenderbufferParameterivOES, (GLenum target, GLenum pname, GLint* params), (target, pname, params)) \
    f(GLboolean, glIsFramebufferOES, (GLuint framebuffer), (framebuffer)) \
    f(void, glBindFramebufferOES, (GLenum target, GLuint framebuffer), (target, framebuffer)) \
    f(void, glDeleteFramebuffersOES, (GLsizei n, const GLuint* framebuffers), (n, framebuffers)) \
    f(void, glGenFramebuffersOES, (GLsizei n, GLuint* framebuffers), (n, framebuffers)) \
    f(GLenum, glCheckFramebufferStatusOES, (GLenum target), (target)) \
    f(void, glFramebufferRenderbufferOES, (GLenum target, GLenum attachment, GLenum renderbuffertarget, GLuint renderbuffer), (target, attachment, renderbuffertarget, renderbuffer)) \
    f(void, glFramebufferTexture2DOES, (GLenum target, GLenum attachment, GLenum textarget, GLuint texture, GLint level), (target, attachment, textarget, texture, level)) \
    f(void, glGetFramebufferAttachmentParameterivOES, (GLenum target, GLenum attachment, GLenum pname, GLint* params), (target, attachment, pname, params)) \
    f(void, glGenerateMipmapOES, (GLenum target), (target)) \
    f(void*, glMapBufferOES, (GLenum target, GLenum access), (target, access)) \
    f(GLboolean, glUnmapBufferOES, (GLenum target), (target)) \
    f(void, glGetBufferPointervOES, (GLenum target, GLenum pname, GLvoid** params), (target, pname, params)) \
    f(void, glCurrentPaletteMatrixOES, (GLuint matrixpaletteindex), (matrixpaletteindex)) \
    f(void, glLoadPaletteFromModelViewMatrixOES, (), ()) \
    f(void, glMatrixIndexPointerOES, (GLint size, GLenum type, GLsizei stride, const GLvoid* pointer), (size, type, stride, pointer)) \
    f(void, glWeightPointerOES, (GLint size, GLenum type, GLsizei stride, const GLvoid* pointer), (size, type, stride, pointer)) \
    f(GLbitfield, glQueryMatrixxOES, (GLfixed* mantissa, GLint* exponent), (mantissa, exponent)) \
    f(void, glDepthRangefOES, (GLclampf zNear, GLclampf zFar), (zNear, zFar)) \
    f(void, glFrustumfOES, (GLfloat left, GLfloat right, GLfloat bottom, GLfloat top, GLfloat zNear, GLfloat zFar), (left, right, bottom, top, zNear, zFar)) \
    f(void, glOrthofOES, (GLfloat left, GLfloat right, GLfloat bottom, GLfloat top, GLfloat zNear, GLfloat zFar), (left, right, bottom, top, zNear, zFar)) \
    f(void, glClipPlanefOES, (GLenum plane, const GLfloat* equation), (plane, equation)) \
    f(void, glClipPlanefIMG, (GLenum plane, const GLfloat* equation), (plane, equation)) \
    f(void, glGetClipPlanefOES, (GLenum pname, GLfloat* eqn), (pname, eqn)) \
    f(void, glClearDepthfOES, (GLclampf depth), (depth)) \
    f(void, glTexGenfOES, (GLenum coord, GLenum pname, GLfloat param), (coord, pname, param)) \
    f(void, glTexGenfvOES, (GLenum coord, GLenum pname, const GLfloat* params), (coord, pname, params)) \
    f(void, glTexGeniOES, (GLenum coord, GLenum pname, GLint param), (coord, pname, param)) \
    f(void, glTexGenivOES, (GLenum coord, GLenum pname, const GLint* params), (coord, pname, params)) \
    f(void, glTexGenxOES, (GLenum coord, GLenum pname, GLfixed param), (coord, pname, param)) \
    f(void, glTexGenxvOES, (GLenum coord, GLenum pname, const GLfixed* params), (coord, pname, params)) \
    f(void, glGetTexGenfvOES, (GLenum coord, GLenum pname, GLfloat* params), (coord, pname, params)) \
    f(void, glGetTexGenivOES, (GLenum coord, GLenum pname, GLint* params), (coord, pname, params)) \
    f(void, glGetTexGenxvOES, (GLenum coord, GLenum pname, GLfixed* params), (coord, pname, params)) \
    f(void, glBindVertexArrayOES, (GLuint array), (array)) \
    f(void, glDeleteVertexArraysOES, (GLsizei n, const GLuint* arrays), (n, arrays)) \
    f(void, glGenVertexArraysOES, (GLsizei n, GLuint* arrays), (n, arrays)) \
    f(GLboolean, glIsVertexArrayOES, (GLuint array), (array)) \
    f(void, glDiscardFramebufferEXT, (GLenum target, GLsizei numAttachments, const GLenum* attachments), (target, numAttachments, attachments)) \
    f(void, glMultiDrawArraysEXT, (GLenum mode, GLint* first, GLsizei* count, GLsizei primcount), (mode, first, count, primcount)) \
    f(void, glMultiDrawElementsEXT, (GLenum mode, const GLsizei* count, GLenum type, const GLvoid** indices, GLsizei primcount), (mode, count, type, indices, primcount)) \
    f(void, glMultiDrawArraysSUN, (GLenum mode, GLint* first, GLsizei* count, GLsizei primcount), (mode, first, count, primcount)) \
    f(void, glMultiDrawElementsSUN, (GLenum mode, const GLsizei* count, GLenum type, const GLvoid** indices, GLsizei primcount), (mode, count, type, indices, primcount)) \
    f(void, glRenderbufferStorageMultisampleIMG, (GLenum target, GLsizei samples, GLenum internalformat, GLsizei width, GLsizei height), (target, samples, internalformat, width, height)) \
    f(void, glFramebufferTexture2DMultisampleIMG, (GLenum target, GLenum attachment, GLenum textarget, GLuint texture, GLint level, GLsizei samples), (target, attachment, textarget, texture, level, samples)) \
    f(void, glDeleteFencesNV, (GLsizei n, const GLuint* fences), (n, fences)) \
    f(void, glGenFencesNV, (GLsizei n, GLuint* fences), (n, fences)) \
    f(GLboolean, glIsFenceNV, (GLuint fence), (fence)) \
    f(GLboolean, glTestFenceNV, (GLuint fence), (fence)) \
    f(void, glGetFenceivNV, (GLuint fence, GLenum pname, GLint* params), (fence, pname, params)) \
    f(void, glFinishFenceNV, (GLuint fence), (fence)) \
    f(void, glSetFenceNV, (GLuint fence, GLenum condition), (fence, condition)) \
    f(void, glGetDriverControlsQCOM, (GLint* num, GLsizei size, GLuint* driverControls), (num, size, driverControls)) \
    f(void, glGetDriverControlStringQCOM, (GLuint driverControl, GLsizei bufSize, GLsizei* length, GLchar* driverControlString), (driverControl, bufSize, length, driverControlString)) \
    f(void, glEnableDriverControlQCOM, (GLuint driverControl), (driverControl)) \
    f(void, glDisableDriverControlQCOM, (GLuint driverControl), (driverControl)) \
    f(void, glExtGetTexturesQCOM, (GLuint* textures, GLint maxTextures, GLint* numTextures), (textures, maxTextures, numTextures)) \
    f(void, glExtGetBuffersQCOM, (GLuint* buffers, GLint maxBuffers, GLint* numBuffers), (buffers, maxBuffers, numBuffers)) \
    f(void, glExtGetRenderbuffersQCOM, (GLuint* renderbuffers, GLint maxRenderbuffers, GLint* numRenderbuffers), (renderbuffers, maxRenderbuffers, numRenderbuffers)) \
    f(void, glExtGetFramebuffersQCOM, (GLuint* framebuffers, GLint maxFramebuffers, GLint* numFramebuffers), (framebuffers, maxFramebuffers, numFramebuffers)) \
    f(void, glExtGetTexLevelParameterivQCOM, (GLuint texture, GLenum face, GLint level, GLenum pname, GLint* params), (texture, face, level, pname, params)) \
    f(void, glExtTexObjectStateOverrideiQCOM, (GLenum target, GLenum pname, GLint param), (target, pname, param)) \
    f(void, glExtGetTexSubImageQCOM, (GLenum target, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLsizei width, GLsizei height, GLsizei depth, GLenum format, GLenum type, GLvoid* texels), (target, level, xoffset, yoffset, zoffset, width, height, depth, format, type, texels)) \
    f(void, glExtGetBufferPointervQCOM, (GLenum target, GLvoid** params), (target, params)) \
    f(void, glExtGetShadersQCOM, (GLuint* shaders, GLint maxShaders, GLint* numShaders), (shaders, maxShaders, numShaders)) \
    f(void, glExtGetProgramsQCOM, (GLuint* programs, GLint maxPrograms, GLint* numPrograms), (programs, maxPrograms, numPrograms)) \
    f(GLboolean, glExtIsProgramBinaryQCOM, (GLuint program), (program)) \
    f(void, glExtGetProgramBinarySourceQCOM, (GLuint program, GLenum shadertype, GLchar* source, GLint* length), (program, shadertype, source, length)) \
    f(void, glStartTilingQCOM, (GLuint x, GLuint y, GLuint width, GLuint height, GLbitfield preserveMask), (x, y, width, height, preserveMask)) \
    f(void, glEndTilingQCOM, (GLbitfield preserveMask), (preserveMask)) \
    \
    /* GL_EXT_texture_storage */ \
    f(void, glTexStorage1DEXT, (GLenum target, GLsizei levels, GLenum internalformat, GLsizei width), (target, levels, internalformat, width)) \
    f(void, glTexStorage2DEXT, (GLenum target, GLsizei levels, GLenum internalformat, GLsizei width, GLsizei height), (target, levels, internalformat, width, height)) \
    f(void, glTexStorage3DEXT, (GLenum target, GLsizei levels, GLenum internalformat, GLsizei width, GLsizei height, GLsizei depth), (target, levels, internalformat, width, height, depth)) \
    f(void, glTextureStorage1DEXT, (GLuint texture, GLenum target, GLsizei levels, GLenum internalformat, GLsizei width), (texture, target, levels, internalformat, width)) \
    f(void, glTextureStorage2DEXT, (GLuint texture, GLenum target, GLsizei levels, GLenum internalformat, GLsizei width, GLsizei height), (texture, target, levels, internalformat, width, height)) \
    f(void, glTextureStorage3DEXT, (GLuint texture, GLenum target, GLsizei levels, GLenum internalformat, GLsizei width, GLsizei height, GLsizei depth), (texture, target, levels, internalformat, width, height, depth)) \
    \
    /* GL_EXT_multisampled_render_to_texture */ \
    f(void, glRenderbufferStorageMultisampleEXT, (GLenum target, GLsizei samples, GLenum internalformat, GLsizei width, GLsizei height), (target, samples, internalformat, width, height)) \
    f(void, glFramebufferTexture2DMultisampleEXT, (GLenum target, GLenum attachment, GLenum textarget, GLuint texture, GLint level, GLsizei samples), (target, attachment, textarget, texture, level, samples)) \
