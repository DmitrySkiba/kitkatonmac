{
  'includes': [
    'common.gypi',
  ],

  'variables': {
    'local_root': '<(platform_root)/host/gles',
  },

  'targets': [
    {
      'target_name': 'host-gles',
      'product_name': 'host-gles',
      'type': 'static_library',

      'dependencies': [
        '<!(<(dependency) frameworks-opengl)',
        '<!(<(dependency) system-libcutils)',
      ],

      'include_dirs': [
        '<(local_root)/include',
      ],

      'defines': [
        'KHR_PREFIX=<(host_gles_prefix)',
      ],

      'xcode_settings': {
        'OTHER_CFLAGS': ['-Wno-c++11-narrowing'],
      },

      'link_settings': {
        'libraries': [
          '$(SDKROOT)/System/Library/Frameworks/Cocoa.framework',
          '$(SDKROOT)/System/Library/Frameworks/OpenGL.framework',
        ],
      },

      'sources': [
        '<(local_root)/EGL/ClientAPIExts.cpp',
        '<(local_root)/EGL/EglConfig.cpp',
        '<(local_root)/EGL/EglContext.cpp',
        '<(local_root)/EGL/EglDisplay.cpp',
        '<(local_root)/EGL/EglGlobalInfo.cpp',
        '<(local_root)/EGL/EglImp.cpp',
        '<(local_root)/EGL/EglMacApi.cpp',
        '<(local_root)/EGL/EglPbufferSurface.cpp',
        '<(local_root)/EGL/EglPixmapSurface.cpp',
        '<(local_root)/EGL/EglSurface.cpp',
        '<(local_root)/EGL/EglThreadInfo.cpp',
        '<(local_root)/EGL/EglValidate.cpp',
        '<(local_root)/EGL/EglWindowSurface.cpp',
        '<(local_root)/EGL/ThreadInfo.cpp',
        '<(local_root)/EGL/MacNative.m',
        '<(local_root)/EGL/MacPixelFormatsAttribs.m',

        '<(local_root)/GLcommon/DummyGLfuncs.cpp',
        '<(local_root)/GLcommon/etc1.cpp',
        '<(local_root)/GLcommon/FramebufferData.cpp',
        '<(local_root)/GLcommon/GLDispatch.cpp',
        '<(local_root)/GLcommon/GLESbuffer.cpp',
        '<(local_root)/GLcommon/GLEScontext.cpp',
        '<(local_root)/GLcommon/GLESpointer.cpp',
        '<(local_root)/GLcommon/GLESvalidate.cpp',
        '<(local_root)/GLcommon/GLutils.cpp',
        '<(local_root)/GLcommon/objectNameManager.cpp',
        '<(local_root)/GLcommon/PaletteTexture.cpp',
        '<(local_root)/GLcommon/RangeManip.cpp',
        '<(local_root)/GLcommon/TextureUtils.cpp',

        '<(local_root)/GLES_CM/GLEScmContext.cpp',
        '<(local_root)/GLES_CM/GLEScmImp.cpp',
        '<(local_root)/GLES_CM/GLEScmUtils.cpp',
        '<(local_root)/GLES_CM/GLEScmValidate.cpp',
      ],
    },
  ],
}
