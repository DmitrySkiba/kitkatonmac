/*
* Copyright (C) 2011 The Android Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

static const struct _egl_funcs_by_name {
    const char *name;
    void *proc;
} egl_funcs_by_name[] = {
    {"eglGetError", (void *)KHR_SYMBOL(eglGetError)},
    {"eglGetDisplay", (void *)KHR_SYMBOL(eglGetDisplay)},
    {"eglInitialize", (void *)KHR_SYMBOL(eglInitialize)},
    {"eglTerminate", (void *)KHR_SYMBOL(eglTerminate)},
    {"eglQueryString", (void *)KHR_SYMBOL(eglQueryString)},
    {"eglGetConfigs", (void *)KHR_SYMBOL(eglGetConfigs)},
    {"eglChooseConfig", (void *)KHR_SYMBOL(eglChooseConfig)},
    {"eglGetConfigAttrib", (void *)KHR_SYMBOL(eglGetConfigAttrib)},
    {"eglCreateWindowSurface", (void *)KHR_SYMBOL(eglCreateWindowSurface)},
    {"eglCreatePbufferSurface", (void *)KHR_SYMBOL(eglCreatePbufferSurface)},
    {"eglCreatePixmapSurface", (void *)KHR_SYMBOL(eglCreatePixmapSurface)},
    {"eglDestroySurface", (void *)KHR_SYMBOL(eglDestroySurface)},
    {"eglQuerySurface", (void *)KHR_SYMBOL(eglQuerySurface)},
    {"eglBindAPI", (void *)KHR_SYMBOL(eglBindAPI)},
    {"eglQueryAPI", (void *)KHR_SYMBOL(eglQueryAPI)},
    {"eglWaitClient", (void *)KHR_SYMBOL(eglWaitClient)},
    {"eglReleaseThread", (void *)KHR_SYMBOL(eglReleaseThread)},
    {"eglCreatePbufferFromClientBuffer", (void *)KHR_SYMBOL(eglCreatePbufferFromClientBuffer)},
    {"eglSurfaceAttrib", (void *)KHR_SYMBOL(eglSurfaceAttrib)},
    {"eglBindTexImage", (void *)KHR_SYMBOL(eglBindTexImage)},
    {"eglReleaseTexImage", (void *)KHR_SYMBOL(eglReleaseTexImage)},
    {"eglSwapInterval", (void *)KHR_SYMBOL(eglSwapInterval)},
    {"eglCreateContext", (void *)KHR_SYMBOL(eglCreateContext)},
    {"eglDestroyContext", (void *)KHR_SYMBOL(eglDestroyContext)},
    {"eglMakeCurrent", (void *)KHR_SYMBOL(eglMakeCurrent)},
    {"eglGetCurrentContext", (void *)KHR_SYMBOL(eglGetCurrentContext)},
    {"eglGetCurrentSurface", (void *)KHR_SYMBOL(eglGetCurrentSurface)},
    {"eglGetCurrentDisplay", (void *)KHR_SYMBOL(eglGetCurrentDisplay)},
    {"eglQueryContext", (void *)KHR_SYMBOL(eglQueryContext)},
    {"eglWaitGL", (void *)KHR_SYMBOL(eglWaitGL)},
    {"eglWaitNative", (void *)KHR_SYMBOL(eglWaitNative)},
    {"eglSwapBuffers", (void *)KHR_SYMBOL(eglSwapBuffers)},
    {"eglCopyBuffers", (void *)KHR_SYMBOL(eglCopyBuffers)},
    {"eglGetProcAddress", (void *)KHR_SYMBOL(eglGetProcAddress)},
    {"eglLockSurfaceKHR", (void *)KHR_SYMBOL(eglLockSurfaceKHR)},
    {"eglUnlockSurfaceKHR", (void *)KHR_SYMBOL(eglUnlockSurfaceKHR)},
    {"eglCreateImageKHR", (void *)KHR_SYMBOL(eglCreateImageKHR)},
    {"eglDestroyImageKHR", (void *)KHR_SYMBOL(eglDestroyImageKHR)},
    {"eglCreateSyncKHR", (void *)KHR_SYMBOL(eglCreateSyncKHR)},
    {"eglDestroySyncKHR", (void *)KHR_SYMBOL(eglDestroySyncKHR)},
    {"eglClientWaitSyncKHR", (void *)KHR_SYMBOL(eglClientWaitSyncKHR)},
    {"eglGetSyncAttribKHR", (void *)KHR_SYMBOL(eglGetSyncAttribKHR)}
};

static const int egl_num_funcs = sizeof(egl_funcs_by_name) / sizeof(struct _egl_funcs_by_name);
