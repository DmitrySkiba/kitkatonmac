{
  'includes': [
    'common.gypi',
  ],

  'variables': {
    'local_root': '<(platform_root)/frameworks/native/opengl/libs/EGL',
    'built_headers_path': '<(headers_root)/EGL_internal',
    'built_egl_headers_path': '<(built_headers_path)/EGL',
  },

  'targets': [
    {
      'target_name': 'frameworks-opengl-egl',
      'product_name': 'egl',
      'type': 'static_library',

      'dependencies': [
        '<!(<(dependency) hardware-libhardware)',
        '<!(<(dependency) frameworks-include)',
        '<!(<(dependency) frameworks-opengl)',
        '<!(<(dependency) system-liblog)',
        '<!(<(dependency) system-libutils)',
        '<!(<(dependency) system-libcutils)',
      ],

      'all_dependent_settings': {
        'defines': [
          'ANDROID_EGLNATIVE',
        ],
      },

      'direct_dependent_settings': {
        'include_dirs': [
          '<(built_headers_path)',
        ],
        'defines': [
          'EGL_NO_HOOKS',
          'STATIC_GLES',
        ],
      },

      'defines': [
        'GL_GLEXT_PROTOTYPES',
        'EGL_EGLEXT_PROTOTYPES',
        'EGL_NO_HOOKS',
        'STATIC_GLES',
        'STATIC_EGL_PREFIX="<(rc_egl_prefix)"',
        'ANDROID_EGLNATIVE',
      ],

      'sources': [
        '<(local_root)/egl_tls.cpp',
        '<(local_root)/egl_cache.cpp',
        '<(local_root)/egl_display.cpp',
        '<(local_root)/egl_object.cpp',
        '<(local_root)/egl.cpp',
        '<(local_root)/eglApi.cpp',
        '<(local_root)/getProcAddress.cpp',
        '<(local_root)/Loader.cpp',
        '<(local_root)/trace.cpp',
      ],

      'hard_dependency': 1,
      'actions': [ # TODO: implement copy.gypi (with correct inputs/outputs tracking)
        {
          'action_name': 'EGL-internal-headers',
          'message': 'Copying internal EGL headers...',

          'inputs': [],
          'outputs': [ '<(built_egl_headers_path)' ],
          'action': [
            'python', 'utils/copy.py',
            '--source', '<(local_root)/egl_cache.h',
            '--source', '<(local_root)/egl_display.h',
            '--source', '<(local_root)/egldefs.h',
            '--source', '<(local_root)/egl_entries.in',
            '--destination-path' ,'<(built_egl_headers_path)'
          ]
        },
      ],
    },
  ],
}
