{
  'includes': [
    'common.gypi'
  ],

  'variables': {
    'local_root': '<(platform_root)/frameworks/base',
  },

  'targets': [
    {
      'target_name': 'frameworks-base-include',
      'type': 'none',

      'direct_dependent_settings': {
        'include_dirs': [
          '<(local_root)/include',
        ],
      },
    },
  ],
}
