#include "EventHubOSX.h"

namespace android {

EventHub::EventHub() {
}

EventHub::~EventHub() {
}

uint32_t EventHub::getDeviceClasses(int32_t deviceId) const {
    return 0;
}

InputDeviceIdentifier EventHub::getDeviceIdentifier(int32_t deviceId) const {
    return InputDeviceIdentifier();
}

int32_t EventHub::getDeviceControllerNumber(int32_t deviceId) const {
    return 0;
}

void EventHub::getConfiguration(int32_t deviceId, PropertyMap* outConfiguration) const {
    return 0;
}

status_t EventHub::getAbsoluteAxisInfo(int32_t deviceId, int axis,
                                       RawAbsoluteAxisInfo* outAxisInfo) const {
    return 0;
}

bool EventHub::hasRelativeAxis(int32_t deviceId, int axis) const {
    return 0;
}

bool EventHub::hasInputProperty(int32_t deviceId, int property) const {
    return 0;
}

status_t EventHub::mapKey(int32_t deviceId, int32_t scanCode, int32_t usageCode,
                          int32_t* outKeycode, uint32_t* outFlags) const {
    *outKeycode = 0;
    *outFlags = 0;
    return NAME_NOT_FOUND;
}

status_t EventHub::mapAxis(int32_t deviceId, int32_t scanCode, AxisInfo* outAxisInfo) const {
    *outAxisInfo = AxisInfo();
    return NAME_NOT_FOUND;
}

void EventHub::setExcludedDevices(const Vector<String8>& devices) {
}

int32_t EventHub::getScanCodeState(int32_t deviceId, int32_t scanCode) const {
    return 0;
}

int32_t EventHub::getKeyCodeState(int32_t deviceId, int32_t keyCode) const {
    return 0;
}

int32_t EventHub::getSwitchState(int32_t deviceId, int32_t sw) const {
    return 0;
}

status_t EventHub::getAbsoluteAxisValue(int32_t deviceId, int32_t axis, int32_t* outValue) const {
    return 0;
}

bool EventHub::markSupportedKeyCodes(int32_t deviceId, size_t numCodes,
                                     const int32_t* keyCodes, uint8_t* outFlags) const {
    return false;
}

size_t EventHub::getEvents(int timeoutMillis, RawEvent* buffer, size_t bufferSize) {
    usleep(timeoutMillis * 1000);
    return 0;
}

bool EventHub::hasScanCode(int32_t deviceId, int32_t scanCode) const {
    return false;
}

bool EventHub::hasLed(int32_t deviceId, int32_t led) const {
    return false;
}

void EventHub::setLedState(int32_t deviceId, int32_t led, bool on) {
    return false;
}

void EventHub::getVirtualKeyDefinitions(int32_t deviceId,
                                        Vector<VirtualKeyDefinition>& outVirtualKeys) const {
}

sp<KeyCharacterMap> EventHub::getKeyCharacterMap(int32_t deviceId) const {
    return {};
}

bool EventHub::setKeyboardLayoutOverlay(int32_t deviceId, const sp<KeyCharacterMap>& map) {
    return false;
}

void EventHub::vibrate(int32_t deviceId, nsecs_t duration) {
}

void EventHub::cancelVibrate(int32_t deviceId) {
}

void EventHub::requestReopenDevices() {
}

void EventHub::wake() {
}

void EventHub::dump(String8& dump) {
    dump = "EventHubOSX";
}

void EventHub::monitor() {
}

// --- Global Functions ---

uint32_t getAbsAxisUsage(int32_t axis, uint32_t deviceClasses) {
    // Touch devices get dibs on touch-related axes.
    if (deviceClasses & INPUT_DEVICE_CLASS_TOUCH) {
        switch (axis) {
        case ABS_X:
        case ABS_Y:
        case ABS_PRESSURE:
        case ABS_TOOL_WIDTH:
        case ABS_DISTANCE:
        case ABS_TILT_X:
        case ABS_TILT_Y:
        case ABS_MT_SLOT:
        case ABS_MT_TOUCH_MAJOR:
        case ABS_MT_TOUCH_MINOR:
        case ABS_MT_WIDTH_MAJOR:
        case ABS_MT_WIDTH_MINOR:
        case ABS_MT_ORIENTATION:
        case ABS_MT_POSITION_X:
        case ABS_MT_POSITION_Y:
        case ABS_MT_TOOL_TYPE:
        case ABS_MT_BLOB_ID:
        case ABS_MT_TRACKING_ID:
        case ABS_MT_PRESSURE:
        case ABS_MT_DISTANCE:
            return INPUT_DEVICE_CLASS_TOUCH;
        }
    }

    // Joystick devices get the rest.
    return deviceClasses & INPUT_DEVICE_CLASS_JOYSTICK;
}

} // namespasce android
