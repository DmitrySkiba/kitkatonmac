{
  'includes': [
    'common.gypi',
  ],

  'variables': {
    'local_root': '<(platform_root)/testapps/Binder',
  },

  'targets': [
    {
      'target_name': 'testapps-Binder',
      'product_name': 'Binder',
      'type': 'executable',

      'dependencies': [
        # Must be first to initialize String8
        '<!(<(dependency) system-libutils)',

        '<!(<(dependency) frameworks-binder)',
      ],

      'xcode_settings': {
        'OTHER_LDFLAGS': ['-all_load'],
      },

      'sources': [
        '<(local_root)/main.cpp',
      ],
    },
  ],
}
