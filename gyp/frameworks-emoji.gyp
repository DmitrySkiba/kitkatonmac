{
  'includes': [
    'common.gypi'
  ],

  'variables': {
    'local_root': '<(platform_root)/frameworks/opt/emoji',
    'local_headers_root': '<(headers_root)/emoji',
  },

  'targets':[
    {
      'target_name': 'frameworks-emoji',
      'type': 'none',

      'direct_dependent_settings': {
        'include_dirs': [
          '<(local_headers_root)',
        ],
      },

      'hard_dependency': 1,
      'actions': [ # TODO: implement copy.gypi (with correct inputs/outputs tracking)
        {
          'action_name': 'emoji-headers',
          'message': 'Copying emoji headers...',

          'inputs': [],
          'outputs': [ '<(local_headers_root)' ],
          'action': [
            'python', 'utils/copy.py',
            '--source', '<(local_root)/EmojiFactory.h',
            '--destination-path' ,'<(local_headers_root)'
          ]
        },
      ],
    }
  ]
}