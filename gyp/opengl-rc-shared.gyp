{
  'includes': [
    'common.gypi'
  ],

  'variables': {
    'rc_root': '<(platform_root)/opengl/rc',
  },

  'targets': [
    {
      'target_name': 'opengl-rc-shared',
      'type': 'none',

      'direct_dependent_settings': {
        'include_dirs': [
          '<(rc_root)/shared',
        ],
      },
    },
  ],
}
