{
  'includes': [
    'common.gypi',
    'java-common.gypi',
  ],

  'variables': {
    'local_root': '<(platform_root)/external/bouncycastle',
  },

  'targets': [
    {
      'target_name': 'external-bouncycastle_jar',
      'type': 'none',

      'dependencies': [
        '<!(<(dependency) libcore-rt_jar)',
        '<!(<(dependency) libcore-conscrypt_jar)',
      ],

      'variables': {
        'jar_path': '<!(<(jar_path_v) bouncycastle)',
        'dex_path': '<!(<(dex_path_v) bouncycastle)',

        'compile_jar.source_paths': [
          '<(local_root)/bcprov/src/main/java',
        ],
        'compile_jar.source_exclude_filters': [
          'bcprov/src/main/java/org/bouncycastle/crypto/digests/AndroidDigestFactoryBouncyCastle.java',
        ],
        'compile_jar.boot_class_paths': '>(dependencies.jar_files)',
        'compile_jar.jar_file': '<(jar_path)',
        'compile_jar.jarjar_tool': '<(jarjar_tool)',
        'compile_jar.jarjar_rules_file': '<(local_root)/jarjar-rules.txt',

        'dex.jar_file': '<(jar_path)',
        'dex.dex_file': '<(dex_path)',
      },
      'includes': [ 'utils/compile_jar.gypi', 'utils/dex.gypi' ],
    }
  ]
}
