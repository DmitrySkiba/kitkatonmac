{
  'includes': [
    'common.gypi'
  ],

  'variables': {
    'local_root': '<(platform_root)/external/sqlite',
    'local_headers_root': '<(headers_root)/sqlite',
  },

  'targets':[
    {
      'target_name': 'external-sqlite',
      'product_name': 'sqlite',
      'type': 'static_library',

      'dependencies': [
        '<!(<(dependency) external-icu4c)',
        '<!(<(dependency) system-libcutils)',
      ],

      'direct_dependent_settings': {
        'include_dirs': [
          '<(local_headers_root)',
        ],
      },

      'defines': [
        'NDEBUG=1',
        'HAVE_USLEEP=1',
        'SQLITE_HAVE_ISNAN',
        'SQLITE_DEFAULT_JOURNAL_SIZE_LIMIT=1048576',
        'SQLITE_THREADSAFE=2',
        'SQLITE_TEMP_STORE=3',
        'SQLITE_POWERSAFE_OVERWRITE=1',
        'SQLITE_DEFAULT_FILE_FORMAT=4',
        'SQLITE_DEFAULT_AUTOVACUUM=1',
        'SQLITE_ENABLE_MEMORY_MANAGEMENT=1',
        'SQLITE_ENABLE_FTS3',
        'SQLITE_ENABLE_FTS3_BACKWARDS',
        'SQLITE_ENABLE_FTS4',
        'SQLITE_OMIT_BUILTIN_TEST',
        'SQLITE_OMIT_COMPILEOPTION_DIAGS',
        'SQLITE_OMIT_LOAD_EXTENSION',
        'SQLITE_DEFAULT_FILE_PERMISSIONS=0600',
        'fdatasync=fdatasync',
        'HAVE_MALLOC_USABLE_SIZE',
      ],

      'include_dirs': [
        '<(local_root)/android',
      ],

      'sources': [
        '<(local_root)/dist/sqlite3.c',
        '<(local_root)/android/PhoneNumberUtils.cpp',
        '<(local_root)/android/OldPhoneNumberUtils.cpp',
        '<(local_root)/android/sqlite3_android.cpp',
      ],

      'hard_dependency': 1,
      'actions': [ # TODO: implement copy.gypi (with correct inputs/outputs tracking)
        {
          'action_name': 'sqlite-headers',
          'message': 'Copying sqlite headers...',

          'inputs': [],
          'outputs': [ '<(local_headers_root)' ],
          'action': [
            'python', 'utils/copy.py',
            '--source', '<(local_root)/dist/sqlite3.h',
            '--source', '<(local_root)/android/sqlite3_android.h',
            '--destination-path' ,'<(local_headers_root)'
          ]
        },
      ],
    }
  ]
}