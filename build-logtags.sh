#!/bin/bash

platform_root=/Volumes/E/NewWork/KitKat/platform
out_root=/Volumes/E/NewWork/KitKat/out/manual/logtags

logtags=$platform_root/build/tools/java-event-log-tags.py

./gyp/java/logtags.py \
    --logtags-tool=$logtags \
    --merged-tags-file=$out_root/all-event-log-tags.txt \
    --inputs=" \
        $platform_root/frameworks/base/core/java/android/content/EventLogTags.logtags
        $platform_root/frameworks/base/core/java/android/speech/tts/EventLogTags.logtags
        $platform_root/frameworks/base/core/java/android/webkit/EventLogTags.logtags
    " \
    --output-path=$out_root \
    --stamp-file=$out_root/.stamp \
