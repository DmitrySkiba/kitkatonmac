{
  'includes': [
    'common.gypi'
  ],

  'variables': {
    'local_root': '<(platform_root)/external/harfbuzz_ng',
    'built_headers_path': '<(headers_root)/harfbuzz_ng',
  },

  'targets': [
    {
      'target_name': 'external-harfbuzz_ng',
      'product_name': 'harfbuzz_ng',
      'type': 'static_library',

      'dependencies': [
        '<!(<(dependency) external-icu4c)',
      ],

      'direct_dependent_settings': {
        'include_dirs': [
          '<(built_headers_path)',
        ],
      },

      'defines': [
        'HB_NO_MT',
        'HAVE_OT',
        'HAVE_ICU'
      ],

      'sources': [
        '<(local_root)/src/hb-blob.cc',
        '<(local_root)/src/hb-buffer-serialize.cc',
        '<(local_root)/src/hb-buffer.cc',
        '<(local_root)/src/hb-common.cc',
        '<(local_root)/src/hb-fallback-shape.cc',
        '<(local_root)/src/hb-font.cc',
        '<(local_root)/src/hb-ot-tag.cc',
        '<(local_root)/src/hb-set.cc',
        '<(local_root)/src/hb-shape.cc',
        '<(local_root)/src/hb-shape-plan.cc',
        '<(local_root)/src/hb-shaper.cc',
        '<(local_root)/src/hb-tt-font.cc',
        '<(local_root)/src/hb-unicode.cc',
        '<(local_root)/src/hb-warning.cc',
        '<(local_root)/src/hb-ot-layout.cc',
        '<(local_root)/src/hb-ot-map.cc',
        '<(local_root)/src/hb-ot-shape.cc',
        '<(local_root)/src/hb-ot-shape-complex-arabic.cc',
        '<(local_root)/src/hb-ot-shape-complex-default.cc',
        '<(local_root)/src/hb-ot-shape-complex-indic.cc',
        '<(local_root)/src/hb-ot-shape-complex-indic-table.cc',
        '<(local_root)/src/hb-ot-shape-complex-myanmar.cc',
        '<(local_root)/src/hb-ot-shape-complex-sea.cc',
        '<(local_root)/src/hb-ot-shape-complex-thai.cc',
        '<(local_root)/src/hb-ot-shape-normalize.cc',
        '<(local_root)/src/hb-ot-shape-fallback.cc',
        '<(local_root)/src/hb-icu.cc',
      ],

      'hard_dependency': 1,
      'actions': [ # TODO: implement copy.gypi (with correct inputs/outputs tracking)
        {
          'action_name': 'harfbuzz_ng-headers',
          'message': 'Copying harfbuzz_ng headers...',

          'inputs': [],
          'outputs': [ '<(built_headers_path)' ],
          'action': [
            'python', 'utils/copy.py',
            '--source', '<(local_root)/src/hb.h',
            '--source', '<(local_root)/src/hb-blob.h',
            '--source', '<(local_root)/src/hb-buffer.h',
            '--source', '<(local_root)/src/hb-common.h',
            '--source', '<(local_root)/src/hb-font.h',
            '--source', '<(local_root)/src/hb-set.h',
            '--source', '<(local_root)/src/hb-shape.h',
            '--source', '<(local_root)/src/hb-shape-plan.h',
            '--source', '<(local_root)/src/hb-unicode.h',
            '--source', '<(local_root)/src/hb-icu.h',
            '--source', '<(local_root)/src/hb-version.h',
            '--destination-path' ,'<(built_headers_path)'
          ]
        },
      ],
    },
  ],
}
