/*
 * Copyright (C) 2005 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define LOG_TAG "IPCThreadState"

#include <binder/IPCThreadState.h>

#include <binder/Binder.h>
#include <binder/BpBinder.h>

#include <utils/Log.h>
#include <utils/UniquePtr.h>
#include <utils/KeyedVector.h>

#include <private/binder/binder_module.h>
#include <private/binder/Static.h>

#ifdef HAVE_PTHREADS
#include <pthread.h>
#include <sched.h>
#include <sys/resource.h>
#endif

#if LOG_NDEBUG
#define LOG_REMOTEREFS(...)
#else
#define LOG_REMOTEREFS(...) ALOG(LOG_VERBOSE, "remoterefs", __VA_ARGS__)
#endif

// ---------------------------------------------------------------------------

namespace android {

class ServiceManagerImpl: public BnServiceManager {
public:
    sp<IBinder> getService(const String16& name) const override {
        AutoMutex lock(mMutex);
        ssize_t index = mServices.indexOfKey(name);
        return index < 0 ? nullptr : mServices.valueAt(index);
    }

    sp<IBinder> checkService(const String16& name) const override {
        return getService(name);
    }

    status_t addService(const String16& name,
                        const sp<IBinder>& service,
                        bool allowIsolated) override
    {
        AutoMutex lock(mMutex);
        if (mServices.indexOfKey(name) >= 0) {
            return ALREADY_EXISTS;
        }
        mServices.add(name, service);
        return NO_ERROR;
    }

    Vector<String16> listServices() override {
        AutoMutex lock(mMutex);
        Vector<String16> serviceNames;
        for (size_t i = 0; i != mServices.size(); ++i) {
            serviceNames.add(mServices.keyAt(i));
        }
        return serviceNames;
    }
private:
    mutable Mutex mMutex;
    KeyedVector<String16, sp<IBinder> > mServices;
};

//TODO: stuff with LOG_XXX macros above
class HandleManager {
public:
    typedef int32_t HandleType;
    const static HandleType InvalidHandle = -1;
public:
    HandleManager() {
        sp<ServiceManagerImpl> serviceManager = new ServiceManagerImpl();
        incStrong(serviceManager.get());
    }

    sp<IBinder> getStrong(HandleType handle) {
        AutoMutex lock(mMutex);
        Entry* entry = findEntry(handle);
        return entry ? entry->binder : nullptr;
    }
    wp<IBinder> getWeak(HandleType handle) {
        AutoMutex lock(mMutex);
        Entry* entry = findEntry(handle);
        if (!entry) {
            return nullptr;
        }
        wp<IBinder> result;
        result.set_object_and_refs(entry->binder, entry->weakBinder);
        return result;
    }
    pthread_tgid_t getThreadGroup(HandleType handle) {
        AutoMutex lock(mMutex);
        Entry* entry = findEntry(handle);
        ALOG_ASSERT(entry);
        if (!entry) {
            return PTHREAD_ROOT_TGID;
        }
        return entry->threadGroup;
    }

    HandleType incStrong(IBinder* binder) {
        if (!binder) {
            return InvalidHandle;
        }
        AutoMutex lock(mMutex);
        return doInc(binder, binder->getWeakRefs(), Strong);
    }
    HandleType incWeak(IBinder* binder, IBinder::weakref_type* weakBinder) {
        if (!binder || !weakBinder) {
            return InvalidHandle;
        }
        AutoMutex lock(mMutex);
        return doInc(binder, weakBinder, Weak);
    }

    bool decStrong(IBinder* binder) {
        if (!binder) {
            return false;
        }
        AutoMutex lock(mMutex);
        Entry* entry = findEntry(binder->getWeakRefs());
        if (!entry) {
            return false;
        }
        decEntry(*entry, Strong);
        return true;
    }
    bool decWeak(IBinder::weakref_type* weakBinder) {
        AutoMutex lock(mMutex);
        Entry* entry = findEntry(weakBinder);
        if (!entry) {
            return false;
        }
        decEntry(*entry, Weak);
        return true;
    }

    IBinder* incStrong(HandleType handle) {
        AutoMutex lock(mMutex);
        Entry* entry = findEntry(handle);
        if (!entry || !incEntry(*entry, Strong)) {
            return nullptr;
        }
        return entry->binder;
    }
    IBinder::weakref_type* incWeak(HandleType handle) {
        AutoMutex lock(mMutex);
        Entry* entry = findEntry(handle);
        if (!entry || !incEntry(*entry, Weak)) {
            return nullptr;
        }
        return entry->weakBinder;
    }

    bool decStrong(HandleType handle) {
        AutoMutex lock(mMutex);
        Entry* entry = findEntry(handle);
        if (!entry) {
            return false;
        }
        decEntry(*entry, Strong);
        return true;
    }
    bool decWeak(HandleType handle) {
        AutoMutex lock(mMutex);
        Entry* entry = findEntry(handle);
        if (!entry) {
            return false;
        }
        decEntry(*entry, Weak);
        return true;
    }

private:
    enum Type {
        Strong,
        Weak
    };
    struct Entry {
        HandleType handle;
        IBinder* binder;
        IBinder::weakref_type* weakBinder;
        size_t references;
        pthread_tgid_t threadGroup;
    };

    HandleType doInc(IBinder* binder, IBinder::weakref_type* weakBinder, Type type) {
        Entry* entry = findEntry(weakBinder);
        if (entry) {
            incEntry(*entry, type);
        } else {
            entry = addEntry(binder, weakBinder);
            if (!entry) {
                return InvalidHandle;
            }
            if (!incEntry(*entry, type)) {
                removeEntry(*entry);
                return InvalidHandle;
            }
        }
        return entry->handle;
    }

    Entry* findEntry(HandleType handle) {
        if (handle < 0 || handle >= mHandles.size()) {
            return nullptr;
        }
        Entry* entry = mHandles.editItemAt(handle);
        return isEntryEmpty(entry) ? nullptr : entry;
    }
    Entry* findEntry(IBinder::weakref_type* weakBinder) {
        ssize_t index = mEntryMap.indexOfKey(weakBinder);
        if (index < 0) {
            return nullptr;
        }
        return mEntryMap.valueAt(index);
    }

    Entry* addEntry(IBinder* binder, IBinder::weakref_type* weakBinder) {
        Entry * entry = nullptr;
        for (size_t i = 0; i != mHandles.size(); ++i) {
            if (isEntryEmpty(mHandles[i])) {
                entry = mHandles.editItemAt(i);
                break;
            }
        }
        if (!entry) {
            mHandles.push(new Entry());
            entry = mHandles.editTop();
            emptyEntry(*entry);
            entry->handle = (HandleType)(mHandles.size() - 1);
        }
        entry->threadGroup = pthread_get_tgid_np();
        if (entry->threadGroup == PTHREAD_ROOT_TGID) {
            // Only allow root thread group when registering zero handle (from ctor)
            ALOG_ASSERT(entry->handle == 0);
            if (entry->handle) {
                emptyEntry(*entry);
                return nullptr;
            }
        }
        ALOG_ASSERT(!entry->references);
        entry->binder = binder;
        entry->weakBinder = weakBinder;
        mEntryMap.add(weakBinder, entry);
        LOG_REMOTEREFS("Added %s.", formatEntry(*entry).string());
        return entry;
    }
    void removeEntry(Entry& entry) {
        LOG_REMOTEREFS("Removed %s.", formatEntry(entry).string());
        mEntryMap.removeItem(entry.weakBinder);
        emptyEntry(entry);
    }

    bool incEntry(Entry& entry, Type type) {
        if (type == Strong) {
            if (!entry.weakBinder->attemptIncStrong(this)) {
                LOG_REMOTEREFS("Failed to add strong reference to %s.",
                               formatEntry(entry).string());
                return false;
            }
        } else {
            entry.weakBinder->incWeak(this);
        }
        entry.references++;
        LOG_REMOTEREFS("Added %s reference to %s.",
                       formatType(type), formatEntry(entry).string());
        return true;
    }
    void decEntry(Entry& entry, Type type) {
        if (type == Strong) {
            ALOG_ASSERT(entry.weakBinder->attemptIncStrong(this) &&
                        entry.binder->getStrongCount() >= 2 &&
                        (entry.binder->decStrong(this), true));
            entry.binder->decStrong(this);
        } else {
            entry.weakBinder->decWeak(this);
        }
        ALOG_ASSERT(entry.references);
        entry.references--;
        LOG_REMOTEREFS("Removed %s reference from %s.",
                       formatType(type), formatEntry(entry).string());
        if (!entry.references) {
            removeEntry(entry);
        }
    }

    static bool isEntryEmpty(const Entry* entry) {
        return entry->binder == nullptr || entry->weakBinder == nullptr;
    }
    static void emptyEntry(Entry& entry) {
        entry.binder = nullptr;
        entry.references = 0;
        entry.threadGroup = PTHREAD_ROOT_TGID;
    }
    static String8 formatEntry(const Entry& entry) {
        return String8::format("%p (handle #%d, %zu references)",
                               entry.binder, entry.handle, entry.references);
    }

    static const char* formatType(Type type) {
        switch (type) {
            case Strong:    return "strong";
            case Weak:      return "weak";
            default:        return nullptr;
        }
    }

    Mutex mMutex;
    Vector<Entry*> mHandles;
    KeyedVector<IBinder::weakref_type*, Entry*> mEntryMap;
};
static HandleManager gHandleManager;

static pthread_mutex_t gTLSMutex = PTHREAD_MUTEX_INITIALIZER;
static bool gHaveTLS = false;
static pthread_key_t gTLS = 0;
static bool gShutdown = false;

IPCThreadState* IPCThreadState::self()
{
    if (gHaveTLS) {
restart:
        const pthread_key_t k = gTLS;
        IPCThreadState* st = (IPCThreadState*)pthread_getspecific(k);
        if (st) return st;
        return new IPCThreadState;
    }

    if (gShutdown) return NULL;

    pthread_mutex_lock(&gTLSMutex);
    if (!gHaveTLS) {
        if (pthread_key_create(&gTLS, threadDestructor) != 0) {
            pthread_mutex_unlock(&gTLSMutex);
            return NULL;
        }
        gHaveTLS = true;
    }
    pthread_mutex_unlock(&gTLSMutex);
    goto restart;
}

IPCThreadState* IPCThreadState::selfOrNull()
{
    if (gHaveTLS) {
        const pthread_key_t k = gTLS;
        IPCThreadState* st = (IPCThreadState*)pthread_getspecific(k);
        return st;
    }
    return NULL;
}

void IPCThreadState::shutdown()
{
    gShutdown = true;

    if (gHaveTLS) {
        // XXX Need to wait for all thread pool threads to exit!
        IPCThreadState* st = (IPCThreadState*)pthread_getspecific(gTLS);
        if (st) {
            delete st;
            pthread_setspecific(gTLS, NULL);
        }
        gHaveTLS = false;
    }
}

void IPCThreadState::disableBackgroundScheduling(bool disable)
{
}

sp<ProcessState> IPCThreadState::process()
{
    return mProcess;
}

status_t IPCThreadState::clearLastError()
{
    const status_t err = mLastError;
    mLastError = NO_ERROR;
    return err;
}

int IPCThreadState::getCallingPid() const
{
    return mCallingPid;
}

int IPCThreadState::getCallingUid() const
{
    return mCallingUid;
}

int64_t IPCThreadState::clearCallingIdentity()
{
    int64_t token = ((int64_t)mCallingUid<<32) | mCallingPid;
    clearCaller();
    return token;
}

void IPCThreadState::setStrictModePolicy(int32_t policy)
{
    mStrictModePolicy = policy;
}

int32_t IPCThreadState::getStrictModePolicy() const
{
    return mStrictModePolicy;
}

void IPCThreadState::setLastTransactionBinderFlags(int32_t flags)
{
    mLastTransactionBinderFlags = flags;
}

int32_t IPCThreadState::getLastTransactionBinderFlags() const
{
    return mLastTransactionBinderFlags;
}

void IPCThreadState::restoreCallingIdentity(int64_t token)
{
    mCallingUid = (int)(token>>32);
    mCallingPid = (int)token;
}

void IPCThreadState::clearCaller()
{
    mCallingPid = getpid();
    mCallingUid = getuid();
}

void IPCThreadState::flushCommands()
{
}

void IPCThreadState::joinThreadPool(bool isMain)
{
}

void IPCThreadState::stopProcess(bool immediate)
{
}

status_t IPCThreadState::transact(int32_t handle,
                                  uint32_t code, const Parcel& data,
                                  Parcel* reply, uint32_t flags)
{
    if (flags & IBinder::FLAG_ONEWAY) {
        ALOG_ASSERT(false, "IBinder::FLAG_ONEWAY is not supported");
        return INVALID_OPERATION;
    }

    sp<IBinder> binder = gHandleManager.getStrong(handle);
    if (binder == nullptr) {
        return DEAD_OBJECT;
    }

    bool translate = (gHandleManager.getThreadGroup(handle) != pthread_get_tgid_np());

    const Parcel* transactData = &data;
    Parcel translatedData;
    if (translate) {
        status_t error = translateObjects(data, translatedData);
        if (error != NO_ERROR) {
            return error;
        }
        transactData = &translatedData;
    }

    Parcel* transactReply = reply;
    Parcel untranslatedReply;
    if (translate || !reply) {
        transactReply = &untranslatedReply;
    }

    status_t error = binder->transact(code, *transactData, transactReply);
    if (error == NO_ERROR && translate && reply) {
        error = translateObjects(untranslatedReply, *reply);
    }

    return error;
}

void IPCThreadState::incStrongHandle(int32_t handle)
{
    gHandleManager.incStrong(handle);
}

void IPCThreadState::decStrongHandle(int32_t handle)
{
    gHandleManager.decStrong(handle);
}

void IPCThreadState::incWeakHandle(int32_t handle)
{
    gHandleManager.incWeak(handle);
}

void IPCThreadState::decWeakHandle(int32_t handle)
{
    gHandleManager.decWeak(handle);
}

status_t IPCThreadState::attemptIncStrongHandle(int32_t handle)
{
    return gHandleManager.incStrong(handle) ? NO_ERROR : UNKNOWN_ERROR;
}

void IPCThreadState::expungeHandle(int32_t handle, IBinder* binder)
{
    self()->mProcess->expungeHandle(handle, binder);
}

status_t IPCThreadState::requestDeathNotification(int32_t handle, BpBinder* proxy)
{
    // No death notifications since everything runs in a single process.
    return NO_ERROR;
}

status_t IPCThreadState::clearDeathNotification(int32_t handle, BpBinder* proxy)
{
    // No death notifications since everything runs in a single process.
    return NO_ERROR;
}

IPCThreadState::IPCThreadState()
    : mProcess(ProcessState::self()),
      mMyThreadId(androidGetTid()),
      mStrictModePolicy(0),
      mLastTransactionBinderFlags(0)
{
    pthread_setspecific(gTLS, this);
    clearCaller();
    mIn.setDataCapacity(256);
    mOut.setDataCapacity(256);
}

IPCThreadState::~IPCThreadState()
{
}

void IPCThreadState::threadDestructor(void *st)
{
    IPCThreadState* const self = static_cast<IPCThreadState*>(st);
    if (self) {
        self->flushCommands();
        // XXX send BINDER_THREAD_EXIT?
        delete self;
    }
}

status_t IPCThreadState::translateObjects(const Parcel& source, Parcel& target)
{
    target.freeData();

    if (!source.objectsCount()) {
        status_t error = target.appendFrom(&source, 0, source.dataSize());
        target.setDataPosition(0);
        return error;
    }

    uint8_t* data = static_cast<uint8_t*>(malloc(source.ipcDataSize()));
    if (!data) {
        return NO_MEMORY;
    }
    memcpy(data, source.ipcData(), source.ipcDataSize());

    size_t* objects = static_cast<size_t*>(malloc(source.ipcObjectsCount() * sizeof(size_t)));
    if (!objects) {
        free(data);
        return NO_MEMORY;
    }
    memcpy(objects, source.ipcObjects(), source.ipcObjectsCount() * sizeof(size_t));

    bool gotDeadObject = false;
    for (size_t i = 0; i != source.objectsCount(); ++i) {
        flat_binder_object* object = reinterpret_cast<flat_binder_object*>(data + objects[i]);

        switch (object->type) {
            case BINDER_TYPE_BINDER: {
                IBinder* binder = static_cast<IBinder*>(object->cookie);
                HandleManager::HandleType handle = gHandleManager.incStrong(binder);
                if (handle == HandleManager::InvalidHandle) {
                    gotDeadObject = true;
                }
                object->type = BINDER_TYPE_HANDLE;
                object->handle = handle;
                break;
            }
            case BINDER_TYPE_WEAK_BINDER: {
                IBinder* binder = static_cast<IBinder*>(object->cookie);
                IBinder::weakref_type* weakBinder = static_cast<IBinder::weakref_type*>(object->binder);
                HandleManager::HandleType handle = gHandleManager.incWeak(binder, weakBinder);
                if (handle == HandleManager::InvalidHandle) {
                    gotDeadObject = true;
                }
                object->type = BINDER_TYPE_WEAK_HANDLE;
                object->handle = handle;
                break;
            }
            case BINDER_TYPE_HANDLE: {
                IBinder* binder = gHandleManager.incStrong(object->handle);
                if (!binder) {
                    gotDeadObject = true;
                }
                object->type = BINDER_TYPE_BINDER;
                object->binder = binder ? binder->getWeakRefs() : nullptr;
                object->cookie = binder;
                break;
            }
            case BINDER_TYPE_WEAK_HANDLE: {
                IBinder* binder = gHandleManager.incStrong(object->handle);
                IBinder::weakref_type* weakBinder = gHandleManager.incWeak(object->handle);
                if (!binder || !weakBinder) {
                    gotDeadObject = true;
                }
                object->type = BINDER_TYPE_WEAK_BINDER;
                object->binder = weakBinder;
                object->cookie = binder;
                if (binder) {
                    gHandleManager.decStrong(binder);
                }
                break;
            }
            case BINDER_TYPE_FD: {
                object->handle = dup(object->handle);
                if (object->handle == -1) {
                    gotDeadObject = true;
                }
                break;
            }
        }
    }

    target.ipcSetDataReference(data, source.ipcDataSize(),
                               objects, source.ipcObjectsCount(),
                               releaseTranslatedObjects, nullptr);
    if (gotDeadObject) {
        target.freeData();
        return DEAD_OBJECT;
    }

    return NO_ERROR;
}

void IPCThreadState::releaseTranslatedObjects(Parcel* parcel, const uint8_t* data, size_t dataSize,
                                              const size_t* objects, size_t objectsSize,
                                              void* cookie)
{
    parcel->closeFileDescriptors();
    if (data && objects && objectsSize) {
        for (size_t i = 0; i != objectsSize; ++i) {
            const flat_binder_object* object =
                reinterpret_cast<const flat_binder_object*>(data + objects[i]);
            switch (object->type) {
                case BINDER_TYPE_BINDER: {
                    IBinder* binder = static_cast<IBinder*>(object->cookie);
                    if (binder) {
                        ALOG_ASSERT(gHandleManager.decStrong(binder));
                    }
                    break;
                }
                case BINDER_TYPE_WEAK_BINDER: {
                    IBinder::weakref_type* weakBinder = static_cast<IBinder::weakref_type*>(object->binder);
                    if (weakBinder) {
                        ALOG_ASSERT(gHandleManager.decWeak(weakBinder));
                    }
                    break;
                }
                case BINDER_TYPE_HANDLE: {
                    HandleManager::HandleType handle = (HandleManager::HandleType)object->handle;
                    if (handle != HandleManager::InvalidHandle) {
                        ALOG_ASSERT(gHandleManager.decStrong(handle));
                    }
                    break;
                }
                case BINDER_TYPE_WEAK_HANDLE: {
                    HandleManager::HandleType handle = (HandleManager::HandleType)object->handle;
                    if (handle != HandleManager::InvalidHandle) {
                        ALOG_ASSERT(gHandleManager.decWeak(handle));
                    }
                    break;
                }
                case BINDER_TYPE_FD: {
                    if (object->handle != -1) {
                        close(object->handle);
                    }
                    break;
                }
            }
        }
    }
    if (data) {
        free(const_cast<uint8_t*>(data));
        free(const_cast<size_t*>(objects));
    }
}

}; // namespace android
