{
  'includes': [
    'common.gypi',
    'system-common.gypi',
  ],

  'variables': {
    'local_root': '<(system_root)/libsuspend',
  },

  'targets': [
    {
      'target_name': 'system-libsuspend',
      'product_name': 'libsuspend',
      'type': 'static_library',

      'direct_dependent_settings': {
        'include_dirs': [
          '<(local_root)/include',
        ],
      },

      'include_dirs': [
        '<(local_root)/include',
      ],

      'sources': [
        '<(local_root)/no_autosuspend.c',
      ],
    },
  ],
}
