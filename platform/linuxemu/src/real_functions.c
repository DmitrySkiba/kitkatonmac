#include "real_functions.h"
#include <assert.h>
#include <dlfcn.h>
#include "utils.h"

#define DEFINE_REAL_FUNCTION(name, ...) \
    __typeof__(name)* real_##name = NULL;

#define INITIALIZE_REAL_FUNCTION(name, suffix...) \
    real_##name = (__typeof__(name)*)dlsym(RTLD_NEXT, #name suffix); \
    assert(real_##name);

ENUM_REAL_FUNCTIONS(DEFINE_REAL_FUNCTION)

DEFINE_ONCE_INIT_FUNCTION(void init_real_functions(void), {
    ENUM_REAL_FUNCTIONS(INITIALIZE_REAL_FUNCTION)
})
