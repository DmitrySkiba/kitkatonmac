{
  'includes': [
    'common.gypi'
  ],

  'variables': {
    'local_root': '<(platform_root)/external/conscrypt',
  },

  'targets': [
    {
      'target_name': 'external-conscrypt',
      'product_name': 'conscrypt',
      'type': 'static_library',

      'dependencies': [
        '<!(<(dependency) libnativehelper)',
        '<!(<(dependency) system-liblog)',
        '<!(<(dependency) external-openssl)',
      ],

      'defines': [
        'JNI_JARJAR_PREFIX=com/android/',
      ],

      'include_dirs': [
         # TODO: these include paths should come from dependencies
        '<(platform_root)/external/openssl',
        '<(platform_root)/libcore/include',
        '<(platform_root)/libcore/luni/src/main/native'
      ],

      'sources': [
        '<(local_root)/src/main/native/org_conscrypt_NativeCrypto.cpp',
      ],
    },
  ],
}
