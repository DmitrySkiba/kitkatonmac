# Copyright (C) 2015 Dmitry Skiba
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

{
  'includes': [
    'common.gypi',
    'java-common.gypi',
  ],

  'conditions': [
    [ 'using_gradle == 1', {
      'targets': [
        {
          'target_name': 'boot_oat',
          'type': 'none',

          'dependencies': [
            '<!(<(dependency) force_action)',
            '<!(<(dependency) art-dex2oat)',
          ],

          'actions': [
            {
              'action_name': 'boot_oat',
              'message': 'Building boot.* files...',

              'inputs': [
                '<(force_action_input)',
              ],
              'outputs': [
                '<(boot_art_file)',
                '<(boot_oat_file)',
              ],

              'action': [
                'python', 'utils/cwd_launcher.py', '<(root_path)',
                'gradlew', 'boot:oat'
              ],
            },
          ]
        }
      ]
    }, {
      'variables': {
        'jar_dependencies': [
          '<!(<(dependency) libcore-rt_jar)',
          '<!(<(dependency) libcore-conscrypt_jar)',
          '<!(<(dependency) external-ext_jar)',
          '<!(<(dependency) external-okhttp_jar)',
          '<!(<(dependency) external-junit_jar)',
          '<!(<(dependency) external-bouncycastle_jar)',
          '<!(<(dependency) frameworks-base-framework_jar)',
          '<!(<(dependency) frameworks-android.policy_jar)',
          '<!(<(dependency) frameworks-base-services_jar)',
          '<!(<(dependency) frameworks-opt-voip_jar)',
          '<!(<(dependency) frameworks-opt-telephony_jar)',
        ],
      },
      'targets': [
        {
          'target_name': 'boot_jars',
          'type': 'none',

          'dependencies': [ '<@(jar_dependencies)' ],
          'export_dependent_settings': [ '<@(jar_dependencies)' ],
        },
        {
          'target_name': 'boot_oat',
          'type': 'none',

          'dependencies': [
            'boot_jars',
            '<!(<(dependency) art-dex2oat)',
          ],

          'actions': [
            {
              'action_name': 'boot_oat',
              'message': 'Building boot.* files...',

              'inputs': [
                '>@(dependencies.dex_files)',
              ],
              'outputs': [
                '<(boot_art_file)',
                '<(boot_oat_file)',
              ],

              'action': [
                'python', 'utils/cwd_launcher.py', '<(android_fs_root)',
                '<!(<(relpath) <(android_fs_root) <(dex2oat_file))',
                '--android-root=<!(<(relpath) <(android_fs_root) <(android_root_path))',
                '--runtime-arg', '-Xms64m',
                '--runtime-arg', '-Xmx64m',
                '--runtime-arg', '-XX:DisableHSpaceCompactForOOM',
                '--dex-file=<!(<(relpath) <(android_fs_root) <!(<(dex_path_v) rt))',
                '--dex-file=<!(<(relpath) <(android_fs_root) <!(<(dex_path_v) ext))',
                '--dex-file=<!(<(relpath) <(android_fs_root) <!(<(dex_path_v) okhttp))',
                '--dex-file=<!(<(relpath) <(android_fs_root) <!(<(dex_path_v) junit))',
                '--dex-file=<!(<(relpath) <(android_fs_root) <!(<(dex_path_v) conscrypt))',
                '--dex-file=<!(<(relpath) <(android_fs_root) <!(<(dex_path_v) bouncycastle))',
                '--dex-file=<!(<(relpath) <(android_fs_root) <!(<(dex_path_v) framework))',
                '--dex-file=<!(<(relpath) <(android_fs_root) <!(<(dex_path_v) framework2))',
                '--dex-file=<!(<(relpath) <(android_fs_root) <!(<(dex_path_v) android.policy))',
                '--dex-file=<!(<(relpath) <(android_fs_root) <!(<(dex_path_v) services))',
                '--dex-file=<!(<(relpath) <(android_fs_root) <!(<(dex_path_v) voip))',
                '--dex-file=<!(<(relpath) <(android_fs_root) <!(<(dex_path_v) telephony))',
                '--base=<(boot_oat_base)',
                '--image-classes=<(platform_root)/frameworks/base/preloaded-classes',
                '--image=<!(<(relpath) <(android_fs_root) <(boot_art_file))',
                '--oat-file=<!(<(relpath) <(android_fs_root) <(boot_oat_file))',
              ],
            },
          ],
        }
      ]
    }]
  ]
}
