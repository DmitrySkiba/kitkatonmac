/*
* Copyright (C) 2011 The Android Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#include <stdio.h>
#include "ThreadInfo.h"

//#define TRACE_THREADINFO
#ifdef TRACE_THREADINFO
#define LOG_THREADINFO(x...) fprintf(stderr, x)
#else
#define LOG_THREADINFO(x...)
#endif

void ThreadInfo::updateInfo(ContextPtr eglCtx,
                            EglDisplay* dpy,
                            GLEScontext* glesCtx,
                            ShareGroupPtr share,
                            ObjectNameManager* manager) {

    eglContext  = eglCtx;
    eglDisplay  = dpy;
    glesContext = glesCtx;
    shareGroup  = share;
    objManager  = manager;
}

#include <pthread.h>

static pthread_once_t s_tls_init = PTHREAD_ONCE_INIT;
static pthread_key_t s_tls;

static int active_instance = 0;

static void tlsDestruct(void *ptr)
{
    active_instance--;
    LOG_THREADINFO("tlsDestruct EGL %lx %d\n", (long)ptr, active_instance);

    delete (ThreadInfo *)ptr;
}

static void tlsKeyInit(void) {
    int error = pthread_key_create(&s_tls, tlsDestruct);
    if (error) {
        // TODO log and halt
    }
}

ThreadInfo *getThreadInfo()
{
    pthread_once(&s_tls_init, tlsKeyInit);

    ThreadInfo *ti = (ThreadInfo *)pthread_getspecific(s_tls);
    if (!ti) {
        ti = new ThreadInfo();
        pthread_setspecific(s_tls, ti);
        active_instance++;
        LOG_THREADINFO("getThreadInfo EGL %lx %d\n", (long)ti, active_instance);
    }
    return ti;
}
