{
  'includes': [
    'common.gypi',
    'testapps-common.gypi',
  ],

  'variables': {
    'local_root': '<(platform_root)/testapps/SurfaceFlinger',
  },

  'targets': [
    {
      'target_name': 'testapps-SurfaceFlinger',
      'product_name': 'SurfaceFlinger',
      'type': 'executable',

      'dependencies': [
        '<!(<(dependency) opengl-rc-gles1)',
        '<!(<(dependency) hardware-libhardware)',
        '<!(<(dependency) frameworks-base-androidfw)',
        '<!(<(dependency) frameworks-binder)',
        '<!(<(dependency) frameworks-opengl)',
        '<!(<(dependency) frameworks-surfaceflinger)',
        '<!(<(dependency) external-skia)',
      ],

      'xcode_settings': {
        'INFOPLIST_FILE': '<(local_root)/Info.plist',
      },

      'mac_bundle_resources': [
        '<(local_root)/Window.xib',
      ],

      'sources': [
        '<(local_root)/main.m',
        '<(local_root)/AppDelegate.mm',
      ],
    },
  ],
}
