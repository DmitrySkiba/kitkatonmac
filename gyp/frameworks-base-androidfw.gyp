{
  'includes': [
    'common.gypi',
  ],

  'variables': {
    'local_root': '<(platform_root)/frameworks/base/libs/androidfw',
  },

  'targets': [
    {
      'target_name': 'frameworks-base-androidfw',
      'product_name': 'androidfw',
      'type': 'static_library',

      'dependencies': [
        '<!(<(dependency) frameworks-base-include)',
        '<!(<(dependency) frameworks-binder)',
        '<!(<(dependency) system-liblog)',
        '<!(<(dependency) system-libutils)',
        '<!(<(dependency) system-libcutils)',
        '<!(<(dependency) zlib)',
      ],

      'export_dependent_settings': [
        '<!(<(dependency) frameworks-base-include)',
      ],

      'sources': [
        '<(local_root)/Asset.cpp',
        '<(local_root)/AssetDir.cpp',
        '<(local_root)/AssetManager.cpp',
        '<(local_root)/misc.cpp',
        '<(local_root)/ObbFile.cpp',
        '<(local_root)/ResourceTypes.cpp',
        '<(local_root)/StreamingZipInflater.cpp',
        '<(local_root)/ZipFileRO.cpp',
        '<(local_root)/ZipUtils.cpp',
        '<(local_root)/BackupData.cpp',
        '<(local_root)/BackupHelpers.cpp',
        '<(local_root)/CursorWindow.cpp',
      ],
    },
  ],
}
