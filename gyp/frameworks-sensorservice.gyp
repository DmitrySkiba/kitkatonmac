{
  'includes': [
    'common.gypi',
  ],

  'variables': {
    'local_root': '<(platform_root)/frameworks/native/services/sensorservice',
  },

  'targets': [
    {
      'target_name': 'frameworks-sensorservice',
      'product_name': 'sensorservice',
      'type': 'static_library',

      'dependencies': [
        '<!(<(dependency) hardware-libhardware)',
        '<!(<(dependency) frameworks-binder)',
        '<!(<(dependency) frameworks-ui)',
        '<!(<(dependency) frameworks-gui)',
        '<!(<(dependency) system-liblog)',
        '<!(<(dependency) system-libutils)',
        '<!(<(dependency) system-libcutils)',
      ],

      'defines': [
        'LOG_TAG="SensorService"',
      ],

      'sources': [
        '<(local_root)/init_sensorservice.cpp',
        '<(local_root)/BatteryService.cpp',
        '<(local_root)/CorrectedGyroSensor.cpp',
        '<(local_root)/Fusion.cpp',
        '<(local_root)/GravitySensor.cpp',
        '<(local_root)/LinearAccelerationSensor.cpp',
        '<(local_root)/OrientationSensor.cpp',
        '<(local_root)/RotationVectorSensor.cpp',
        '<(local_root)/SensorDevice.cpp',
        '<(local_root)/SensorFusion.cpp',
        '<(local_root)/SensorInterface.cpp',
        '<(local_root)/SensorService.cpp',
      ],
    },
  ],
}
