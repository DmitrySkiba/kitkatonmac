#!/usr/bin/env python

import os
import sys
import optparse

import build_utils
import md5_check


def _find_input_files(options):
  inputs = []
  if options.aapt_M:
    inputs.append(options.aapt_M)
  if options.aapt_S:
    inputs += build_utils.find_in_paths([options.aapt_S])
  if options.aapt_A:
    inputs += build_utils.find_in_paths([options.aapt_A])
  return inputs


def main(arguments):
  aapt_dest_ = 'aapt_'
  aapt_options = {
    '-x': {'dest': aapt_dest_ + 'x', 'action': 'store_true'},
    '-z': {'dest': aapt_dest_ + 'z', 'action': 'store_true'},
    '-f': {'dest': aapt_dest_ + 'f', 'action': 'store_true'},
    '-c': {'dest': aapt_dest_ + 'c'},
    '-M': {'dest': aapt_dest_ + 'M'},
    '-S': {'dest': aapt_dest_ + 'S'},
    '-A': {'dest': aapt_dest_ + 'A'},
    '--min-sdk-version': {'dest': aapt_dest_ + 'min_sdk_version'},
    '--target-sdk-version': {'dest': aapt_dest_ + 'target_sdk_version'},
    '--product': {'dest': aapt_dest_ + 'product'},
    '--version-code': {'dest': aapt_dest_ + 'version_code'},
    '--version-name': {'dest': aapt_dest_ + 'version_name'},
    '-P': {'dest': aapt_dest_ + 'P'},
    '-F': {'dest': aapt_dest_ + 'F'},
    '-m': {'dest': aapt_dest_ + 'm', 'action': 'store_true'},
    '-J': {'dest': aapt_dest_ + 'J'},
  }

  aapt_key_by_dest = {}
  aapt_option_list = []
  for key, info in aapt_options.iteritems():
    aapt_option_list.append(optparse.make_option(key, **info))
    aapt_key_by_dest[info['dest']] = key

  parser = optparse.OptionParser(option_list = aapt_option_list)
  build_utils.add_required_option(parser, '--aapt-tool')
  parser.add_option('--print-gyp-inputs', action = 'store_true')
  parser.add_option('--print-gyp-outputs', action = 'store_true')
  parser.add_option('--stamp-file')

  options, _ = parser.parse_args(arguments)
  build_utils.check_required_options(options, parser)

  # Handle --print-gyp-XXX options and exit
  if options.print_gyp_inputs:
    print '\n'.join(_find_input_files(options))
    return
  elif options.print_gyp_outputs:
    if options.aapt_P:
      print options.aapt_P
    if options.aapt_F:
      print options.aapt_F
    return

  # Main course

  cmd = [options.aapt_tool, 'package']

  for dest, value in options.__dict__.items():
    if dest not in aapt_key_by_dest or value is None:
      continue
    cmd.append(aapt_key_by_dest[dest])
    if type(value) is not bool:
      cmd.append(value)

  def _aapt():
    if options.aapt_J and not os.path.isdir(options.aapt_J):
      os.makedirs(options.aapt_J)
    build_utils.check_call_die(cmd)

  if options.stamp_file:
    md5_check.CallAndRecordIfStale(
        _aapt,
        record_path = options.stamp_file,
        input_paths = _find_input_files(options),
        input_strings = cmd)
  else:
    _aapt()


if __name__ == '__main__':
  sys.exit(main(sys.argv))
