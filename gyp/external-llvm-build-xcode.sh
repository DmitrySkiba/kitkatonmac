#!/bin/sh

# When invoked from Xcode, python build script makes cmake freeze in execute_process()
# function. I spent days trying to find a fix, but failed. Maybe next versions of Xcode,
# python or cmake will magically fix the issue, but for now I'm fine with this build
# script used exclusively from Xcode.

set -e

if [ "$#" -lt 3 ]; then
    echo "Wrong number of arguments (expected <llvmPath> <buildPath> <buildType>)."
    exit 1
fi

llvmPath=$1
buildPath=$2
buildType=$3

case "$llvmPath" in
   /*)
      ;;
   *)
      llvmPath=$(pwd)/$llvmPath
      ;;
esac

if [ ! -d "$llvmPath" ]; then
    echo "'$llvmPath' doesn't exist!"
    exit 1
fi

case "$buildPath" in
   /*)
      ;;
   *)
      buildPath=$(pwd)/$buildPath
      ;;
esac

stampFile="$buildPath/.stamp"

if [ -f "$stampFile" ]; then
  echo "Nothing to do."
  exit 0
fi

rm -rf "$buildPath"
mkdir -p "$buildPath"
cd "$buildPath"

if [ "$XCODE_VERSION_MAJOR" ]; then
    export PATH=$(bash -l -c 'echo $PATH')
    unset MACOSX_DEPLOYMENT_TARGET
fi

cmake \
    -G Ninja \
    -DLLVM_BUILD_32_BITS=ON \
    -DLLVM_ENABLE_CXX11=ON \
    -DLLVM_ENABLE_LIBCXX=ON \
    -DLLVM_REQUIRES_RTTI=ON \
    -DLLVM_TARGETS_TO_BUILD="ARM;X86;Mips" \
    -DCMAKE_BUILD_TYPE="$buildType" \
    "$llvmPath"
ninja

touch "$stampFile"
