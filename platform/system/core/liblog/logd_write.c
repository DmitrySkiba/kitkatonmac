#include <android/log.h>
#include <log/logd.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#define NOT_IMPLEMENTED(what) \
    static volatile int counter = 0; \
    if ((counter += 1) < 3) { \
        __android_log_write(ANDROID_LOG_ERROR, \
                            "logd_write", what " is not implemented!"); \
    }

#define LOG_BUF_SIZE 1024

int __android_log_write(int prio, const char *tag, const char *text) {
    if (prio == ANDROID_LOG_VERBOSE) {
        return 0;
    }
    const char* prioString = NULL;
    char prioStringBuffer[6];
    switch (prio) {
        case ANDROID_LOG_VERBOSE:   prioString = "....V"; break;
        case ANDROID_LOG_DEBUG:     prioString = "DEBUG"; break;
        case ANDROID_LOG_INFO:      prioString = ".INFO"; break;
        case ANDROID_LOG_WARN:      prioString = ".WARN"; break;
        case ANDROID_LOG_ERROR:     prioString = "ERROR"; break;
        case ANDROID_LOG_FATAL:     prioString = "FATAL"; break;
        default:
            snprintf(prioStringBuffer, sizeof(prioStringBuffer), "?% 3d?", prio);
            prioString = prioStringBuffer;
            break;
    }

    static bool stdoutBufferingDisabled = false;
    if (!stdoutBufferingDisabled) {
      stdoutBufferingDisabled = true;
      setvbuf(stdout, NULL, _IONBF, 0);
    }

    return printf("%s/%s: %s\n", prioString, tag, text);
}

int __android_log_print(int prio, const char *tag,  const char *fmt, ...) {
    va_list args;
    va_start(args, fmt);
    __android_log_vprint(prio, tag, fmt, args);
    va_end(args);
    return 0;
}

int __android_log_vprint(int prio, const char *tag, const char *fmt, va_list args) {
    char msg[LOG_BUF_SIZE];
    vsnprintf(msg, LOG_BUF_SIZE, fmt, args);
    return __android_log_write(prio, tag, msg);
}

void __android_log_assert(const char *cond, const char *tag, const char *fmt, ...) {
    char buf[LOG_BUF_SIZE];

    if (fmt) {
        va_list ap;
        va_start(ap, fmt);
        vsnprintf(buf, LOG_BUF_SIZE, fmt, ap);
        va_end(ap);
    } else {
        /* Msg not provided, log condition.  N.B. Do not use cond directly as
         * format string as it could contain spurious '%' syntax (e.g.
         * "%d" in "blocks%devs == 0").
         */
        if (cond)
            snprintf(buf, LOG_BUF_SIZE, "Assertion failed: %s", cond);
        else
            strcpy(buf, "Unspecified assertion failed");
    }

    __android_log_write(ANDROID_LOG_FATAL, tag, buf);
    __builtin_trap(); /* trap so we have a chance to debug the situation */
    /* NOTREACHED */
}

int __android_log_bwrite(int32_t tag, const void *payload, size_t len) {
    NOT_IMPLEMENTED("__android_log_bwrite");
    return -1;
}

int __android_log_btwrite(int32_t tag, char type, const void *payload, size_t len) {
    NOT_IMPLEMENTED("__android_log_btwrite");
    return -1;
}

int __android_log_buf_write(int bufID, int prio, const char *tag, const char *text) {
    return __android_log_write(prio, tag, text);
}

int __android_log_buf_print(int bufID, int prio, const char *tag, const char *fmt, ...) {
    va_list ap;
    char buf[LOG_BUF_SIZE];

    va_start(ap, fmt);
    vsnprintf(buf, LOG_BUF_SIZE, fmt, ap);
    va_end(ap);

    return __android_log_buf_write(bufID, prio, tag, buf);
}
