#include <pthread.h>
#include <cutils/sched_policy.h>
#include <binder/IServiceManager.h>
#include "SurfaceFlinger.h"

using namespace android;

static void* surfaceflinger_thread(void*) {
    sp<SurfaceFlinger> flinger = new SurfaceFlinger();

#if defined(HAVE_PTHREADS)
    setpriority(PRIO_PROCESS, 0, PRIORITY_URGENT_DISPLAY);
#endif
    set_sched_policy(0, SP_FOREGROUND);

    // initialize before clients can connect
    flinger->init();

    // publish surface flinger
    sp<IServiceManager> sm(defaultServiceManager());
    sm->addService(String16(SurfaceFlinger::getServiceName()), flinger, false);

    // run in this thread
    flinger->run();

    return 0;
}

extern "C" int init_surfaceflinger(void) {
    pthread_t thread;
    int error = pthread_create(&thread, NULL, &surfaceflinger_thread, NULL);
    pthread_detach(thread);
    return error;
}
