#ifndef KHR_API

#ifdef KHR_PREFIX

#define _KHR_APPEND_PREFIX(prefix, name) _KHR_APPEND_PREFIX2(prefix, name)
#define _KHR_APPEND_PREFIX2(prefix, name) prefix ## name

#define KHR_SYMBOL(symbol) _KHR_APPEND_PREFIX(KHR_PREFIX, symbol)
#define KHR_API(ReturnType, function) \
    extern "C" ReturnType KHRONOS_APIENTRY KHR_SYMBOL(function)

#else

#define KHR_SYMBOL(symbol) symbol
#define KHR_API(ReturnType, function) \
    KHRONOS_APICALL ReturnType KHRONOS_APIENTRY KHR_SYMBOL(function)

#endif

#endif // KHR_API
