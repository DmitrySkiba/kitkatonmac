{
  'includes': [
    'common.gypi',
  ],

  'variables': {
    'local_root': '<(platform_root)/frameworks/base/libs/hwui',
  },

  'targets': [
    {
      'target_name': 'frameworks-base-hwui',
      'product_name': 'hwui',
      'type': 'static_library',

      'dependencies': [
        '<!(<(dependency) frameworks-base-include)',
        '<!(<(dependency) frameworks-opengl)',
        '<!(<(dependency) frameworks-ui)',
        '<!(<(dependency) system-liblog)',
        '<!(<(dependency) system-libutils)',
        '<!(<(dependency) system-libcutils)',
        '<!(<(dependency) external-skia)',
      ],

      'direct_dependent_settings': {
        'include_dirs': [
          '<(local_root)',
        ],
        'defines': [
          'USE_OPENGL_RENDERER',
        ],
      },

      'defines': [
        'USE_OPENGL_RENDERER',
        'EGL_EGLEXT_PROTOTYPES',
        'GL_GLEXT_PROTOTYPES',
      ],

      'sources': [
        '<(local_root)/utils/Blur.cpp',
        '<(local_root)/utils/SortedListImpl.cpp',
        '<(local_root)/thread/TaskManager.cpp',
        '<(local_root)/font/CacheTexture.cpp',
        '<(local_root)/font/Font.cpp',
        '<(local_root)/AssetAtlas.cpp',
        '<(local_root)/FontRenderer.cpp',
        '<(local_root)/GammaFontRenderer.cpp',
        '<(local_root)/Caches.cpp',
        '<(local_root)/DisplayList.cpp',
        '<(local_root)/DeferredDisplayList.cpp',
        '<(local_root)/DisplayListLogBuffer.cpp',
        '<(local_root)/DisplayListRenderer.cpp',
        '<(local_root)/Dither.cpp',
        '<(local_root)/Extensions.cpp',
        '<(local_root)/FboCache.cpp',
        '<(local_root)/GradientCache.cpp',
        '<(local_root)/Image.cpp',
        '<(local_root)/Layer.cpp',
        '<(local_root)/LayerCache.cpp',
        '<(local_root)/LayerRenderer.cpp',
        '<(local_root)/Matrix.cpp',
        '<(local_root)/OpenGLRenderer.cpp',
        '<(local_root)/Patch.cpp',
        '<(local_root)/PatchCache.cpp',
        '<(local_root)/PathCache.cpp',
        '<(local_root)/PathTessellator.cpp',
        '<(local_root)/PixelBuffer.cpp',
        '<(local_root)/Program.cpp',
        '<(local_root)/ProgramCache.cpp',
        '<(local_root)/RenderBufferCache.cpp',
        '<(local_root)/ResourceCache.cpp',
        '<(local_root)/SkiaColorFilter.cpp',
        '<(local_root)/SkiaShader.cpp',
        '<(local_root)/Snapshot.cpp',
        '<(local_root)/Stencil.cpp',
        '<(local_root)/Texture.cpp',
        '<(local_root)/TextureCache.cpp',
        '<(local_root)/TextDropShadowCache.cpp',
      ],
    },
  ],
}
