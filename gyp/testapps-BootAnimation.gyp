{
  'includes': [
    'common.gypi',
    'testapps-common.gypi',
  ],

  'variables': {
    'local_root': '<(platform_root)/testapps/BootAnimation',
    'bootanimation_root': '<(platform_root)/frameworks/base/cmds/bootanimation',
  },

  'targets': [
    {
      'target_name': 'testapps-BootAnimation',
      'product_name': 'BootAnimation',
      'type': 'executable',

      'dependencies': [
        '<!(<(dependency) hardware-libhardware)',
        '<!(<(dependency) frameworks-base-androidfw)',
        '<!(<(dependency) frameworks-binder)',
        '<!(<(dependency) frameworks-opengl)',
        '<!(<(dependency) frameworks-surfaceflinger)',
        '<!(<(dependency) external-skia)',
      ],

      'xcode_settings': {
        'INFOPLIST_FILE': '<(local_root)/Info.plist',
      },

      'mac_bundle_resources': [
        '<(local_root)/Window.xib',
        '<(local_root)/android_root',
      ],

      'include_dirs': [
        '<(bootanimation_root)',
      ],

      'defines': [
        'GL_GLEXT_PROTOTYPES',
      ],

      'sources': [
        '<(local_root)/main.m',
        '<(local_root)/AppDelegate.m',
        '<(bootanimation_root)/BootAnimation.cpp',
      ],
    },
  ],
}
