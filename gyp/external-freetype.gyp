{
  'includes': [
    'common.gypi'
  ],

  'variables': {
    'local_root': '<(platform_root)/external/freetype',
  },

  'targets': [
    {
      'target_name': 'external-freetype',
      'product_name': 'freetype',
      'type': 'static_library',

      'dependencies': [
        '<!(<(dependency) external-libpng)',
        '<!(<(dependency) zlib)',
      ],

      'direct_dependent_settings': {
        'include_dirs': [
          '<(local_root)/include',
        ],
      },

      'include_dirs': [
        '<(local_root)/builds',
        '<(local_root)/include',
      ],

      'defines': [
        'DARWIN_NO_CARBON',
        'FT2_BUILD_LIBRARY',
      ],

      'sources': [
        '<(local_root)/src/base/ftbbox.c',
        '<(local_root)/src/base/ftbitmap.c',
        '<(local_root)/src/base/ftfstype.c',
        '<(local_root)/src/base/ftglyph.c',
        '<(local_root)/src/base/ftlcdfil.c',
        '<(local_root)/src/base/ftstroke.c',
        '<(local_root)/src/base/fttype1.c',
        '<(local_root)/src/base/ftxf86.c',
        '<(local_root)/src/base/ftbase.c',
        '<(local_root)/src/base/ftsystem.c',
        '<(local_root)/src/base/ftinit.c',
        '<(local_root)/src/base/ftgasp.c',
        '<(local_root)/src/raster/raster.c',
        '<(local_root)/src/sfnt/sfnt.c',
        '<(local_root)/src/smooth/smooth.c',
        '<(local_root)/src/autofit/autofit.c',
        '<(local_root)/src/truetype/truetype.c',
        '<(local_root)/src/cff/cff.c',
        '<(local_root)/src/psnames/psnames.c',
        '<(local_root)/src/pshinter/pshinter.c',
      ],
    },
  ],
}
