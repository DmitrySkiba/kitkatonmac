{
  'includes': [
    'common.gypi',
  ],

  'variables': {
    'rc_root': '<(platform_root)/opengl/rc',
    'local_root': '<(rc_root)/egl',
  },

  'targets': [
    {
      'target_name': 'opengl-rc-egl',
      'product_name': 'rc-egl',
      'type': 'static_library',

      'dependencies': [
        '<!(<(dependency) system-libutils)',
        '<!(<(dependency) system-liblog)',
        '<!(<(dependency) frameworks-include)',
        '<!(<(dependency) frameworks-opengl)',
        '<!(<(dependency) opengl-rc-shared)',
        '<!(<(dependency) opengl-rc-utils)',
        'hardware-libhardware.gyp:hardware-libhardware-common',
      ],

      'defines': [
        'EGL_EGLEXT_PROTOTYPES',
        'KHR_PREFIX=<(rc_egl_prefix)',
      ],

      'sources': [
        '<(local_root)/egl.cpp',
        '<(local_root)/eglDisplay.cpp',
        '<(local_root)/ThreadInfo.cpp',
      ],
    },
  ],
}
