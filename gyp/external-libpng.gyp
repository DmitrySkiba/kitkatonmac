{
  'includes': [
    'common.gypi'
  ],

  'variables': {
    'local_root': '<(platform_root)/external/libpng',
    'local_headers_root': '<(headers_root)/libpng',
  },

  'targets': [
    {
      'target_name': 'external-libpng',
      'product_name': 'png',
      'type': 'static_library',

      'direct_dependent_settings': {
        'include_dirs': [
          '<(headers_root)',
          '<(local_headers_root)',
        ],
      },

      'xcode_settings': {
        'GCC_C_LANGUAGE_STANDARD': 'gnu89',
        'OTHER_CFLAGS': ['-fvisibility=hidden', '-ftrapv'],
      },

      'include_dirs': [
        '<(local_root)',
      ],

      'sources': [
        '<(local_root)/png.c',
        '<(local_root)/pngerror.c',
        '<(local_root)/pnggccrd.c',
        '<(local_root)/pngget.c',
        '<(local_root)/pngmem.c',
        '<(local_root)/pngpread.c',
        '<(local_root)/pngread.c',
        '<(local_root)/pngrio.c',
        '<(local_root)/pngrtran.c',
        '<(local_root)/pngrutil.c',
        '<(local_root)/pngset.c',
        '<(local_root)/pngtrans.c',
        '<(local_root)/pngvcrd.c',
        '<(local_root)/pngwio.c',
        '<(local_root)/pngwrite.c',
        '<(local_root)/pngwtran.c',
        '<(local_root)/pngwutil.c',
      ],

      'hard_dependency': 1,
      'actions': [
        {
          'action_name': 'headers',
          'message': 'Copying headers...',

          'inputs': [
            '<(local_root)/png.h',
            '<(local_root)/pngconf.h',
            '<(local_root)/pngusr.h',
          ],
          'outputs': ['<(local_headers_root)'],
          'action': [
            'python', 'utils/copy.py',
            '--source', '<(local_root)/png.h',
            '--source', '<(local_root)/pngconf.h',
            '--source', '<(local_root)/pngusr.h',
            '--destination-path' ,'<(local_headers_root)',
          ],
        },
      ],
    },
  ],
}
