#!/usr/bin/env python

import optparse
import os
import sys
import hashlib

import build_utils
import md5_check


def _find_input_files(options):
  inputs = build_utils.parse_gyp_list(options.inputs)
  inputs = [os.path.abspath(s) for s in inputs]
  return build_utils.find_in_paths(inputs, file_filters = ['*.logtags'])


def _generate_output_file(options, input_file):
  # The logtags tool wants an output file path, but all .logtags files are named
  # EventLogTags.logtags, so we can't put them in one folder. So we create uniquely
  # named folder for each input file.
  md5 = hashlib.md5()
  md5.update(input_file)
  output_folder = md5.hexdigest()
  file_name = os.path.splitext(os.path.basename(input_file))[0]
  return os.path.join(options.output_path, output_folder, file_name + '.java')


def _process_logtags(options):
  input_files = _find_input_files(options)

  def _do():
    build_utils.delete_directory(options.output_path)
    build_utils.make_directory(options.output_path)
    for input_file in input_files:
      cmd = [
        options.logtags_tool,
        '-o', _generate_output_file(options, input_file),
        input_file,
        options.merged_tags_file
      ]
      build_utils.check_call_die(cmd)

  if options.stamp_file:
    md5_check.CallAndRecordIfStale(
        _do,
        record_path = options.stamp_file,
        input_paths = input_files)
  else:
    _do()


def main(arguments):
  parser = optparse.OptionParser()
  build_utils.add_required_option(parser, '--logtags-tool')
  parser.add_option('--print-gyp-inputs', action='store_true')
  parser.add_option('--print-gyp-outputs', action='store_true')
  build_utils.add_required_option(parser, '--inputs')
  build_utils.add_required_option(parser, '--merged-tags-file')
  build_utils.add_required_option(parser, '--output-path')
  parser.add_option('--stamp-file')

  options, _ = parser.parse_args(arguments)
  build_utils.check_required_options(options, parser)

  if options.print_gyp_inputs:
    print '\n'.join(_find_input_files(options))
  elif options.print_gyp_outputs:
    for input_file in _find_input_files(options):
      print _generate_output_file(options, input_file)
    if options.stamp_file:
      print options.stamp_file
  else:
    _process_logtags(options)


if __name__ == '__main__':
  sys.exit(main(sys.argv))
