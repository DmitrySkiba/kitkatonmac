#import "AppDelegate.h"

#include <android/native_window.h>
#include <cutils/memory.h>
#include <utils/Log.h>
#include <gui/Surface.h>
#include <gui/SurfaceComposerClient.h>
#include <gui/ISurfaceComposer.h>
#include <ui/DisplayInfo.h>
#include "../../opengl/rc/shared/RenderControl.h"

using namespace android;

extern "C" void initFrameBuffer(void* window, int width, int height);
extern "C" int init_surfaceflinger(void);

@implementation AppDelegate

@synthesize window = _window;

- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)sender
{
    return YES;
}

- (void)applicationDidFinishLaunching:(NSNotification *)notification
{
    NSSize size = self.window.frame.size;
    int width = (int)size.width;
    int height = (int)size.height;

    initFrameBuffer(self.window, width, height);

#if 1

    init_surfaceflinger();
    [NSThread detachNewThreadSelector:@selector(_testSurfaceFlinger) toTarget:self withObject:nil];

#else

    uint32_t config = 5;
    GLenum glFormat = GL_RGBA;
    GLenum glType = GL_UNSIGNED_BYTE;

    uint32_t windowSurface = rcCreateWindowSurface(config, width, height);
    uint32_t context = rcCreateContext(config, 0, 1);

    uint32_t colorBuffer = rcCreateColorBuffer(width, height, glFormat);
    
    int width2 = width / 2;
    int height2 = height / 2;
    uint32_t colorBuffer2 = rcCreateColorBuffer(width2, height2, glFormat);
    //rcOpenColorBuffer(colorBuffer);
    {
        size_t size = width2 * height2 * sizeof(uint32_t);
        uint32_t* pixels = (uint32_t*)malloc(size);
        android_memset32(pixels, 0xFFFFFFF0, size);
        rcUpdateColorBuffer(colorBuffer2, 0, 0, width2, height2, glFormat, glType, pixels);
        free(pixels);
    }
    //rcCloseColorBuffer(colorBuffer);

    rcMakeCurrent(context, windowSurface, windowSurface);
    rcSetWindowColorBuffer(windowSurface, colorBuffer);

    glViewport(0, 0, width, height);
    glClearColor(1, 1, 0.6f, 1);
    glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

    rcFlushWindowColorBuffer(windowSurface);
    
    rcBindTexture(colorBuffer2);
    
    rcFBPost(colorBuffer);

#endif
}

static void fillSurfaceRGBA8(const sp<SurfaceControl>& sc, uint8_t r, uint8_t g, uint8_t b) {
    ANativeWindow_Buffer outBuffer;
    sp<Surface> s = sc->getSurface();
    s->lock(&outBuffer, NULL);
    uint8_t* img = reinterpret_cast<uint8_t*>(outBuffer.bits);
    for (uint32_t y = 0; y < outBuffer.height; y++) {
        for (uint32_t x = 0; x < outBuffer.width; x++) {
            uint8_t* pixel = img + (4 * (y*outBuffer.stride + x));
            pixel[0] = r;
            pixel[1] = g;
            pixel[2] = b;
            pixel[3] = 255;
        }
    }
    s->unlockAndPost();
}

- (void)_testSurfaceFlinger
{
    sp<IBinder> display(SurfaceComposerClient::getBuiltInDisplay(ISurfaceComposer::eDisplayIdMain));
    DisplayInfo info;
    SurfaceComposerClient::getDisplayInfo(display, &info);

    // create a client to surfaceflinger
    sp<SurfaceComposerClient> client = new SurfaceComposerClient();
    client->initCheck();

    sp<SurfaceControl> surfaceControl = client->createSurface(
        String8("test"),
        100, 50, PIXEL_FORMAT_RGBA_8888, 0);
    ALOG_ASSERT(surfaceControl->isValid());

    SurfaceComposerClient::openGlobalTransaction();
    surfaceControl->setLayer(INT_MAX - 1);
    surfaceControl->setPosition(50, 100);
    surfaceControl->show();
    SurfaceComposerClient::closeGlobalTransaction(true);

    fillSurfaceRGBA8(surfaceControl, 195, 63, 63);

    sleep(10000);
}

@end
