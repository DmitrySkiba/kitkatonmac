#!/bin/sh

aapt=/Volumes/A/DCF/toolchain/macosx/android-sdk/build-tools/19.0.1/aapt
platform_root=/Volumes/E/NewWork/KitKat/platform
out_root=/Volumes/E/NewWork/KitKat/out/manual/aapt

if [ ! -d "$out_root" ]; then
    mkdir -p "$out_root"
fi

gyp/java/aapt_package.py \
    --aapt-tool $aapt \
    -x -z -f \
    -c en_US,en_US,normal,hdpi,xhdpi,xxhdpi,nodpi \
    -M $platform_root/frameworks/base/core/res/AndroidManifest.xml \
    -S $platform_root/frameworks/base/core/res/res \
    -A $platform_root/frameworks/base/core/res/assets \
    --min-sdk-version=19 \
    --target-sdk-version=19 \
    --product default \
    --version-code=19 \
    --version-name kk \
    -P $out_root/public_resources.xml \
    -F $out_root/framework-res.apk \
    -m \
    -J $out_root \
    --stamp-file=$out_root/.stamp