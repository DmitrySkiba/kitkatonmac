# Arguments:
# logtags.inputs            REQUIRED, array
# logtags.merged_tags_file
# logtags.output_path       REQUIRED
# logtags.stamp_file

{
  'variables': {
    'my_path': '.',
    'logtags.stamp_file%': '',
    'logtags.merged_tags_file%': 'all-event-log-tags.txt',
  },

  'actions': [
    {
      'variables': {
        'invocation': [
            'python', '<(my_path)/logtags.py',
            '--logtags-tool=<(logtags_tool)',
            '--inputs=<(logtags.inputs)',
            '--merged-tags-file=<(logtags.merged_tags_file)',
            '--output-path=<(logtags.output_path)',
        ],
        'conditions': [
          [ '"<(logtags.stamp_file)" != ""', {
            'invocation+': [ '--stamp-file', '<(logtags.stamp_file)' ],
          }],
        ]
      },

      'action_name': 'logtags-<(_target_name)',
      'message': 'Processing logtags...',

      'inputs': [ '<!@(<(invocation) --print-gyp-inputs)' ],
      'outputs': [ '<!@(<(invocation) --print-gyp-outputs)' ],

      'action': [ '<@(invocation)' ],
    },
  ],
}
