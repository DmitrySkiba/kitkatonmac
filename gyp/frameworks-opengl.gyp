{ #TODO merge into frameworks-include or rename to opengl-include
  'includes': [
    'common.gypi'
  ],

  'variables': {
    'local_root': '<(platform_root)/frameworks/native/opengl',
  },

  'targets': [
    {
      'target_name': 'frameworks-opengl',
      'type': 'none',

      'direct_dependent_settings': {
        'include_dirs': [
          '<(local_root)/include',
        ],
      },
    },
  ],
}
