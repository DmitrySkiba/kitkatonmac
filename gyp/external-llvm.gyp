{
  'includes': [
    'common.gypi',
  ],

  'variables': {
    'local_root': '<(platform_root)/external/llvm',
    'local_build_root': '<(INTERMEDIATE_DIR)',
    'local_headers_root': '<(headers_root)',

    'built_headers': '<(local_build_root)/include/llvm',
    'build_libraries': [
      '<(local_build_root)/lib/libLLVMARMAsmParser.a',
      '<(local_build_root)/lib/libLLVMARMAsmPrinter.a',
      '<(local_build_root)/lib/libLLVMARMCodeGen.a',
      '<(local_build_root)/lib/libLLVMARMDesc.a',
      '<(local_build_root)/lib/libLLVMARMDisassembler.a',
      '<(local_build_root)/lib/libLLVMARMInfo.a',
      '<(local_build_root)/lib/libLLVMAnalysis.a',
      '<(local_build_root)/lib/libLLVMAsmParser.a',
      '<(local_build_root)/lib/libLLVMAsmPrinter.a',
      '<(local_build_root)/lib/libLLVMBitReader.a',
      '<(local_build_root)/lib/libLLVMBitWriter.a',
      '<(local_build_root)/lib/libLLVMCodeGen.a',
      '<(local_build_root)/lib/libLLVMCore.a',
      '<(local_build_root)/lib/libLLVMDebugInfo.a',
      '<(local_build_root)/lib/libLLVMExecutionEngine.a',
      '<(local_build_root)/lib/libLLVMIRReader.a',
      '<(local_build_root)/lib/libLLVMInstCombine.a',
      '<(local_build_root)/lib/libLLVMInstrumentation.a',
      '<(local_build_root)/lib/libLLVMInterpreter.a',
      '<(local_build_root)/lib/libLLVMJIT.a',
      '<(local_build_root)/lib/libLLVMLinker.a',
      '<(local_build_root)/lib/libLLVMMC.a',
      '<(local_build_root)/lib/libLLVMMCDisassembler.a',
      '<(local_build_root)/lib/libLLVMMCJIT.a',
      '<(local_build_root)/lib/libLLVMMCParser.a',
      '<(local_build_root)/lib/libLLVMMipsAsmParser.a',
      '<(local_build_root)/lib/libLLVMMipsAsmPrinter.a',
      '<(local_build_root)/lib/libLLVMMipsCodeGen.a',
      '<(local_build_root)/lib/libLLVMMipsDesc.a',
      '<(local_build_root)/lib/libLLVMMipsDisassembler.a',
      '<(local_build_root)/lib/libLLVMMipsInfo.a',
      '<(local_build_root)/lib/libLLVMObjCARCOpts.a',
      '<(local_build_root)/lib/libLLVMObject.a',
      '<(local_build_root)/lib/libLLVMOption.a',
      '<(local_build_root)/lib/libLLVMRuntimeDyld.a',
      '<(local_build_root)/lib/libLLVMScalarOpts.a',
      '<(local_build_root)/lib/libLLVMSelectionDAG.a',
      '<(local_build_root)/lib/libLLVMSupport.a',
      '<(local_build_root)/lib/libLLVMTableGen.a',
      '<(local_build_root)/lib/libLLVMTarget.a',
      '<(local_build_root)/lib/libLLVMTransformUtils.a',
      '<(local_build_root)/lib/libLLVMVectorize.a',
      '<(local_build_root)/lib/libLLVMX86AsmParser.a',
      '<(local_build_root)/lib/libLLVMX86AsmPrinter.a',
      '<(local_build_root)/lib/libLLVMX86CodeGen.a',
      '<(local_build_root)/lib/libLLVMX86Desc.a',
      '<(local_build_root)/lib/libLLVMX86Disassembler.a',
      '<(local_build_root)/lib/libLLVMX86Info.a',
      '<(local_build_root)/lib/libLLVMX86Utils.a',
      '<(local_build_root)/lib/libLLVMipa.a',
      '<(local_build_root)/lib/libLLVMipo.a',
      '<(local_build_root)/lib/libLTO.a',
      '<(local_build_root)/lib/libgtest.a',
      '<(local_build_root)/lib/libgtest_main.a',
      '<(local_build_root)/lib/libprofile_rt.a'
    ],

    'lib_path': '<(lib_root)/libllvm.a',
  },

  'targets': [
    {
      'target_name': 'external-llvm-headers',
      'type': 'none',

      'direct_dependent_settings': {
        'defines': [
          '__STDC_LIMIT_MACROS=1',
          '__STDC_CONSTANT_MACROS=1',
          '__STDC_FORMAT_MACROS=1',
        ],
        'include_dirs': [
          '<(local_root)/include',
          '<(local_root)/host/include',
        ],
      },
    },
    {
      'target_name': 'external-llvm',
      'type': 'none',

      'dependencies': [
        'external-llvm-headers',
        '<!(<(dependency) zlib)',
        '<!(<(dependency) libcurses)',
      ],

      'export_dependent_settings': [
        'external-llvm-headers',
        '<!(<(dependency) zlib)',
        '<!(<(dependency) libcurses)',
      ],

      'direct_dependent_settings': {
        'defines': [
          '__STDC_LIMIT_MACROS=1',
          '__STDC_CONSTANT_MACROS=1',
          '__STDC_FORMAT_MACROS=1',
        ],
        'include_dirs': [
          '<(local_root)/include',
          '<(local_root)/host/include',
          '<(local_headers_root)',
        ],
        'link_settings': {
          'libraries': [ '<(lib_path)' ],
        },
      },

      'hard_dependency': 1,

      'actions': [
        {
          'action_name': 'llvm',
          'message': 'Building LLVM...',

          'inputs': [],
          'outputs': [ '<@(build_libraries)', '<(built_headers)' ],

          'conditions': [
            [ '"<(GENERATOR)" == "xcode"', {
              # See comment in external-llvm-build-xcode.sh
              'action': [
                './external-llvm-build-xcode.sh',
                '<(local_root)',
                '<(local_build_root)',
                '<(CONFIGURATION_NAME)',
              ]
            }, {
              'action': [
                'python', 'external-llvm-build.py',
                '--llvm-path', '<(local_root)',
                '--build-path', '<(local_build_root)',
                '--build-type', '<(CONFIGURATION_NAME)',
              ]
            }],
          ],
        },
        {
          'action_name': 'llvm-headers',
          'message': 'Copying LLVM headers...',

          'inputs': [ '<(built_headers)' ],
          'outputs': [ '<(local_headers_root)/llvm' ],

          'action': [
            'python', 'utils/copy.py',
            '--source', '<(built_headers)',
            '--destination' ,'<(local_headers_root)/llvm'
          ]
        }
      ],

      'variables': {
        'ar.action_name': 'archive_llvm',
        'ar.message': 'Archiving LLVM libraries...',
        'ar.archive_path': '<(lib_path)',
        'ar.input_files': [ '<@(build_libraries)' ],
      },
      'includes': [ 'utils/ar.gypi' ],
    },
  ],
}
