{
  'includes': [
    'common.gypi',
    'java-common.gypi',
  ],

  'variables': {
    'local_root': '<(platform_root)/frameworks/opt/telephony',
  },

  'targets': [
    {
      'target_name': 'frameworks-opt-telephony_jar',
      'type': 'none',

      'dependencies': [
        '<!(<(dependency) libcore-rt_jar)',
        '<!(<(dependency) frameworks-base-framework_jar)',
        '<!(<(dependency) frameworks-opt-voip_jar)',
      ],

      'variables': {
        'jar_path': '<!(<(jar_path_v) telephony)',
        'dex_path': '<!(<(dex_path_v) telephony)',

        #logtags
        'logtags.inputs': [ '<(local_root)/src/java' ],

        # aidl
        'aidl.inputs': [
          '<(local_root)/src/java',
        ],
        'aidl.includes': [
          '<(local_root)/src/java',
        ],

        # compile_jar
        'compile_jar.source_paths': [
          '<(logtags.output_path)',
          '<(aidl.output_path)',
          '<(local_root)/src/java',
        ],
        'compile_jar.boot_class_paths': '>(dependencies.jar_files)',
        'compile_jar.jar_file': '<(jar_path)',
        'compile_jar.extra_action_inputs': [
          '<(logtags.stamp_file)',
          '<(aidl.stamp_file)',
        ],

        # dex
        'dex.jar_file': '<(jar_path)',
        'dex.dex_file': '<(dex_path)',
      },
      'includes': [
        'utils/logtags.gypi',
        'utils/aidl.gypi',
        'utils/compile_jar.gypi',
        'utils/dex.gypi'
      ],
    }
  ]
}
