/*
* Copyright (C) 2011 The Android Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#include "ThreadInfo.h"
#include <cutils/threads.h>
#include <cutils/log.h>

static thread_store_t s_tls = THREAD_STORE_INITIALIZER;

RenderThreadInfo::RenderThreadInfo() {
    struct Helper {
        static void destruct(void* info) {
            delete static_cast<RenderThreadInfo*>(info);
        }
    };
    ALOG_ASSERT(!thread_store_get(&s_tls));
    thread_store_set(&s_tls, this, Helper::destruct);
}

RenderThreadInfo* RenderThreadInfo::get() {
    RenderThreadInfo* info = static_cast<RenderThreadInfo*>(thread_store_get(&s_tls));
    if (!info) {
        info = new RenderThreadInfo();
    }
    return info;
}
