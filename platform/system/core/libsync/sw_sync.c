#include <sync/sync.h>
#include <sync/sw_sync.h>
#include <cutils/log.h>

#define LOG_UNIMPLEMENTED() \
    LOG_FATAL("%s is NOT implemented!", __FUNCTION__)

int sync_wait(int fd, int timeout)
{
    LOG_UNIMPLEMENTED();
    return -1;
}

int sync_merge(const char *name, int fd1, int fd2)
{
    LOG_UNIMPLEMENTED();
    return -1;
}

struct sync_fence_info_data *sync_fence_info(int fd)
{
    LOG_UNIMPLEMENTED();
    return NULL;
}

struct sync_pt_info *sync_pt_info(struct sync_fence_info_data *info,
                                  struct sync_pt_info *itr)
{
    LOG_UNIMPLEMENTED();
    return NULL;
}

void sync_fence_info_free(struct sync_fence_info_data *info)
{
    free(info);
}

int sw_sync_timeline_create(void)
{
    LOG_UNIMPLEMENTED();
    return -1;
}

int sw_sync_timeline_inc(int fd, unsigned count)
{
    LOG_UNIMPLEMENTED();
    return -1;
}

int sw_sync_fence_create(int fd, const char *name, unsigned value)
{
    LOG_UNIMPLEMENTED();
    return -1;
}
