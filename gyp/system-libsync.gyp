{
  'includes': [
    'common.gypi',
    'system-common.gypi',
  ],

  'variables': {
    'local_root': '<(system_root)/libsync',
  },

  'targets': [
    {
      'target_name': 'system-libsync',
      'product_name': 'libsync',
      'type': 'static_library',

      'sources': [
        '<(local_root)/sw_sync.c',
      ],
    },
  ],
}
