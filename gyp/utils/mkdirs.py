#!/usr/bin/env python

import sys
import os

def main(arguments):
  for dir in arguments[1:]:
    if not os.path.exists(dir):
      os.makedirs(dir)

if __name__ == '__main__':
  main(sys.argv)
