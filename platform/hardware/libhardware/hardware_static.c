/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <hardware/hardware.h>

#include <dlfcn.h>
#include <string.h>
#include <errno.h>

#define LOG_TAG "HAL"
#include <utils/Log.h>

int hw_get_module_by_class(const char *class_id, const char *inst,
                           const struct hw_module_t **module)
{
    int status;
    struct hw_module_t *hmi;

    size_t sym_length = strlen(HAL_MODULE_INFO_SYM_PREFIX_AS_STR) +
                        strlen(class_id) +
                        (inst ? 1 + strlen(inst) : 0) +
                        1;
    char sym[sym_length];

    if (inst) {
        snprintf(sym, sym_length, "%s%s_%s", HAL_MODULE_INFO_SYM_PREFIX_AS_STR, class_id, inst);
    } else {
        snprintf(sym, sym_length, "%s%s", HAL_MODULE_INFO_SYM_PREFIX_AS_STR, class_id);
    }

    hmi = (struct hw_module_t *)dlsym(RTLD_DEFAULT, sym);
    if (hmi == NULL) {
        ALOGE("load: couldn't find symbol %s", sym);
        status = -EINVAL;
        goto done;
    }

    if (strcmp(class_id, hmi->id) != 0) {
        ALOGE("load: id=%s != hmi->id=%s", class_id, hmi->id);
        status = -EINVAL;
        goto done;
    }

    /* success */
    status = 0;

    done:
    if (status != 0) {
        hmi = NULL;
    } else {
        ALOGV("loaded HAL id=%s hmi=%p", class_id, hmi);
    }

    *module = hmi;

    return status;
}

int hw_get_module(const char *id, const struct hw_module_t **module)
{
    return hw_get_module_by_class(id, NULL, module);
}
