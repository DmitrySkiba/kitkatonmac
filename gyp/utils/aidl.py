#!/usr/bin/env python

import optparse
import os
import sys
import re
import itertools

import build_utils
import md5_check


def _find_input_files(options):
  inputs = build_utils.parse_gyp_list(options.inputs)
  inputs = [os.path.abspath(s) for s in inputs]
  return build_utils.find_in_paths(inputs, file_filters = ['*.aidl'])


def _parse_aidl(aidl_file):
  package_matcher = re.compile(
    '^package\s+(' +
      '[a-zA-Z_]{1}[a-zA-Z0-9_]*' +
      '(?:\\.[a-zA-Z_]{1}[a-zA-Z0-9_]*)*' +
    ');$')

  class_name = os.path.splitext(os.path.basename(aidl_file))[0]
  parcelable_matcher = re.compile('^parcelable\s+' + class_name + '\s*;$')

  package = None
  is_parcelable = False
  with open(aidl_file) as file:
    for line in file.readlines():
      line = line.rstrip()
      if not package:
        match = package_matcher.match(line)
        if match:
          package = match.group(1)
      if not is_parcelable:
        is_parcelable = (parcelable_matcher.match(line) is not None)

  if package is None:
    package = ''
  return (package, is_parcelable)


def _generate_output_file(options, input_file):
  (package, is_parcelable) = _parse_aidl(input_file)
  if is_parcelable:
    return None
  output_name = os.path.splitext(os.path.basename(input_file))[0] + '.java'
  output_file = os.path.join(options.output_path, package.replace('.', '/'), output_name)
  return output_file


def _compile_aidls(options):
  input_files = _find_input_files(options)

  cmd_base = [options.aidl_tool]
  if options.fail_on_parcelables:
    cmd_base.append('-b')
  if options.includes:
    includes = build_utils.parse_gyp_list(options.includes)
    cmd_base.extend(('-I' + i for i in includes))
  cmd_base.append('-o' + options.output_path)

  def _do():
    for input_file in input_files:
      output_file = _generate_output_file(options, input_file)
      if not output_file:
        continue
      if os.path.isfile(output_file):
        os.remove(output_file)
      build_utils.check_call_die(cmd_base + [input_file])
      if not os.path.isfile(output_file):
        print "Oops: '{}' was not created!".format(output_file)
        sys.exit(1)

  if options.stamp_file:
    md5_check.CallAndRecordIfStale(
        _do,
        record_path = options.stamp_file,
        input_paths = input_files,
        input_strings = cmd_base)
  else:
    _do()


def main(arguments):
  parser = optparse.OptionParser()
  build_utils.add_required_option(parser, '--aidl-tool')
  build_utils.add_required_option(parser, '--inputs')
  build_utils.add_required_option(parser, '--output-path')
  parser.add_option('--print-gyp-inputs', action='store_true')
  parser.add_option('--print-gyp-outputs', action='store_true')
  parser.add_option('--includes')
  parser.add_option('--fail-on-parcelables', action='store_true')
  parser.add_option('--stamp-file')

  options, _ = parser.parse_args(arguments)
  build_utils.check_required_options(options, parser)

  if options.print_gyp_inputs:
    print '\n'.join(_find_input_files(options))
  elif options.print_gyp_outputs:
    for input_file in _find_input_files(options):
      output_file = _generate_output_file(options, input_file)
      if output_file:
        print output_file
    if options.stamp_file:
      print options.stamp_file
  else:
    _compile_aidls(options)


if __name__ == '__main__':
  sys.exit(main(sys.argv))
