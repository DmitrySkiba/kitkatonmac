#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate> {
    NSWindow* _window;
}

@property (assign) IBOutlet NSWindow *window;

@end
