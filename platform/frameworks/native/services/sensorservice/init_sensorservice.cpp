#include "SensorService.h"

static void* sensorservice_thread(void*) {
    android::SensorService::publishAndJoinThreadPool();
    return nullptr;
}

extern "C" int init_sensorservice(void) {
    pthread_t thread;
    int error = pthread_create(&thread, NULL, &sensorservice_thread, NULL);
    pthread_detach(thread);
    return error;
}
