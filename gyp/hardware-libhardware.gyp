{
  'includes': [
    'common.gypi'
  ],

  'variables': {
    'local_root': '<(platform_root)/hardware/libhardware',
    'local_legacy_root': '<(platform_root)/hardware/libhardware_legacy',
  },

  'targets': [
    {
      'target_name': 'hardware-libhardware-common',
      'type': 'none',

      'direct_dependent_settings': {
        'include_dirs': [
          '<(local_root)/include',
          '<(local_legacy_root)/include',
        ],
        'defines': [
          'STATIC_HAL_MODULES',
        ],
      },
    },

    # Modules
    {
      'target_name': 'hardware-libhardware-gralloc', #TODO remove this and associated source?
      'product_name': 'libhardware-gralloc',
      'type': 'static_library',

      'dependencies': [
        'hardware-libhardware-common',
        '<!(<(dependency) system-libcutils)',
        '<!(<(dependency) system-liblog)',
      ],

      'export_dependent_settings': [
        'hardware-libhardware-common',
      ],

      'defines': [
        'LOG_TAG="gralloc"',
        'STATIC_HAL_MODULE_ID=gralloc', # Must match GRALLOC_HARDWARE_MODULE_ID
      ],

      'sources': [
        '<(local_root)/modules/gralloc/gralloc.cpp',
        '<(local_root)/modules/gralloc/framebuffer.cpp',
        '<(local_root)/modules/gralloc/mapper.cpp',
      ],
    },

    # libhardware
    {
      'target_name': 'hardware-libhardware',
      'product_name': 'libhardware',
      'type': 'static_library',

      'dependencies': [
        'hardware-libhardware-common',
        '<!(<(dependency) system-libcutils)',
        '<!(<(dependency) system-liblog)',

        # Modules
        # 'hardware-libhardware-gralloc', #TODO delete gralloc source?
      ],

      'export_dependent_settings': [
        'hardware-libhardware-common',
        '<!(<(dependency) system-libcutils)',
      ],

      'sources': [
        '<(local_root)/hardware_static.c',
        '<(local_legacy_root)/power/no_power.c',
        '<(local_legacy_root)/uevent/uevent.c',
        '<(local_legacy_root)/vibrator/no_vibrator.c',
      ]
    },
  ],
}
