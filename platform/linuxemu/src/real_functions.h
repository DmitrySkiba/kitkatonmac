#include <unistd.h>
#include <pwd.h>
#include <pthread.h>
#include <sys/stat.h>

#define ENUM_REAL_FUNCTIONS(f) \
    f(setuid) \
    f(getuid) \
    f(seteuid) \
    f(geteuid) \
    f(setreuid) \
    f(chown) \
    f(fchown) \
    f(lchown) \
    f(getpwnam) \
    f(getpwuid) \
    f(getpwent) \
    f(getpwuid_r) \
    f(getpwnam_r) \
    f(pthread_create) \
    f(stat, __DARWIN_SUF_64_BIT_INO_T) \
    f(fstat, __DARWIN_SUF_64_BIT_INO_T) \
    f(lstat, __DARWIN_SUF_64_BIT_INO_T) \

__BEGIN_DECLS

#define DECLARE_REAL_FUNCTION(name, ...) \
    extern __typeof__(name)* real_##name;

ENUM_REAL_FUNCTIONS(DECLARE_REAL_FUNCTION)

#undef DECLARE_REAL_FUNCTION

extern void init_real_functions(void);

__END_DECLS
