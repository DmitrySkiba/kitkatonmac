#ifndef LINUXEMU_LINUX_INPUT_H_
#define LINUXEMU_LINUX_INPUT_H_

/* Minimal set constants needed to compile services/input */

// Device properties
enum {
    INPUT_PROP_BUTTONPAD,
    INPUT_PROP_DIRECT,
    INPUT_PROP_POINTER,
    INPUT_PROP_SEMI_MT,

    INPUT_PROP_MAX, INPUT_PROP_CNT
};

// Event types
enum {
    EV_ABS,
    EV_KEY,
    EV_MSC,
    EV_REL,
    EV_SW,
    EV_SYN,

    EV_MAX, EV_CNT
};

// Synchronization events
enum {
    SYN_DROPPED,
    SYN_MT_REPORT,
    SYN_REPORT,
};

// Keys and buttons
enum {
    BTN_BACK,
    BTN_DIGI,
    BTN_EXTRA,
    BTN_FORWARD,
    BTN_JOYSTICK,
    BTN_LEFT,
    BTN_MIDDLE,
    BTN_MISC,
    BTN_MOUSE,
    BTN_RIGHT,
    BTN_SIDE,
    BTN_STYLUS,
    BTN_STYLUS2,
    BTN_TASK,
    BTN_TOOL_AIRBRUSH,
    BTN_TOOL_BRUSH,
    BTN_TOOL_DOUBLETAP,
    BTN_TOOL_FINGER,
    BTN_TOOL_LENS,
    BTN_TOOL_MOUSE,
    BTN_TOOL_PEN,
    BTN_TOOL_PENCIL,
    BTN_TOOL_QUADTAP,
    BTN_TOOL_RUBBER,
    BTN_TOOL_TRIPLETAP,
    BTN_TOUCH,

    KEY_OK,
};

// Absolute axes
enum {
    ABS_X,
    ABS_Y,
    ABS_PRESSURE,
    ABS_TOOL_WIDTH,
    ABS_DISTANCE,
    ABS_TILT_X,
    ABS_TILT_Y,
    ABS_MT_SLOT,
    ABS_MT_TOUCH_MAJOR,
    ABS_MT_TOUCH_MINOR,
    ABS_MT_WIDTH_MAJOR,
    ABS_MT_WIDTH_MINOR,
    ABS_MT_ORIENTATION,
    ABS_MT_POSITION_X,
    ABS_MT_POSITION_Y,
    ABS_MT_TOOL_TYPE,
    ABS_MT_BLOB_ID,
    ABS_MT_TRACKING_ID,
    ABS_MT_PRESSURE,
    ABS_MT_DISTANCE,

    ABS_MAX, ABS_CNT
};

// Relative axes
enum {
    REL_HWHEEL,
    REL_WHEEL,
    REL_X,
    REL_Y,

    REL_MAX, REL_CNT
};

// Misc events
enum {
    MSC_SCAN,

    MSC_MAX, MSC_CNT
};

// LEDs
enum {
    LED_CAPSL,
    LED_NUML,
    LED_SCROLLL,

    LED_MAX, LED_CNT
};

// MT_TOOL types
enum {
    MT_TOOL_FINGER,
    MT_TOOL_PEN,

    MT_TOOL_MAX, MT_TOOL_CNT
};

#endif /* LINUXEMU_LINUX_INPUT_H_ */
