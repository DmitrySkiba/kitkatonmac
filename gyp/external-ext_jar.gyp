{
  'includes': [
    'common.gypi',
    'java-common.gypi',
  ],

  'variables': {
    'local_root': '<(platform_root)/external',
  },

  'targets': [
    {
      'target_name': 'external-ext_jar',
      'type': 'none',

      'dependencies': [
        '<!(<(dependency) libcore-rt_jar)',
      ],

      'variables': {
        'jar_path': '<!(<(jar_path_v) ext)',
        'dex_path': '<!(<(dex_path_v) ext)',

        'compile_jar.source_paths': [
          '<(local_root)/nist-sip/java',
          '<(local_root)/apache-http/src',
          '<(local_root)/tagsoup/src',
          '<(local_root)/libphonenumber/java/src',
        ],
        'compile_jar.resource_paths': [
          '<(local_root)/libphonenumber/java/src',
        ],
        'compile_jar.boot_class_paths': '>(dependencies.jar_files)',
        'compile_jar.jar_file': '<(jar_path)',

        'dex.jar_file': '<(jar_path)',
        'dex.dex_file': '<(dex_path)',
        'dex.core_library': 1,
      },
      'includes': [ 'utils/compile_jar.gypi', 'utils/dex.gypi' ],
    }
  ]
}