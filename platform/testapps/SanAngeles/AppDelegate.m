#import "AppDelegate.h"
#import <EGL/egl.h>

extern void angeles(EGLNativeWindowType window);

@interface AppDelegate ()

- (void)_runSample;

@end

@implementation AppDelegate

@synthesize window = _window;

- (void)applicationDidFinishLaunching:(NSNotification *)notification
{
    [NSThread detachNewThreadSelector:@selector(_runSample) toTarget:self withObject:nil];
}

- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)sender
{
    return YES;
}

extern void initFrameBuffer(void* window, int width, int height);
extern EGLNativeWindowType android_createDisplaySurface(void);

- (void)_runSample
{
    NSSize size = self.window.frame.size;
    initFrameBuffer(self.window, (int)size.width, (int)size.height);
    angeles(android_createDisplaySurface());//(EGLNativeWindowType)self.window.contentView);
}

@end
