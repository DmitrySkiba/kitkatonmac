#import "AppDelegate.h"
#include <android_runtime/AndroidRuntime.h>
#include <utils/StrongPointer.h>
#include <cutils/properties.h>
#include <list>
#include <utility>

using namespace android;

extern "C" {
    void initFrameBuffer(void* window, int width, int height);
    int init_surfaceflinger(void);
    int init_sensorservice(void);
    bool LoadApp(const char* oat_location, JNIEnv* env);
}

class AppRuntime: public AndroidRuntime {
public:
    AppRuntime(const char* className): mClassName(className) {
    }

    void onStarted() override {
        ALOGV("AppRuntime::onStarted");

        AndroidRuntime* runtime = AndroidRuntime::getRuntime();
        runtime->callMain(mClassName, mClass, 0, nullptr);
    }

    void onVmCreated(JNIEnv* env) override {
        ALOGV("AppRuntime::onVmCreated");

//        bool appLoaded = LoadApp(NULL, env);
//        ALOG_ASSERT(appLoaded);

        char* jniClassName = toSlashClassName(mClassName);
        jclass clazz = env->FindClass(jniClassName);
        ALOG_ASSERT(clazz);
        free(jniClassName);

        mClass = (jclass)env->NewGlobalRef(clazz);
    }

    void addVmArgument(const String8& argument) {
        mVmArguments.push_back(std::make_pair(argument, nullptr));
        auto& argumentPair = mVmArguments.back();
        argumentPair.second = argumentPair.first;
        addVmArguments(1, &argumentPair.second);
    }

private:
    String8 mClassName;
    jclass mClass;
    std::list<std::pair<String8, const char*>> mVmArguments;
};

@implementation AppDelegate

@synthesize window = _window;

- (void)applicationDidFinishLaunching:(NSNotification *)notification
{
    NSSize size = self.window.frame.size;
    initFrameBuffer(self.window, (int)size.width, (int)size.height);

    init_surfaceflinger();
    init_sensorservice();

    NSString* androidFSRoot = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"android_fs"];
    NSString* androidRoot = [androidFSRoot stringByAppendingPathComponent:@"system"];
    NSString* androidData = [androidFSRoot stringByAppendingPathComponent:@"data"];

    setenv("ANDROID_FS_ROOT", [androidFSRoot UTF8String], true);
    setenv("ANDROID_ROOT", [androidRoot UTF8String], true);
    setenv("ANDROID_DATA", [androidData UTF8String], true);

    setenv("EXTERNAL_STORAGE", "/tmp", true);
    setenv("EMULATED_STORAGE_SOURCE", "/tmp", true);
    setenv("EMULATED_STORAGE_TARGET", "/tmp", true);
    setenv("MEDIA_STORAGE", "/tmp", true);
    setenv("SECONDARY_STORAGE", "/tmp", true);
    setenv("DOWNLOAD_CACHE", "/tmp", true);

    property_set("config.disable_location", "true");
    property_set("config.disable_telephony", "true");
    property_set("config.disable_bluetooth", "true");
    property_set("config.disable_media", "true");
    property_set("config.disable_storage", "true");
    property_set("config.disable_noncore", "true");
    property_set("config.disable_network", "true");

    property_set("ro.build.id", "alpha");
    property_set("ro.build.display.id", "alpha");
    property_set("ro.build.version.incremental", "1");
    property_set("ro.build.version.sdk", "19");
    property_set("ro.build.version.codename", "KK");
    property_set("ro.build.version.release", "1.0");
    property_set("ro.build.date", "Fri Jun 13 07:05:49 UTC 2014");
    property_set("ro.build.date.utc", "1402643149");
    property_set("ro.build.type", "user");
    property_set("ro.build.user", "unknown");
    property_set("ro.build.host", "localhost");
    property_set("ro.build.tags", "debug");
    property_set("ro.product.model", "unknown");
    property_set("ro.product.brand", "unknown");
    property_set("ro.product.name", "unknown");
    property_set("ro.product.device", "unknown");
    property_set("ro.product.board", "unknown");
    property_set("ro.product.cpu.abi", "x86");
    property_set("ro.product.manufacturer", "unknown");
    property_set("ro.product.locale.language", "en");
    property_set("ro.product.locale.region", "US");
    property_set("ro.wifi.channels", "");
    property_set("ro.board.platform", "unknown");
    property_set("ro.build.characteristics", "nosdcard");

    property_set("ro.sf.lcd_density", "240");

    NSThread* mainThread = [[NSThread alloc] initWithTarget:self
                                                   selector:@selector(_mainThread)
                                                     object:nil];
    // Default stack size is 512Kb, which is too small to debug some calls
    mainThread.stackSize = 1 * 1024 * 1024;
    [[mainThread autorelease] start];
}

- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)sender
{
    return YES;
}

- (void)_mainThread
{
    pthread_set_next_tgid_np();

    AppRuntime runtime("com.android.server.SystemServer");

    String8 androidData(getenv("ANDROID_DATA"));

    {
        String8 argument;
        argument += "-Ximage:";
        argument += androidData.appendPathCopy("dalvik-cache/boot.art");
        runtime.addVmArgument(argument);
    }
    runtime.addVmArgument(String8("-Xnorelocate"));
    runtime.addVmArgument(String8("-XX:DisableHSpaceCompactForOOM"));

    const char* jdwpPort = getenv("JDWP_PORT");
    if (jdwpPort) {
        runtime.addVmArgument(String8::format(
            "-agentlib:jdwp=transport=dt_socket,address=%s,server=y,suspend=y",
            jdwpPort));
    }

    runtime.start("com.android.internal.os.RuntimeInit", "application");
}

@end
