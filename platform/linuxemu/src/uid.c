#include <unistd.h>
#include <dlfcn.h>
#include <pthread.h>
#include <pwd.h>
#include <sys/stat.h>
#include "private/android_filesystem_config.h"
#include "real_functions.h"
#include "utils.h"

/* Since we are running Android in single process, we need to make sure getuid()
   returns AID_SYSTEM which Android expects (and checks) in many places. So we
   override uid-related functions and translate between our real uid and AID_SYSTEM.
*/

static uid_t real_uid = 0;
static const uid_t fake_uid = AID_SYSTEM;

static uid_t from_real_uid(uid_t uid) {
    return (uid == real_uid) ? fake_uid : uid;
}

static uid_t to_real_uid(uid_t uid) {
    return (uid == fake_uid) ? real_uid : uid;
}

DEFINE_ONCE_INIT_FUNCTION(static void init_uid(void), {
    init_real_functions();
    real_uid = real_getuid();
});

/* unistd.h API */

int setuid(uid_t id) {
    init_uid();

    return real_setuid(to_real_uid(id));
}

uid_t getuid(void) {
    init_uid();

    return from_real_uid(real_getuid());
}

int seteuid(uid_t uid) {
    init_uid();

    return real_seteuid(to_real_uid(uid));
}

uid_t geteuid(void) {
    init_uid();

    return from_real_uid(real_geteuid());
}

int setreuid(uid_t ruid, uid_t euid) {
    init_uid();

    return real_setreuid(to_real_uid(ruid), to_real_uid(euid));
}

int chown(const char* path, uid_t owner, gid_t group) {
    init_uid();

    return real_chown(path, to_real_uid(owner), group);
}

int fchown(int fd, uid_t owner, gid_t group) {
    init_uid();

    return real_fchown(fd, to_real_uid(owner), group);
}

int lchown(const char* path, uid_t owner, gid_t group) {
    init_uid();

    return real_lchown(path, to_real_uid(owner), group);
}

/* pwd.h API */

static struct passwd* from_real_passwd(struct passwd* passwd) {
    if (passwd) {
        passwd->pw_uid = from_real_uid(passwd->pw_uid);
    }
    return passwd;
}

struct passwd* getpwnam(const char* name) {
    init_uid();

    return from_real_passwd(real_getpwnam(name));
}

struct passwd* getpwuid(uid_t uid) {
    init_uid();

    return from_real_passwd(real_getpwuid(to_real_uid(uid)));
}

struct passwd* getpwent(void) {
    init_uid();

    return from_real_passwd(real_getpwent());
}

int getpwuid_r(uid_t uid, struct passwd* pwd, char* buf, size_t bufsize, struct passwd** result) {
    init_uid();

    int error = real_getpwuid_r(to_real_uid(uid), pwd, buf, bufsize, result);
    if (result && *result) {
        from_real_passwd(*result);
    }
    return error;
}

int getpwnam_r(const char* name, struct passwd* pwd, char* buf, size_t bufsize, struct passwd** result) {
    init_uid();

    int error = real_getpwnam_r(name, pwd, buf, bufsize, result);
    if (result && *result) {
        from_real_passwd(*result);
    }
    return error;
}

/* stat.h API */

static void from_real_stat(struct stat* stat) {
    if (stat) {
        stat->st_uid = from_real_uid(stat->st_uid);
    }
}

int stat(const char* path, struct stat* buf) {
    init_uid();

    int error = real_stat(path, buf);
    if (!error) {
        from_real_stat(buf);
    }
    return error;
}

int lstat(const char* path, struct stat* buf) {
    init_uid();

    int error = real_lstat(path, buf);
    if (!error) {
        from_real_stat(buf);
    }
    return error;
}

int fstat(int fd, struct stat* buf) {
    init_uid();

    int error = real_fstat(fd, buf);
    if (!error) {
        from_real_stat(buf);
    }
    return error;
}
