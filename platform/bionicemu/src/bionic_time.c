/*
 * Copyright (C) 2015 Dmitry Skiba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "bionic_time.h"
#include <stdio.h>

#define PRINTF_ONCE(...) \
    static volatile int counter = 0; \
    if ((counter += 1) < 3) printf(__VA_ARGS__)

size_t strftime_tz(char* s, size_t max, const char* format, const struct tm* tm, const struct strftime_locale* lc) {
    PRINTF_ONCE("FIXME: strftime_tz() ignoring locale argument\n");
    return strftime(s, max, format, tm);
}

time_t mktime_tz(struct tm* const tmp, char const* tz) {
    PRINTF_ONCE("FIXME: mktime_tz() ignoring timezone argument %p\n", tz);
    return mktime(tmp);
}

void localtime_tz(const time_t* const timep, struct tm* tmp, const char* tz) {
    PRINTF_ONCE("FIXME: localtime_tz() ignoring timezone argument %p\n", tz);
    localtime_r(timep, tmp);
}

