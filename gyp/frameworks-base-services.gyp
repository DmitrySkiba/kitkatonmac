{
  'includes': [
    'common.gypi',
  ],

  'variables': {
    'local_root': '<(platform_root)/frameworks/base/services',
  },

  'targets': [
    {
      'target_name': 'frameworks-base-services',
      'product_name': 'services',
      'type': 'static_library',

      'dependencies': [
        '<!(<(dependency) hardware-libhardware)',
        '<!(<(dependency) frameworks-include)',
        '<!(<(dependency) frameworks-base-include)',
        '<!(<(dependency) frameworks-opengl)',
        '<!(<(dependency) frameworks-opengl-egl)',
        '<!(<(dependency) frameworks-binder)',
        '<!(<(dependency) frameworks-ui)',
        '<!(<(dependency) frameworks-gui)',
        '<!(<(dependency) system-liblog)',
        '<!(<(dependency) system-libutils)',
        '<!(<(dependency) system-libcutils)',
        '<!(<(dependency) system-libsuspend)',
        '<!(<(dependency) libnativehelper)',
        '<!(<(dependency) external-skia)',
        '<!(<(dependency) external-conscrypt)',
      ],

      'defines': [
        'GL_GLEXT_PROTOTYPES',
        'EGL_EGLEXT_PROTOTYPES',
      ],

      'include_dirs': [
        '<(local_root)',
        '<(platform_root)/frameworks/base/core/jni',
        '<(platform_root)/frameworks/native/services',
      ],

      'sources': [
        '<(local_root)/input/EventHubOSX.cpp',
        '<(local_root)/input/InputApplication.cpp',
        '<(local_root)/input/InputDispatcher.cpp',
        '<(local_root)/input/InputListener.cpp',
        '<(local_root)/input/InputManager.cpp',
        '<(local_root)/input/InputReader.cpp',
        '<(local_root)/input/InputWindow.cpp',
        '<(local_root)/input/PointerController.cpp',
        '<(local_root)/input/SpriteController.cpp',

        '<(local_root)/jni/com_android_server_AlarmManagerService.cpp',
        # '<(local_root)/jni/com_android_server_AssetAtlasService.cpp',
        '<(local_root)/jni/com_android_server_ConsumerIrService.cpp',
        '<(local_root)/jni/com_android_server_input_InputApplicationHandle.cpp',
        '<(local_root)/jni/com_android_server_input_InputManagerService.cpp',
        '<(local_root)/jni/com_android_server_input_InputWindowHandle.cpp',
        '<(local_root)/jni/com_android_server_LightsService.cpp',
        '<(local_root)/jni/com_android_server_power_PowerManagerService.cpp',
        # '<(local_root)/jni/com_android_server_SerialService.cpp',
        '<(local_root)/jni/com_android_server_SystemServer.cpp',
        # '<(local_root)/jni/com_android_server_UsbDeviceManager.cpp',
        # '<(local_root)/jni/com_android_server_UsbHostManager.cpp',
        '<(local_root)/jni/com_android_server_VibratorService.cpp',
        # '<(local_root)/jni/com_android_server_location_GpsLocationProvider.cpp',
        # '<(local_root)/jni/com_android_server_location_FlpHardwareProvider.cpp',
        # '<(local_root)/jni/com_android_server_connectivity_Vpn.cpp',
        '<(local_root)/jni/onload.cpp',
      ],
    },
  ],
}
