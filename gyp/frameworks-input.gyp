{
  'includes': [
    'common.gypi',
  ],

  'variables': {
    'local_root': '<(platform_root)/frameworks/native/libs/input',
  },

  'targets': [
    {
      'target_name': 'frameworks-input',
      'product_name': 'input',
      'type': 'static_library',

      'dependencies': [
        '<!(<(dependency) system-liblog)',
        '<!(<(dependency) system-libutils)',
        '<!(<(dependency) system-libcutils)',
        '<!(<(dependency) frameworks-binder)',
      ],

      'sources': [
        '<(local_root)/Input.cpp',
        '<(local_root)/InputDevice.cpp',
        '<(local_root)/Keyboard.cpp',
        '<(local_root)/KeyCharacterMap.cpp',
        '<(local_root)/KeyLayoutMap.cpp',
        '<(local_root)/VirtualKeyMap.cpp',
        '<(local_root)/InputTransport.cpp',
        '<(local_root)/VelocityControl.cpp',
        '<(local_root)/VelocityTracker.cpp',
      ],
    },
  ],
}
