/*
* Copyright (C) 2011 The Android Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#define GL_GLEXT_PROTOTYPES

#include <cmath>
#include <map>
#include <stdio.h>

#include <GLES/gl.h>
#include <GLES/glext.h>
#include "KHR_API.h"

#include <GLcommon/gldefs.h>
#include <GLcommon/GLDispatch.h>
#include <GLcommon/GLconversion_macros.h>
#include <GLcommon/TranslatorIfaces.h>
#include <GLcommon/FramebufferData.h>
#include <GLcommon/GLESmacros.h>
#include <GLcommon/TextureUtils.h>

#include "GLEScmContext.h"
#include "GLEScmValidate.h"
#include "GLEScmUtils.h"

static EGLiface* s_eglIface = NULL;

static ObjectLocalName TextureLocalName(GLenum target, unsigned int tex) {
    GET_CTX_RET(0);
    return (tex!=0? tex : ctx->getDefaultTextureName(target));
}

static TextureData* getTextureData(ObjectLocalName tex){
    GET_CTX_RET(NULL);

    if(!ctx->shareGroup()->isObject(TEXTURE,tex))
    {
        return NULL;
    }

    TextureData *texData = NULL;
    ObjectDataPtr objData = ctx->shareGroup()->getObjectData(TEXTURE,tex);
    if(!objData.Ptr()){
        texData = new TextureData();
        ctx->shareGroup()->setObjectData(TEXTURE, tex, ObjectDataPtr(texData));
    } else {
        texData = (TextureData*)objData.Ptr();
    }
    return texData;
}

static TextureData* getTextureTargetData(GLenum target){
    GET_CTX_RET(NULL);
    unsigned int tex = ctx->getBindedTexture(target);
    return getTextureData(TextureLocalName(target,tex));
}

KHR_API(GLboolean, glIsBuffer)(GLuint buffer) {
    GET_CTX_RET(GL_FALSE)

    if(buffer && ctx->shareGroup().Ptr()) {
       ObjectDataPtr objData = ctx->shareGroup()->getObjectData(VERTEXBUFFER,buffer);
       return objData.Ptr() ? ((GLESbuffer*)objData.Ptr())->wasBinded():GL_FALSE;
    }
    return GL_FALSE;
}

KHR_API(GLboolean, glIsEnabled)(GLenum cap) {
    GET_CTX_CM_RET(GL_FALSE)
    RET_AND_SET_ERROR_IF(!GLEScmValidate::capability(cap,ctx->getMaxLights(),ctx->getMaxClipPlanes()),GL_INVALID_ENUM,GL_FALSE);

    if (cap == GL_POINT_SIZE_ARRAY_OES)
        return ctx->isArrEnabled(cap);
    else if (cap==GL_TEXTURE_GEN_STR_OES)
        return (ctx->dispatcher().glIsEnabled(GL_TEXTURE_GEN_S) &&
                ctx->dispatcher().glIsEnabled(GL_TEXTURE_GEN_T) &&
                ctx->dispatcher().glIsEnabled(GL_TEXTURE_GEN_R));
    else
        return ctx->dispatcher().glIsEnabled(cap);
}

KHR_API(GLboolean, glIsTexture)(GLuint texture) {
    GET_CTX_RET(GL_FALSE)

    if(texture == 0) // Special case
        return GL_FALSE;

    TextureData* tex = getTextureData(texture);
    return tex ? tex->wasBound : GL_FALSE;
}

KHR_API(GLenum, glGetError)(void) {
    GET_CTX_RET(GL_NO_ERROR)
    GLenum err = ctx->getGLerror();
    if(err != GL_NO_ERROR) {
        ctx->setGLerror(GL_NO_ERROR);
        return err;
    }

    return ctx->dispatcher().glGetError();
}

KHR_API(const GLubyte *, glGetString)(GLenum name) {

    GET_CTX_RET(NULL)
    switch(name) {
        case GL_VENDOR:
            return (const GLubyte*)ctx->getVendorString();
        case GL_RENDERER:
            return (const GLubyte*)ctx->getRendererString();
        case GL_VERSION:
            return (const GLubyte*)ctx->getVersionString();
        case GL_EXTENSIONS:
            return (const GLubyte*)ctx->getExtensionString();
        default:
            RET_AND_SET_ERROR_IF(true,GL_INVALID_ENUM,NULL);
    }
}

KHR_API(void, glActiveTexture)(GLenum texture) {
    GET_CTX_CM()
    SET_ERROR_IF(!GLEScmValidate::textureEnum(texture,ctx->getMaxTexUnits()),GL_INVALID_ENUM);
    ctx->setActiveTexture(texture);
    ctx->dispatcher().glActiveTexture(texture);
}

KHR_API(void, glAlphaFunc)(GLenum func, GLclampf ref) {
    GET_CTX()
    SET_ERROR_IF(!GLEScmValidate::alphaFunc(func),GL_INVALID_ENUM);
    ctx->dispatcher().glAlphaFunc(func,ref);
}


KHR_API(void, glAlphaFuncx)(GLenum func, GLclampx ref) {
    GET_CTX()
    SET_ERROR_IF(!GLEScmValidate::alphaFunc(func),GL_INVALID_ENUM);
    ctx->dispatcher().glAlphaFunc(func,X2F(ref));
}


KHR_API(void, glBindBuffer)(GLenum target, GLuint buffer) {
    GET_CTX()
    SET_ERROR_IF(!GLEScmValidate::bufferTarget(target),GL_INVALID_ENUM);

    //if buffer wasn't generated before,generate one
    if(buffer && ctx->shareGroup().Ptr() && !ctx->shareGroup()->isObject(VERTEXBUFFER,buffer)){
        ctx->shareGroup()->genName(VERTEXBUFFER,buffer);
        ctx->shareGroup()->setObjectData(VERTEXBUFFER,buffer,ObjectDataPtr(new GLESbuffer()));
    }
    ctx->bindBuffer(target,buffer);
    if (buffer) {
        GLESbuffer* vbo = (GLESbuffer*)ctx->shareGroup()->getObjectData(VERTEXBUFFER,buffer).Ptr();
        vbo->setBinded();
    }
}


KHR_API(void, glBindTexture)(GLenum target, GLuint texture) {
    GET_CTX()
    SET_ERROR_IF(!GLEScmValidate::textureTarget(target),GL_INVALID_ENUM)

    //for handling default texture (0)
    ObjectLocalName localTexName = TextureLocalName(target,texture);

    GLuint globalTextureName = localTexName;
    if(ctx->shareGroup().Ptr()){
        globalTextureName = ctx->shareGroup()->getGlobalName(TEXTURE,localTexName);
        //if texture wasn't generated before,generate one
        if(!globalTextureName){
            ctx->shareGroup()->genName(TEXTURE,localTexName);
            globalTextureName = ctx->shareGroup()->getGlobalName(TEXTURE,localTexName);
        }

        TextureData* texData = getTextureData(localTexName);
        if (texData->target==0)
            texData->target = target;
        //if texture was already bound to another target
        SET_ERROR_IF(ctx->GLTextureTargetToLocal(texData->target) != ctx->GLTextureTargetToLocal(target), GL_INVALID_OPERATION);
        texData->wasBound = true;
    }

    ctx->setBindedTexture(target,texture);
    ctx->dispatcher().glBindTexture(target,globalTextureName);
}

KHR_API(void, glBlendFunc)(GLenum sfactor, GLenum dfactor) {
    GET_CTX()
    SET_ERROR_IF(!GLEScmValidate::blendSrc(sfactor) || !GLEScmValidate::blendDst(dfactor),GL_INVALID_ENUM)
    ctx->dispatcher().glBlendFunc(sfactor,dfactor);
}

KHR_API(void, glBufferData)(GLenum target, GLsizeiptr size, const GLvoid *data, GLenum usage) {
    GET_CTX()
    SET_ERROR_IF(!GLEScmValidate::bufferTarget(target),GL_INVALID_ENUM);
    SET_ERROR_IF(!ctx->isBindedBuffer(target),GL_INVALID_OPERATION);
    ctx->setBufferData(target,size,data,usage);
}

KHR_API(void, glBufferSubData)(GLenum target, GLintptr offset, GLsizeiptr size, const GLvoid *data) {
    GET_CTX()
    SET_ERROR_IF(!ctx->isBindedBuffer(target),GL_INVALID_OPERATION);
    SET_ERROR_IF(!GLEScmValidate::bufferTarget(target),GL_INVALID_ENUM);
    SET_ERROR_IF(!ctx->setBufferSubData(target,offset,size,data),GL_INVALID_VALUE);
}

KHR_API(void, glClear)(GLbitfield mask) {
    GET_CTX()
    ctx->drawValidate();

    ctx->dispatcher().glClear(mask);
}

KHR_API(void, glClearColor)(GLclampf red, GLclampf green, GLclampf blue, GLclampf alpha) {
    GET_CTX()
    ctx->dispatcher().glClearColor(red,green,blue,alpha);
}

KHR_API(void, glClearColorx)(GLclampx red, GLclampx green, GLclampx blue, GLclampx alpha) {
    GET_CTX()
    ctx->dispatcher().glClearColor(X2F(red),X2F(green),X2F(blue),X2F(alpha));
}


KHR_API(void, glClearDepthf)(GLclampf depth) {
    GET_CTX()
    ctx->dispatcher().glClearDepth(depth);
}

KHR_API(void, glClearDepthx)(GLclampx depth) {
    GET_CTX()
    ctx->dispatcher().glClearDepth(X2F(depth));
}

KHR_API(void, glClearStencil)(GLint s) {
    GET_CTX()
    ctx->dispatcher().glClearStencil(s);
}

KHR_API(void, glClientActiveTexture)(GLenum texture) {
    GET_CTX_CM()
    SET_ERROR_IF(!GLEScmValidate::textureEnum(texture,ctx->getMaxTexUnits()),GL_INVALID_ENUM);
    ctx->setClientActiveTexture(texture);
    ctx->dispatcher().glClientActiveTexture(texture);

}

KHR_API(void, glClipPlanef)(GLenum plane, const GLfloat *equation) {
    GET_CTX()
    GLdouble tmpEquation[4];

    for(int i = 0; i < 4; i++) {
         tmpEquation[i] = static_cast<GLdouble>(equation[i]);
    }
    ctx->dispatcher().glClipPlane(plane,tmpEquation);
}

KHR_API(void, glClipPlanex)(GLenum plane, const GLfixed *equation) {
    GET_CTX()
    GLdouble tmpEquation[4];
    for(int i = 0; i < 4; i++) {
        tmpEquation[i] = X2D(equation[i]);
    }
    ctx->dispatcher().glClipPlane(plane,tmpEquation);
}

KHR_API(void, glColor4f)(GLfloat red, GLfloat green, GLfloat blue, GLfloat alpha) {
    GET_CTX()
    ctx->dispatcher().glColor4f(red,green,blue,alpha);
}

KHR_API(void, glColor4ub)(GLubyte red, GLubyte green, GLubyte blue, GLubyte alpha) {
    GET_CTX()
    ctx->dispatcher().glColor4ub(red,green,blue,alpha);
}

KHR_API(void, glColor4x)(GLfixed red, GLfixed green, GLfixed blue, GLfixed alpha) {
    GET_CTX()
    ctx->dispatcher().glColor4f(X2F(red),X2F(green),X2F(blue),X2F(alpha));
}

KHR_API(void, glColorMask)(GLboolean red, GLboolean green, GLboolean blue, GLboolean alpha) {
    GET_CTX()
    ctx->dispatcher().glColorMask(red,green,blue,alpha);
}

KHR_API(void, glColorPointer)(GLint size, GLenum type, GLsizei stride, const GLvoid *pointer) {
    GET_CTX()
    SET_ERROR_IF(!GLEScmValidate::colorPointerParams(size,stride),GL_INVALID_VALUE);
    SET_ERROR_IF(!GLEScmValidate::colorPointerType(type),GL_INVALID_ENUM);
    ctx->setPointer(GL_COLOR_ARRAY,size,type,stride,pointer);
}

KHR_API(void, glCompressedTexImage2D)(GLenum target, GLint level, GLenum internalformat, GLsizei width, GLsizei height, GLint border, GLsizei imageSize, const GLvoid *data) {
    GET_CTX_CM()
    SET_ERROR_IF(!GLEScmValidate::textureTargetEx(target),GL_INVALID_ENUM);

    doCompressedTexImage2D(ctx, target, level, internalformat,
                                width, height, border,
                                imageSize, data, (void*)glTexImage2D);
}

KHR_API(void, glCompressedTexSubImage2D)(GLenum target, GLint level, GLint xoffset, GLint yoffset, GLsizei width, GLsizei height, GLenum format, GLsizei imageSize, const GLvoid *data) {
    GET_CTX_CM()
    SET_ERROR_IF(!(GLEScmValidate::texCompImgFrmt(format) && GLEScmValidate::textureTargetEx(target)),GL_INVALID_ENUM);
    SET_ERROR_IF(level < 0 || level > log2(ctx->getMaxTexSize()),GL_INVALID_VALUE)

    GLenum uncompressedFrmt;
    unsigned char* uncompressed = uncompressTexture(format,uncompressedFrmt,width,height,imageSize,data,level);
    ctx->dispatcher().glTexSubImage2D(target,level,xoffset,yoffset,width,height,uncompressedFrmt,GL_UNSIGNED_BYTE,uncompressed);
    delete uncompressed;
}

KHR_API(void, glCopyTexImage2D)(GLenum target, GLint level, GLenum internalformat, GLint x, GLint y, GLsizei width, GLsizei height, GLint border) {
    GET_CTX()
    SET_ERROR_IF(!(GLEScmValidate::pixelFrmt(ctx,internalformat) && GLEScmValidate::textureTargetEx(target)),GL_INVALID_ENUM);
    SET_ERROR_IF(border != 0,GL_INVALID_VALUE);
    ctx->dispatcher().glCopyTexImage2D(target,level,internalformat,x,y,width,height,border);
}

KHR_API(void, glCopyTexSubImage2D)(GLenum target, GLint level, GLint xoffset, GLint yoffset, GLint x, GLint y, GLsizei width, GLsizei height) {
    GET_CTX()
    SET_ERROR_IF(!GLEScmValidate::textureTargetEx(target),GL_INVALID_ENUM);
    ctx->dispatcher().glCopyTexSubImage2D(target,level,xoffset,yoffset,x,y,width,height);
}

KHR_API(void, glCullFace)(GLenum mode) {
    GET_CTX()
    ctx->dispatcher().glCullFace(mode);
}

KHR_API(void, glDeleteBuffers)(GLsizei n, const GLuint *buffers) {
    GET_CTX()
    SET_ERROR_IF(n<0,GL_INVALID_VALUE);
    if(ctx->shareGroup().Ptr()) {
        for(int i=0; i < n; i++){
           ctx->shareGroup()->deleteName(VERTEXBUFFER,buffers[i]);
           ctx->unbindBuffer(buffers[i]);
        }
    }
}

KHR_API(void, glDeleteTextures)(GLsizei n, const GLuint *textures) {
    GET_CTX()
    SET_ERROR_IF(n<0,GL_INVALID_VALUE);
    if(ctx->shareGroup().Ptr()) {
        for(int i=0; i < n; i++){
            if(textures[i] != 0)
            {
                TextureData* tData = getTextureData(textures[i]);
                // delete the underlying OpenGL texture but only if this
                // texture is not a target of EGLImage.
                if (!tData || tData->sourceEGLImage == 0) {
                    const GLuint globalTextureName = ctx->shareGroup()->getGlobalName(TEXTURE,textures[i]);
                    ctx->dispatcher().glDeleteTextures(1,&globalTextureName);
                }
                ctx->shareGroup()->deleteName(TEXTURE,textures[i]);
                
                if(ctx->getBindedTexture(GL_TEXTURE_2D) == textures[i])
                    ctx->setBindedTexture(GL_TEXTURE_2D,0);
                if (ctx->getBindedTexture(GL_TEXTURE_CUBE_MAP) == textures[i])
                    ctx->setBindedTexture(GL_TEXTURE_CUBE_MAP,0);
            }
        }
    }
}

KHR_API(void, glDepthFunc)(GLenum func) {
    GET_CTX()
    ctx->dispatcher().glDepthFunc(func);
}

KHR_API(void, glDepthMask)(GLboolean flag) {
    GET_CTX()
    ctx->dispatcher().glDepthMask(flag);
}

KHR_API(void, glDepthRangef)(GLclampf zNear, GLclampf zFar) {
    GET_CTX()
    ctx->dispatcher().glDepthRange(zNear,zFar);
}

KHR_API(void, glDepthRangex)(GLclampx zNear, GLclampx zFar) {
    GET_CTX()
    ctx->dispatcher().glDepthRange(X2F(zNear),X2F(zFar));
}

KHR_API(void, glDisable)(GLenum cap) {
    GET_CTX()
    if (cap==GL_TEXTURE_GEN_STR_OES) {
        ctx->dispatcher().glDisable(GL_TEXTURE_GEN_S);
        ctx->dispatcher().glDisable(GL_TEXTURE_GEN_T);
        ctx->dispatcher().glDisable(GL_TEXTURE_GEN_R);
    }
    else ctx->dispatcher().glDisable(cap);
    if (cap==GL_TEXTURE_2D || cap==GL_TEXTURE_CUBE_MAP_OES)
        ctx->setTextureEnabled(cap,false);
}

KHR_API(void, glDisableClientState)(GLenum array) {
    GET_CTX()
    SET_ERROR_IF(!GLEScmValidate::supportedArrays(array),GL_INVALID_ENUM)

    ctx->enableArr(array,false);
    if(array != GL_POINT_SIZE_ARRAY_OES) ctx->dispatcher().glDisableClientState(array);
}


KHR_API(void, glDrawArrays)(GLenum mode, GLint first, GLsizei count) {
    GET_CTX_CM()
    SET_ERROR_IF(count < 0,GL_INVALID_VALUE)
    SET_ERROR_IF(!GLEScmValidate::drawMode(mode),GL_INVALID_ENUM)

    ctx->drawValidate();

    if(!ctx->isArrEnabled(GL_VERTEX_ARRAY)) return;

    GLESConversionArrays tmpArrs;
    ctx->setupArraysPointers(tmpArrs,first,count,0,NULL,true);
    if(mode == GL_POINTS && ctx->isArrEnabled(GL_POINT_SIZE_ARRAY_OES)){
        ctx->drawPointsArrs(tmpArrs,first,count);
    }
    else
    {
        ctx->dispatcher().glDrawArrays(mode,first,count);
    }
}

KHR_API(void, glDrawElements)(GLenum mode, GLsizei count, GLenum type, const GLvoid *elementsIndices) {
    GET_CTX_CM()
    SET_ERROR_IF(count < 0,GL_INVALID_VALUE)
    SET_ERROR_IF((!GLEScmValidate::drawMode(mode) || !GLEScmValidate::drawType(type)),GL_INVALID_ENUM)
    if(!ctx->isArrEnabled(GL_VERTEX_ARRAY)) return;

    ctx->drawValidate();

    const GLvoid* indices = elementsIndices;
    GLESConversionArrays tmpArrs;
    if(ctx->isBindedBuffer(GL_ELEMENT_ARRAY_BUFFER)) { // if vbo is binded take the indices from the vbo
        const unsigned char* buf = static_cast<unsigned char *>(ctx->getBindedBuffer(GL_ELEMENT_ARRAY_BUFFER));
        indices = buf+reinterpret_cast<uintptr_t>(elementsIndices);
    }

    ctx->setupArraysPointers(tmpArrs,0,count,type,indices,false);
    if(mode == GL_POINTS && ctx->isArrEnabled(GL_POINT_SIZE_ARRAY_OES)){
        ctx->drawPointsElems(tmpArrs,count,type,indices);
    }
    else{
        ctx->dispatcher().glDrawElements(mode,count,type,indices);
    }
}

KHR_API(void, glEnable)(GLenum cap) {
    GET_CTX()
    if (cap==GL_TEXTURE_GEN_STR_OES) {
        ctx->dispatcher().glEnable(GL_TEXTURE_GEN_S);
        ctx->dispatcher().glEnable(GL_TEXTURE_GEN_T);
        ctx->dispatcher().glEnable(GL_TEXTURE_GEN_R);
    }
    else
        ctx->dispatcher().glEnable(cap);
    if (cap==GL_TEXTURE_2D || cap==GL_TEXTURE_CUBE_MAP_OES)
        ctx->setTextureEnabled(cap,true);
}

KHR_API(void, glEnableClientState)(GLenum array) {
    GET_CTX()
    SET_ERROR_IF(!GLEScmValidate::supportedArrays(array),GL_INVALID_ENUM)

    ctx->enableArr(array,true);
    if(array != GL_POINT_SIZE_ARRAY_OES) ctx->dispatcher().glEnableClientState(array);
}

KHR_API(void, glFinish)(void) {
    GET_CTX()
    ctx->dispatcher().glFinish();
}

KHR_API(void, glFlush)(void) {
    GET_CTX()
    ctx->dispatcher().glFlush();
}

KHR_API(void, glFogf)(GLenum pname, GLfloat param) {
    GET_CTX()
    ctx->dispatcher().glFogf(pname,param);
}

KHR_API(void, glFogfv)(GLenum pname, const GLfloat *params) {
    GET_CTX()
    ctx->dispatcher().glFogfv(pname,params);
}

KHR_API(void, glFogx)(GLenum pname, GLfixed param) {
    GET_CTX()
    ctx->dispatcher().glFogf(pname,(pname == GL_FOG_MODE)? static_cast<GLfloat>(param):X2F(param));
}

KHR_API(void, glFogxv)(GLenum pname, const GLfixed *params) {
    GET_CTX()
    if(pname == GL_FOG_MODE) {
        GLfloat tmpParam = static_cast<GLfloat>(params[0]);
        ctx->dispatcher().glFogfv(pname,&tmpParam);
    } else {
        GLfloat tmpParams[4];
        for(int i=0; i< 4; i++) {
            tmpParams[i] = X2F(params[i]);
        }
        ctx->dispatcher().glFogfv(pname,tmpParams);
    }

}

KHR_API(void, glFrontFace)(GLenum mode) {
    GET_CTX()
    ctx->dispatcher().glFrontFace(mode);
}

KHR_API(void, glFrustumf)(GLfloat left, GLfloat right, GLfloat bottom, GLfloat top, GLfloat zNear, GLfloat zFar) {
    GET_CTX()
    ctx->dispatcher().glFrustum(left,right,bottom,top,zNear,zFar);
}

KHR_API(void, glFrustumx)(GLfixed left, GLfixed right, GLfixed bottom, GLfixed top, GLfixed zNear, GLfixed zFar) {
    GET_CTX()
    ctx->dispatcher().glFrustum(X2F(left),X2F(right),X2F(bottom),X2F(top),X2F(zNear),X2F(zFar));
}

KHR_API(void, glGenBuffers)(GLsizei n, GLuint *buffers) {
    GET_CTX()
    SET_ERROR_IF(n<0,GL_INVALID_VALUE);
    if(ctx->shareGroup().Ptr()) {
        for(int i=0; i<n ;i++) {
            buffers[i] = ctx->shareGroup()->genName(VERTEXBUFFER, 0, true);
            //generating vbo object related to this buffer name
            ctx->shareGroup()->setObjectData(VERTEXBUFFER,buffers[i],ObjectDataPtr(new GLESbuffer()));
        }
    }
}

KHR_API(void, glGenTextures)(GLsizei n, GLuint *textures) {
    GET_CTX();
    if(ctx->shareGroup().Ptr()) {
        for(int i=0; i<n ;i++) {
            textures[i] = ctx->shareGroup()->genName(TEXTURE, 0, true);
        }
    }
}

KHR_API(void, glGetBooleanv)(GLenum pname, GLboolean *params) {
    GET_CTX()

    if(ctx->glGetBooleanv(pname, params))
    {
        return;
    }

    GLint i;

    switch(pname)
    {
    case GL_FRAMEBUFFER_BINDING_OES:
    case GL_RENDERBUFFER_BINDING_OES:
        {
            GLint name;
            glGetIntegerv(pname,&name);
            *params = name!=0 ? GL_TRUE: GL_FALSE;
        }
    break;
    case GL_TEXTURE_GEN_STR_OES:
        {
            GLboolean state_s = GL_FALSE;
            GLboolean state_t = GL_FALSE;
            GLboolean state_r = GL_FALSE;
            ctx->dispatcher().glGetBooleanv(GL_TEXTURE_GEN_S,&state_s);
            ctx->dispatcher().glGetBooleanv(GL_TEXTURE_GEN_T,&state_t);
            ctx->dispatcher().glGetBooleanv(GL_TEXTURE_GEN_R,&state_r);
            *params = state_s && state_t && state_r ? GL_TRUE: GL_FALSE;
        }
    break; 
    case GL_NUM_COMPRESSED_TEXTURE_FORMATS:
        *params = (GLboolean)getCompressedFormats(NULL); 
    break;    
    case GL_COMPRESSED_TEXTURE_FORMATS:
        {
            int nparams = getCompressedFormats(NULL);
            if (nparams>0) {
                int * iparams = new int[nparams];
                getCompressedFormats(iparams);
                for (int i=0; i<nparams; i++) params[i] = (GLboolean)iparams[i];
                delete [] iparams;
            }
        }
    break;
    default:
        ctx->dispatcher().glGetBooleanv(pname,params);
    }
}

KHR_API(void, glGetBufferParameteriv)(GLenum target, GLenum pname, GLint *params) {
    GET_CTX()
    SET_ERROR_IF(!(GLEScmValidate::bufferTarget(target) && GLEScmValidate::bufferParam(pname)),GL_INVALID_ENUM);
    SET_ERROR_IF(!ctx->isBindedBuffer(target),GL_INVALID_OPERATION);
    bool ret = true;
    switch(pname) {
    case GL_BUFFER_SIZE:
        ctx->getBufferSize(target,params);
        break;
    case GL_BUFFER_USAGE:
        ctx->getBufferUsage(target,params);
        break;
    }

}

KHR_API(void, glGetClipPlanef)(GLenum pname, GLfloat eqn[4]) {
    GET_CTX()
    GLdouble tmpEqn[4];

    ctx->dispatcher().glGetClipPlane(pname,tmpEqn);
    for(int i =0 ;i < 4; i++){
        eqn[i] = static_cast<GLfloat>(tmpEqn[i]);
    }
}

KHR_API(void, glGetClipPlanex)(GLenum pname, GLfixed eqn[4]) {
    GET_CTX()
    GLdouble tmpEqn[4];

    ctx->dispatcher().glGetClipPlane(pname,tmpEqn);
    for(int i =0 ;i < 4; i++){
        eqn[i] = F2X(tmpEqn[i]);
    }
}

KHR_API(void, glGetFixedv)(GLenum pname, GLfixed *params) {
    GET_CTX()

    if(ctx->glGetFixedv(pname, params))
    {
        return;
    }

    size_t nParams = glParamSize(pname);
    GLfloat fParams[16];
    GLint i;

    switch(pname)
    {
    case GL_FRAMEBUFFER_BINDING_OES:
    case GL_RENDERBUFFER_BINDING_OES:
    case GL_TEXTURE_GEN_STR_OES:
        glGetFloatv(pname,&fParams[0]);
        break;
    case GL_NUM_COMPRESSED_TEXTURE_FORMATS:
        *params = I2X(getCompressedFormats(NULL));
        return;
    break;    
    case GL_COMPRESSED_TEXTURE_FORMATS:
        {
            int nparams = getCompressedFormats(NULL);
            if (nparams>0) {
                int * iparams = new int[nparams];
                getCompressedFormats(iparams);
                for (int i=0; i<nparams; i++) params[i] = I2X(iparams[i]);
                delete [] iparams;
            }
            return;
        }
    break;
    default:
        ctx->dispatcher().glGetFloatv(pname,fParams);
    }

    if (nParams)
    {
        for(size_t i =0 ; i < nParams;i++) {
            params[i] = F2X(fParams[i]);
        }
    }
}

KHR_API(void, glGetFloatv)(GLenum pname, GLfloat *params) {
    GET_CTX()

    if(ctx->glGetFloatv(pname, params))
    {
        return;
    }

    GLint i;

    switch (pname) {
    case GL_FRAMEBUFFER_BINDING_OES:
    case GL_RENDERBUFFER_BINDING_OES:
    case GL_TEXTURE_GEN_STR_OES:
        glGetIntegerv(pname,&i);
        *params = (GLfloat)i;
    break;   
    case GL_NUM_COMPRESSED_TEXTURE_FORMATS:
        *params = (GLfloat)getCompressedFormats(NULL); 
    break;    
    case GL_COMPRESSED_TEXTURE_FORMATS:
        {
            int nparams = getCompressedFormats(NULL);
            if (nparams>0) {
                int * iparams = new int[nparams];
                getCompressedFormats(iparams);
                for (int i=0; i<nparams; i++) params[i] = (GLfloat)iparams[i];
                delete [] iparams;
            }
        }
    break;
    default:
        ctx->dispatcher().glGetFloatv(pname,params);
    }
}

KHR_API(void, glGetIntegerv)(GLenum pname, GLint *params) {
    GET_CTX()

    if(ctx->glGetIntegerv(pname, params))
    {
        return;
    }
    
    GLint i;
    GLfloat f;

    switch(pname)
    {
    case GL_TEXTURE_GEN_STR_OES:
        ctx->dispatcher().glGetIntegerv(GL_TEXTURE_GEN_S,&params[0]);
        break;
    case GL_FRAMEBUFFER_BINDING_OES:
        if (ctx->shareGroup().Ptr()) {
            ctx->dispatcher().glGetIntegerv(pname,&i);
            *params = ctx->shareGroup()->getLocalName(FRAMEBUFFER,i);
        }
        break;
    case GL_RENDERBUFFER_BINDING_OES:
        if (ctx->shareGroup().Ptr()) {
            ctx->dispatcher().glGetIntegerv(pname,&i);
            *params = ctx->shareGroup()->getLocalName(RENDERBUFFER,i);
        }
        break;
    case GL_NUM_COMPRESSED_TEXTURE_FORMATS:
        *params = getCompressedFormats(NULL);
        break;
    case GL_COMPRESSED_TEXTURE_FORMATS:
        getCompressedFormats(params);
        break;
    case GL_MAX_CLIP_PLANES:
        ctx->dispatcher().glGetIntegerv(pname,params);
        if(*params > 6)
        {
            // GLES spec requires only 6, and the ATI driver erronously
            // returns 8 (although it supports only 6). This WAR is simple,
            // compliant and good enough for developers.
            *params = 6;
        }
        break;
    case GL_ALPHA_TEST_REF:
        // Both the ATI and nVidia OpenGL drivers return the wrong answer
        // here. So return the right one.
        ctx->dispatcher().glGetFloatv(pname,&f);
        *params = (int)(f * (float)0x7fffffff);
        break;
    case GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS:
        ctx->dispatcher().glGetIntegerv(pname,params);
        if(*params > 16)
        {
            // GLES spec requires only 2, and the ATI driver erronously
            // returns 32 (although it supports only 16). This WAR is simple,
            // compliant and good enough for developers.
            *params = 16;
        }
        break;

    default:
        ctx->dispatcher().glGetIntegerv(pname,params);
    }
}

KHR_API(void, glGetLightfv)(GLenum light, GLenum pname, GLfloat *params) {
    GET_CTX()
    ctx->dispatcher().glGetLightfv(light,pname,params);
}

KHR_API(void, glGetLightxv)(GLenum light, GLenum pname, GLfixed *params) {
    GET_CTX()
    GLfloat tmpParams[4];

    ctx->dispatcher().glGetLightfv(light,pname,tmpParams);
    switch (pname){
        case GL_AMBIENT:
        case GL_DIFFUSE:
        case GL_SPECULAR:
        case GL_POSITION:
            params[3] = F2X(tmpParams[3]);
        case GL_SPOT_DIRECTION:
            params[2] = F2X(tmpParams[2]);
        case GL_SPOT_EXPONENT:
        case GL_SPOT_CUTOFF:
        case GL_CONSTANT_ATTENUATION:
        case GL_LINEAR_ATTENUATION:
        case GL_QUADRATIC_ATTENUATION:
            params[1] = F2X(tmpParams[1]);
            break;
        default:{
            ctx->setGLerror(GL_INVALID_ENUM);
            return;
        }

    }
    params[0] = F2X(tmpParams[0]);
}

KHR_API(void, glGetMaterialfv)(GLenum face, GLenum pname, GLfloat *params) {
    GET_CTX()
    ctx->dispatcher().glGetMaterialfv(face,pname,params);
}

KHR_API(void, glGetMaterialxv)(GLenum face, GLenum pname, GLfixed *params) {
    GET_CTX()
    GLfloat tmpParams[4];
    ctx->dispatcher().glGetMaterialfv(face,pname,tmpParams);
    switch(pname){
    case GL_AMBIENT:
    case GL_DIFFUSE:
    case GL_SPECULAR:
    case GL_EMISSION:
    case GL_AMBIENT_AND_DIFFUSE:
        params[3] = tmpParams[3];
        params[2] = tmpParams[2];
        params[1] = tmpParams[1];
    case GL_SHININESS:
        params[0] = tmpParams[0];
        break;
    default:{
            ctx->setGLerror(GL_INVALID_ENUM);
            return;
        }
    }
}

KHR_API(void, glGetPointerv)(GLenum pname, void **params) {
    GET_CTX()
    const GLESpointer* p = ctx->getPointer(pname);
    if(p) {
        if(p->isVBO())
        {
            *params = (void*)(p->getBufferOffset());
        }else{
            *params = const_cast<void *>( p->getArrayData());
        }
    } else {
        ctx->setGLerror(GL_INVALID_ENUM);
    }

}

KHR_API(void, glGetTexEnvfv)(GLenum env, GLenum pname, GLfloat *params) {
    GET_CTX()
    ctx->dispatcher().glGetTexEnvfv(env,pname,params);
}

KHR_API(void, glGetTexEnviv)(GLenum env, GLenum pname, GLint *params) {
    GET_CTX()
    ctx->dispatcher().glGetTexEnviv(env,pname,params);
}

KHR_API(void, glGetTexEnvxv)(GLenum env, GLenum pname, GLfixed *params) {
    GET_CTX()
    GLfloat tmpParams[4];

    ctx->dispatcher().glGetTexEnvfv(env,pname,tmpParams);
    if(pname == GL_TEXTURE_ENV_MODE) {
        params[0] = static_cast<GLfixed>(tmpParams[0]);
    } else {
        for(int i=0 ; i < 4 ; i++)
            params[i] = F2X(tmpParams[i]);
    }
}

KHR_API(void, glGetTexParameterfv)(GLenum target, GLenum pname, GLfloat *params) {
    GET_CTX()
   if (pname==GL_TEXTURE_CROP_RECT_OES) {
      TextureData *texData = getTextureTargetData(target);
      SET_ERROR_IF(texData==NULL,GL_INVALID_OPERATION);
      for (int i=0;i<4;++i)
        params[i] = texData->crop_rect[i];
    }
    else {
      ctx->dispatcher().glGetTexParameterfv(target,pname,params);
    }
}

KHR_API(void, glGetTexParameteriv)(GLenum target, GLenum pname, GLint *params) {
    GET_CTX()
    if (pname==GL_TEXTURE_CROP_RECT_OES) {
      TextureData *texData = getTextureTargetData(target);
      SET_ERROR_IF(texData==NULL,GL_INVALID_OPERATION);
      for (int i=0;i<4;++i)
        params[i] = texData->crop_rect[i];
    }
    else {
      ctx->dispatcher().glGetTexParameteriv(target,pname,params);
    }
}

KHR_API(void, glGetTexParameterxv)(GLenum target, GLenum pname, GLfixed *params) {
    GET_CTX()
    if (pname==GL_TEXTURE_CROP_RECT_OES) {
      TextureData *texData = getTextureTargetData(target);
      SET_ERROR_IF(texData==NULL,GL_INVALID_OPERATION);
      for (int i=0;i<4;++i)
        params[i] = F2X(texData->crop_rect[i]);
    }
    else {
      GLfloat tmpParam;
      ctx->dispatcher().glGetTexParameterfv(target,pname,&tmpParam);
      params[0] = static_cast<GLfixed>(tmpParam);
    }
}

KHR_API(void, glHint)(GLenum target, GLenum mode) {
    GET_CTX()
    SET_ERROR_IF(!GLEScmValidate::hintTargetMode(target,mode),GL_INVALID_ENUM);
    ctx->dispatcher().glHint(target,mode);
}

KHR_API(void, glLightModelf)(GLenum pname, GLfloat param) {
    GET_CTX()
    ctx->dispatcher().glLightModelf(pname,param);
}

KHR_API(void, glLightModelfv)(GLenum pname, const GLfloat *params) {
    GET_CTX()
    ctx->dispatcher().glLightModelfv(pname,params);
}

KHR_API(void, glLightModelx)(GLenum pname, GLfixed param) {
    GET_CTX()
    GLfloat tmpParam = static_cast<GLfloat>(param);
    ctx->dispatcher().glLightModelf(pname,tmpParam);
}

KHR_API(void, glLightModelxv)(GLenum pname, const GLfixed *params) {
    GET_CTX()
    GLfloat tmpParams[4];
    if(pname == GL_LIGHT_MODEL_TWO_SIDE) {
        tmpParams[0] = X2F(params[0]);
    } else if (pname == GL_LIGHT_MODEL_AMBIENT) {
        for(int i=0;i<4;i++) {
            tmpParams[i] = X2F(params[i]);
        }
    }

    ctx->dispatcher().glLightModelfv(pname,tmpParams);
}

KHR_API(void, glLightf)(GLenum light, GLenum pname, GLfloat param) {
    GET_CTX()
    ctx->dispatcher().glLightf(light,pname,param);
}

KHR_API(void, glLightfv)(GLenum light, GLenum pname, const GLfloat *params) {
    GET_CTX()
    ctx->dispatcher().glLightfv(light,pname,params);
}

KHR_API(void, glLightx)(GLenum light, GLenum pname, GLfixed param) {
    GET_CTX()
    ctx->dispatcher().glLightf(light,pname,X2F(param));
}

KHR_API(void, glLightxv)(GLenum light, GLenum pname, const GLfixed *params) {
    GET_CTX()
    GLfloat tmpParams[4];

    switch (pname) {
        case GL_AMBIENT:
        case GL_DIFFUSE:
        case GL_SPECULAR:
        case GL_EMISSION:
        case GL_POSITION:
            tmpParams[3] = X2F(params[3]);
        case GL_SPOT_DIRECTION:
            tmpParams[2] = X2F(params[2]);
            tmpParams[1] = X2F(params[1]);
        case GL_SPOT_EXPONENT:
        case GL_SPOT_CUTOFF:
        case GL_CONSTANT_ATTENUATION:
        case GL_LINEAR_ATTENUATION:
        case GL_QUADRATIC_ATTENUATION:
            tmpParams[0] = X2F(params[0]);
            break;
        default: {
                ctx->setGLerror(GL_INVALID_ENUM);
                return;
            }
    }
    ctx->dispatcher().glLightfv(light,pname,tmpParams);
}

KHR_API(void, glLineWidth)(GLfloat width) {
    GET_CTX()
    ctx->dispatcher().glLineWidth(width);
}

KHR_API(void, glLineWidthx)(GLfixed width) {
    GET_CTX()
    ctx->dispatcher().glLineWidth(X2F(width));
}

KHR_API(void, glLoadIdentity)(void) {
    GET_CTX()
    ctx->dispatcher().glLoadIdentity();
}

KHR_API(void, glLoadMatrixf)(const GLfloat *m) {
    GET_CTX()
    ctx->dispatcher().glLoadMatrixf(m);
}

KHR_API(void, glLoadMatrixx)(const GLfixed *m) {
    GET_CTX()
    GLfloat mat[16];
    for(int i=0; i< 16 ; i++) {
        mat[i] = X2F(m[i]);
    }
    ctx->dispatcher().glLoadMatrixf(mat);
}

KHR_API(void, glLogicOp)(GLenum opcode) {
    GET_CTX()
    ctx->dispatcher().glLogicOp(opcode);
}

KHR_API(void, glMaterialf)(GLenum face, GLenum pname, GLfloat param) {
    GET_CTX()
    ctx->dispatcher().glMaterialf(face,pname,param);
}

KHR_API(void, glMaterialfv)(GLenum face, GLenum pname, const GLfloat *params) {
    GET_CTX()
    ctx->dispatcher().glMaterialfv(face,pname,params);
}

KHR_API(void, glMaterialx)(GLenum face, GLenum pname, GLfixed param) {
    GET_CTX()
    ctx->dispatcher().glMaterialf(face,pname,X2F(param));
}

KHR_API(void, glMaterialxv)(GLenum face, GLenum pname, const GLfixed *params) {
    GET_CTX()
    GLfloat tmpParams[4];

    for(int i=0; i< 4; i++) {
        tmpParams[i] = X2F(params[i]);
    }
    ctx->dispatcher().glMaterialfv(face,pname,tmpParams);
}

KHR_API(void, glMatrixMode)(GLenum mode) {
    GET_CTX()
    ctx->dispatcher().glMatrixMode(mode);
}

KHR_API(void, glMultMatrixf)(const GLfloat *m) {
    GET_CTX()
    ctx->dispatcher().glMultMatrixf(m);
}

KHR_API(void, glMultMatrixx)(const GLfixed *m) {
    GET_CTX()
    GLfloat mat[16];
    for(int i=0; i< 16 ; i++) {
        mat[i] = X2F(m[i]);
    }
    ctx->dispatcher().glMultMatrixf(mat);
}

KHR_API(void, glMultiTexCoord4f)(GLenum target, GLfloat s, GLfloat t, GLfloat r, GLfloat q) {
    GET_CTX_CM()
    SET_ERROR_IF(!GLEScmValidate::textureEnum(target,ctx->getMaxTexUnits()),GL_INVALID_ENUM);
    ctx->dispatcher().glMultiTexCoord4f(target,s,t,r,q);
}

KHR_API(void, glMultiTexCoord4x)(GLenum target, GLfixed s, GLfixed t, GLfixed r, GLfixed q) {
    GET_CTX_CM()
    SET_ERROR_IF(!GLEScmValidate::textureEnum(target,ctx->getMaxTexUnits()),GL_INVALID_ENUM);
    ctx->dispatcher().glMultiTexCoord4f(target,X2F(s),X2F(t),X2F(r),X2F(q));
}

KHR_API(void, glNormal3f)(GLfloat nx, GLfloat ny, GLfloat nz) {
    GET_CTX()
    ctx->dispatcher().glNormal3f(nx,ny,nz);
}

KHR_API(void, glNormal3x)(GLfixed nx, GLfixed ny, GLfixed nz) {
    GET_CTX()
    ctx->dispatcher().glNormal3f(X2F(nx),X2F(ny),X2F(nz));
}

KHR_API(void, glNormalPointer)(GLenum type, GLsizei stride, const GLvoid *pointer) {
    GET_CTX()
    SET_ERROR_IF(stride < 0,GL_INVALID_VALUE);
    SET_ERROR_IF(!GLEScmValidate::normalPointerType(type),GL_INVALID_ENUM);
    ctx->setPointer(GL_NORMAL_ARRAY,3,type,stride,pointer);//3 normal verctor
}

KHR_API(void, glOrthof)(GLfloat left, GLfloat right, GLfloat bottom, GLfloat top, GLfloat zNear, GLfloat zFar) {
    GET_CTX()
    ctx->dispatcher().glOrtho(left,right,bottom,top,zNear,zFar);
}

KHR_API(void, glOrthox)(GLfixed left, GLfixed right, GLfixed bottom, GLfixed top, GLfixed zNear, GLfixed zFar) {
    GET_CTX()
    ctx->dispatcher().glOrtho(X2F(left),X2F(right),X2F(bottom),X2F(top),X2F(zNear),X2F(zFar));
}

KHR_API(void, glPixelStorei)(GLenum pname, GLint param) {
    GET_CTX()
    SET_ERROR_IF(!(pname == GL_PACK_ALIGNMENT || pname == GL_UNPACK_ALIGNMENT),GL_INVALID_ENUM);
    SET_ERROR_IF(!((param==1)||(param==2)||(param==4)||(param==8)), GL_INVALID_VALUE);
    ctx->setUnpackAlignment(param);
    ctx->dispatcher().glPixelStorei(pname,param);
}

KHR_API(void, glPointParameterf)(GLenum pname, GLfloat param) {
    GET_CTX()
    ctx->dispatcher().glPointParameterf(pname,param);
}

KHR_API(void, glPointParameterfv)(GLenum pname, const GLfloat *params) {
    GET_CTX()
    ctx->dispatcher().glPointParameterfv(pname,params);
}

KHR_API(void, glPointParameterx)(GLenum pname, GLfixed param)
{
    GET_CTX()
    ctx->dispatcher().glPointParameterf(pname,X2F(param));
}

KHR_API(void, glPointParameterxv)(GLenum pname, const GLfixed *params) {
    GET_CTX()

    GLfloat tmpParam = X2F(*params) ;
    ctx->dispatcher().glPointParameterfv(pname,&tmpParam);
}

KHR_API(void, glPointSize)(GLfloat size) {
    GET_CTX()
    ctx->dispatcher().glPointSize(size);
}

KHR_API(void, glPointSizePointerOES)(GLenum type, GLsizei stride, const GLvoid *pointer) {
    GET_CTX()
    SET_ERROR_IF(stride < 0,GL_INVALID_VALUE);
    SET_ERROR_IF(!GLEScmValidate::pointPointerType(type),GL_INVALID_ENUM);
    ctx->setPointer(GL_POINT_SIZE_ARRAY_OES,1,type,stride,pointer);
}

KHR_API(void, glPointSizex)(GLfixed size) {
    GET_CTX()
    ctx->dispatcher().glPointSize(X2F(size));
}

KHR_API(void, glPolygonOffset)(GLfloat factor, GLfloat units) {
    GET_CTX()
    ctx->dispatcher().glPolygonOffset(factor,units);
}

KHR_API(void, glPolygonOffsetx)(GLfixed factor, GLfixed units) {
    GET_CTX()
    ctx->dispatcher().glPolygonOffset(X2F(factor),X2F(units));
}

KHR_API(void, glPopMatrix)(void) {
    GET_CTX()
    ctx->dispatcher().glPopMatrix();
}

KHR_API(void, glPushMatrix)(void) {
    GET_CTX()
    ctx->dispatcher().glPushMatrix();
}

KHR_API(void, glReadPixels)(GLint x, GLint y, GLsizei width, GLsizei height, GLenum format, GLenum type, GLvoid *pixels) {
    GET_CTX()
    SET_ERROR_IF(!(GLEScmValidate::pixelFrmt(ctx,format) && GLEScmValidate::pixelType(ctx,type)),GL_INVALID_ENUM);
    SET_ERROR_IF(!(GLEScmValidate::pixelOp(format,type)),GL_INVALID_OPERATION);

    ctx->dispatcher().glReadPixels(x,y,width,height,format,type,pixels);
}

KHR_API(void, glRotatef)(GLfloat angle, GLfloat x, GLfloat y, GLfloat z) {
    GET_CTX()
    ctx->dispatcher().glRotatef(angle,x,y,z);
}

KHR_API(void, glRotatex)(GLfixed angle, GLfixed x, GLfixed y, GLfixed z) {
    GET_CTX()
    ctx->dispatcher().glRotatef(angle,X2F(x),X2F(y),X2F(z));
}

KHR_API(void, glSampleCoverage)(GLclampf value, GLboolean invert) {
    GET_CTX()
    ctx->dispatcher().glSampleCoverage(value,invert);
}

KHR_API(void, glSampleCoveragex)(GLclampx value, GLboolean invert) {
    GET_CTX()
    ctx->dispatcher().glSampleCoverage(X2F(value),invert);
}

KHR_API(void, glScalef)(GLfloat x, GLfloat y, GLfloat z) {
    GET_CTX()
    ctx->dispatcher().glScalef(x,y,z);
}

KHR_API(void, glScalex)(GLfixed x, GLfixed y, GLfixed z) {
    GET_CTX()
    ctx->dispatcher().glScalef(X2F(x),X2F(y),X2F(z));
}

KHR_API(void, glScissor)(GLint x, GLint y, GLsizei width, GLsizei height) {
    GET_CTX()
    ctx->dispatcher().glScissor(x,y,width,height);
}

KHR_API(void, glShadeModel)(GLenum mode) {
    GET_CTX()
    ctx->dispatcher().glShadeModel(mode);
}

KHR_API(void, glStencilFunc)(GLenum func, GLint ref, GLuint mask) {
    GET_CTX()
    ctx->dispatcher().glStencilFunc(func,ref,mask);
}

KHR_API(void, glStencilMask)(GLuint mask) {
    GET_CTX()
    ctx->dispatcher().glStencilMask(mask);
}

KHR_API(void, glStencilOp)(GLenum fail, GLenum zfail, GLenum zpass) {
    GET_CTX()
    SET_ERROR_IF(!(GLEScmValidate::stencilOp(fail) && GLEScmValidate::stencilOp(zfail) && GLEScmValidate::stencilOp(zpass)),GL_INVALID_ENUM);
    ctx->dispatcher().glStencilOp(fail,zfail,zpass);
}

KHR_API(void, glTexCoordPointer)(GLint size, GLenum type, GLsizei stride, const GLvoid *pointer) {
    GET_CTX()
    SET_ERROR_IF(!GLEScmValidate::texCoordPointerParams(size,stride),GL_INVALID_VALUE);
    SET_ERROR_IF(!GLEScmValidate::texCoordPointerType(type),GL_INVALID_ENUM);
    ctx->setPointer(GL_TEXTURE_COORD_ARRAY,size,type,stride,pointer);
}

KHR_API(void, glTexEnvf)(GLenum target, GLenum pname, GLfloat param) {
    GET_CTX()
    SET_ERROR_IF(!GLEScmValidate::texEnv(target,pname),GL_INVALID_ENUM);
    ctx->dispatcher().glTexEnvf(target,pname,param);
}

KHR_API(void, glTexEnvfv)(GLenum target, GLenum pname, const GLfloat *params) {
    GET_CTX()
    SET_ERROR_IF(!GLEScmValidate::texEnv(target,pname),GL_INVALID_ENUM);
    ctx->dispatcher().glTexEnvfv(target,pname,params);
}

KHR_API(void, glTexEnvi)(GLenum target, GLenum pname, GLint param) {
    GET_CTX()
    SET_ERROR_IF(!GLEScmValidate::texEnv(target,pname),GL_INVALID_ENUM);
    ctx->dispatcher().glTexEnvi(target,pname,param);
}

KHR_API(void, glTexEnviv)(GLenum target, GLenum pname, const GLint *params) {
    GET_CTX()
    SET_ERROR_IF(!GLEScmValidate::texEnv(target,pname),GL_INVALID_ENUM);
    ctx->dispatcher().glTexEnviv(target,pname,params);
}

KHR_API(void, glTexEnvx)(GLenum target, GLenum pname, GLfixed param) {
    GET_CTX()
    SET_ERROR_IF(!GLEScmValidate::texEnv(target,pname),GL_INVALID_ENUM);
    GLfloat tmpParam = static_cast<GLfloat>(param);
    ctx->dispatcher().glTexEnvf(target,pname,tmpParam);
}

KHR_API(void, glTexEnvxv)(GLenum target, GLenum pname, const GLfixed *params) {
    GET_CTX()
    SET_ERROR_IF(!GLEScmValidate::texEnv(target,pname),GL_INVALID_ENUM);

    GLfloat tmpParams[4];
    if(pname == GL_TEXTURE_ENV_COLOR) {
        for(int i =0;i<4;i++) {
            tmpParams[i] = X2F(params[i]);
        }
    } else {
        tmpParams[0] = static_cast<GLfloat>(params[0]);
    }
    ctx->dispatcher().glTexEnvfv(target,pname,tmpParams);
}

KHR_API(void, glTexImage2D)(GLenum target, GLint level, GLint internalformat, GLsizei width, GLsizei height, GLint border, GLenum format, GLenum type, const GLvoid *pixels) {
    GET_CTX()

    SET_ERROR_IF(!(GLEScmValidate::textureTargetEx(target) &&
                     GLEScmValidate::pixelFrmt(ctx,internalformat) &&
                     GLEScmValidate::pixelFrmt(ctx,format) &&
                     GLEScmValidate::pixelType(ctx,type)),GL_INVALID_ENUM);

    SET_ERROR_IF(!(GLEScmValidate::pixelOp(format,type) && internalformat == ((GLint)format)),GL_INVALID_OPERATION);

    bool needAutoMipmap = false;

    if (ctx->shareGroup().Ptr()){
        TextureData *texData = getTextureTargetData(target);
        SET_ERROR_IF(texData==NULL,GL_INVALID_OPERATION);
        if(texData) {
            texData->width = width;
            texData->height = height;
            texData->border = border;
            texData->internalFormat = internalformat;
            texData->target = target;

            if (texData->sourceEGLImage != 0) {
                //
                // This texture was a target of EGLImage,
                // but now it is re-defined so we need to detach
                // from the EGLImage and re-generate global texture name
                // for it.
                //
                if (texData->eglImageDetach) {
                    (*texData->eglImageDetach)(texData->sourceEGLImage);
                }
                unsigned int tex = ctx->getBindedTexture(target);
                ctx->shareGroup()->replaceGlobalName(TEXTURE,
                                                     tex,
                                                     texData->oldGlobal);
                ctx->dispatcher().glBindTexture(GL_TEXTURE_2D, texData->oldGlobal);
                texData->sourceEGLImage = 0;
                texData->oldGlobal = 0;
            }

            needAutoMipmap = texData->requiresAutoMipmap;
        }
    }

    ctx->dispatcher().glTexImage2D(target,level,
                                   internalformat,width,height,
                                   border,format,type,pixels);

    if(needAutoMipmap)
    {
        ctx->dispatcher().glGenerateMipmapEXT(target);
    }
}

static bool handleMipmapGeneration(GLenum target, GLenum pname, bool param)
{
    GET_CTX_RET(false)

    if(pname == GL_GENERATE_MIPMAP && !ctx->isAutoMipmapSupported())
    {
        TextureData *texData = getTextureTargetData(target);
        if(texData)
        {
            texData->requiresAutoMipmap = param;
        }
        return true;
    }

    return false;
}

KHR_API(void, glTexParameterf)(GLenum target, GLenum pname, GLfloat param) {
    GET_CTX()
    SET_ERROR_IF(!GLEScmValidate::texParams(target,pname),GL_INVALID_ENUM);

    if(handleMipmapGeneration(target, pname, (bool)param))
        return;

    ctx->dispatcher().glTexParameterf(target,pname,param);
}

KHR_API(void, glTexParameterfv)(GLenum target, GLenum pname, const GLfloat *params) {
    GET_CTX()
    SET_ERROR_IF(!GLEScmValidate::texParams(target,pname),GL_INVALID_ENUM);

    if(handleMipmapGeneration(target, pname, (bool)(*params)))
        return;

    if (pname==GL_TEXTURE_CROP_RECT_OES) {
        TextureData *texData = getTextureTargetData(target);
        SET_ERROR_IF(texData==NULL,GL_INVALID_OPERATION);
        for (int i=0;i<4;++i)
            texData->crop_rect[i] = params[i];
    }
    else {
        ctx->dispatcher().glTexParameterfv(target,pname,params);
    }
}

KHR_API(void, glTexParameteri)(GLenum target, GLenum pname, GLint param) {
    GET_CTX()
    SET_ERROR_IF(!GLEScmValidate::texParams(target,pname),GL_INVALID_ENUM);

    if(handleMipmapGeneration(target, pname, (bool)param))
        return;

    ctx->dispatcher().glTexParameteri(target,pname,param);
}

KHR_API(void, glTexParameteriv)(GLenum target, GLenum pname, const GLint *params) {
    GET_CTX()
    SET_ERROR_IF(!GLEScmValidate::texParams(target,pname),GL_INVALID_ENUM);

    if(handleMipmapGeneration(target, pname, (bool)(*params)))
        return;

    if (pname==GL_TEXTURE_CROP_RECT_OES) {
        TextureData *texData = getTextureTargetData(target);
        SET_ERROR_IF(texData==NULL,GL_INVALID_OPERATION);
        for (int i=0;i<4;++i)
            texData->crop_rect[i] = params[i];
    }
    else {
        ctx->dispatcher().glTexParameteriv(target,pname,params);
    }
}

KHR_API(void, glTexParameterx)(GLenum target, GLenum pname, GLfixed param) {
    GET_CTX()
    SET_ERROR_IF(!GLEScmValidate::texParams(target,pname),GL_INVALID_ENUM);

    if(handleMipmapGeneration(target, pname, (bool)param))
        return;

    ctx->dispatcher().glTexParameterf(target,pname,static_cast<GLfloat>(param));
}

KHR_API(void, glTexParameterxv)(GLenum target, GLenum pname, const GLfixed *params) {
    GET_CTX()
    SET_ERROR_IF(!GLEScmValidate::texParams(target,pname),GL_INVALID_ENUM);

    if(handleMipmapGeneration(target, pname, (bool)(*params)))
        return;

    if (pname==GL_TEXTURE_CROP_RECT_OES) {
        TextureData *texData = getTextureTargetData(target);
        SET_ERROR_IF(texData==NULL,GL_INVALID_OPERATION);
        for (int i=0;i<4;++i)
            texData->crop_rect[i] = X2F(params[i]);
    }
    else {
        GLfloat param = static_cast<GLfloat>(params[0]);
        ctx->dispatcher().glTexParameterfv(target,pname,&param);
    }
}

KHR_API(void, glTexSubImage2D)(GLenum target, GLint level, GLint xoffset, GLint yoffset, GLsizei width, GLsizei height, GLenum format, GLenum type, const GLvoid *pixels) {
    GET_CTX()
    SET_ERROR_IF(!(GLEScmValidate::textureTargetEx(target) &&
                   GLEScmValidate::pixelFrmt(ctx,format)&&
                   GLEScmValidate::pixelType(ctx,type)),GL_INVALID_ENUM);
    SET_ERROR_IF(!GLEScmValidate::pixelOp(format,type),GL_INVALID_OPERATION);

    ctx->dispatcher().glTexSubImage2D(target,level,xoffset,yoffset,width,height,format,type,pixels);

    if (ctx->shareGroup().Ptr()){
        TextureData *texData = getTextureTargetData(target);
        SET_ERROR_IF(texData==NULL,GL_INVALID_OPERATION);
        if(texData && texData->requiresAutoMipmap)
        {
                ctx->dispatcher().glGenerateMipmapEXT(target);
        }
    }
}

KHR_API(void, glTranslatef)(GLfloat x, GLfloat y, GLfloat z) {
    GET_CTX()
    ctx->dispatcher().glTranslatef(x,y,z);
}

KHR_API(void, glTranslatex)(GLfixed x, GLfixed y, GLfixed z) {
    GET_CTX()
    ctx->dispatcher().glTranslatef(X2F(x),X2F(y),X2F(z));
}

KHR_API(void, glVertexPointer)(GLint size, GLenum type, GLsizei stride, const GLvoid *pointer) {
    GET_CTX()
    SET_ERROR_IF(!GLEScmValidate::vertexPointerParams(size,stride),GL_INVALID_VALUE);
    SET_ERROR_IF(!GLEScmValidate::vertexPointerType(type),GL_INVALID_ENUM);
    ctx->setPointer(GL_VERTEX_ARRAY,size,type,stride,pointer);
}

KHR_API(void, glViewport)(GLint x, GLint y, GLsizei width, GLsizei height) {
    GET_CTX()
    ctx->dispatcher().glViewport(x,y,width,height);
}

KHR_API(void, glEGLImageTargetTexture2DOES)(GLenum target, GLeglImageOES image)
{
    GET_CTX();
    if (!GLEScmValidate::textureTargetLimited(target)) {
        SET_ERROR_IF(!GLEScmValidate::textureTargetLimited(target),GL_INVALID_ENUM);
    }
    unsigned int imagehndl = ToTargetCompatibleHandle((uintptr_t)image);
    EglImage *img = s_eglIface->eglAttachEGLImage(imagehndl);
    if (img) {
        // Create the texture object in the underlying EGL implementation,
        // flag to the OpenGL layer to skip the image creation and map the
        // current binded texture object to the existing global object.
        if (ctx->shareGroup().Ptr()) {
            ObjectLocalName tex = TextureLocalName(target,ctx->getBindedTexture(target));
            unsigned int oldGlobal = ctx->shareGroup()->getGlobalName(TEXTURE, tex);
            // Delete old texture object but only if it is not a target of a EGLImage
            if (oldGlobal) {
                TextureData* oldTexData = getTextureData(tex);
                if (!oldTexData || oldTexData->sourceEGLImage == 0) {
                    ctx->dispatcher().glDeleteTextures(1, &oldGlobal);
                }
            }
            // replace mapping and bind the new global object
            ctx->shareGroup()->replaceGlobalName(TEXTURE, tex,img->globalTexName);
            ctx->dispatcher().glBindTexture(GL_TEXTURE_2D, img->globalTexName);
            TextureData *texData = getTextureTargetData(target);
            SET_ERROR_IF(texData==NULL,GL_INVALID_OPERATION);
            texData->width = img->width;
            texData->height = img->height;
            texData->border = img->border;
            texData->internalFormat = img->internalFormat;
            texData->sourceEGLImage = imagehndl;
            texData->eglImageDetach = s_eglIface->eglDetachEGLImage;
            texData->oldGlobal = oldGlobal;
        }
    }
}

KHR_API(void, glEGLImageTargetRenderbufferStorageOES)(GLenum target, GLeglImageOES image)
{
    GET_CTX();
    SET_ERROR_IF(target != GL_RENDERBUFFER_OES,GL_INVALID_ENUM);
    unsigned int imagehndl = ToTargetCompatibleHandle((uintptr_t)image);
    EglImage *img = s_eglIface->eglAttachEGLImage(imagehndl);
    SET_ERROR_IF(!img,GL_INVALID_VALUE);
    SET_ERROR_IF(!ctx->shareGroup().Ptr(),GL_INVALID_OPERATION);

    // Get current bounded renderbuffer
    // raise INVALID_OPERATIOn if no renderbuffer is bounded
    GLuint rb = ctx->getRenderbufferBinding();
    SET_ERROR_IF(rb == 0,GL_INVALID_OPERATION);
    ObjectDataPtr objData = ctx->shareGroup()->getObjectData(RENDERBUFFER,rb);
    RenderbufferData *rbData = (RenderbufferData *)objData.Ptr();
    SET_ERROR_IF(!rbData,GL_INVALID_OPERATION);

    //
    // flag in the renderbufferData that it is an eglImage target
    //
    rbData->sourceEGLImage = imagehndl;
    rbData->eglImageDetach = s_eglIface->eglDetachEGLImage;
    rbData->eglImageGlobalTexName = img->globalTexName;

    //
    // if the renderbuffer is attached to a framebuffer
    // change the framebuffer attachment in the undelying OpenGL
    // to point to the eglImage texture object.
    //
    if (rbData->attachedFB) {
        // update the framebuffer attachment point to the
        // underlying texture of the img
        GLuint prevFB = ctx->getFramebufferBinding();
        if (prevFB != rbData->attachedFB) {
            ctx->dispatcher().glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 
                                                   rbData->attachedFB);
        }
        ctx->dispatcher().glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT,
                                                    rbData->attachedPoint,
                                                    GL_TEXTURE_2D,
                                                    img->globalTexName,0);
        if (prevFB != rbData->attachedFB) {
            ctx->dispatcher().glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 
                                                   prevFB);
        }
    }
}

/* GL_OES_blend_subtract*/
KHR_API(void, glBlendEquationOES)(GLenum mode) {
    GET_CTX()
    SET_ERROR_IF(!(GLEScmValidate::blendEquationMode(mode)), GL_INVALID_ENUM);
    ctx->dispatcher().glBlendEquation(mode);
}

/* GL_OES_blend_equation_separate */
KHR_API(void, glBlendEquationSeparateOES)(GLenum modeRGB, GLenum modeAlpha) {
    GET_CTX()
    SET_ERROR_IF(!(GLEScmValidate::blendEquationMode(modeRGB) && GLEScmValidate::blendEquationMode(modeAlpha)), GL_INVALID_ENUM);
    ctx->dispatcher().glBlendEquationSeparate(modeRGB,modeAlpha);
}

/* GL_OES_blend_func_separate */
KHR_API(void, glBlendFuncSeparateOES)(GLenum srcRGB, GLenum dstRGB, GLenum srcAlpha, GLenum dstAlpha) {
    GET_CTX()
    SET_ERROR_IF(!GLEScmValidate::blendSrc(srcRGB) || !GLEScmValidate::blendDst(dstRGB) ||
                 !GLEScmValidate::blendSrc(srcAlpha) || ! GLEScmValidate::blendDst(dstAlpha) ,GL_INVALID_ENUM);
    ctx->dispatcher().glBlendFuncSeparate(srcRGB,dstRGB,srcAlpha,dstAlpha);
}

/* GL_OES_framebuffer_object */
KHR_API(GLboolean, glIsRenderbufferOES)(GLuint renderbuffer) {
    GET_CTX_RET(GL_FALSE)
    RET_AND_SET_ERROR_IF(!ctx->getCaps()->GL_EXT_FRAMEBUFFER_OBJECT,GL_INVALID_OPERATION,GL_FALSE);
    if(renderbuffer && ctx->shareGroup().Ptr()){
        return ctx->shareGroup()->isObject(RENDERBUFFER,renderbuffer) ? GL_TRUE :GL_FALSE;
    }
    return ctx->dispatcher().glIsRenderbufferEXT(renderbuffer);
}

KHR_API(void, glBindRenderbufferOES)(GLenum target, GLuint renderbuffer) {
    GET_CTX()
    SET_ERROR_IF(!ctx->getCaps()->GL_EXT_FRAMEBUFFER_OBJECT,GL_INVALID_OPERATION);
    SET_ERROR_IF(!GLEScmValidate::renderbufferTarget(target),GL_INVALID_ENUM);

    //if buffer wasn't generated before,generate one
    if(renderbuffer && ctx->shareGroup().Ptr() && !ctx->shareGroup()->isObject(RENDERBUFFER,renderbuffer)){
        ctx->shareGroup()->genName(RENDERBUFFER,renderbuffer);
        ctx->shareGroup()->setObjectData(RENDERBUFFER,
                                         renderbuffer,
                                         ObjectDataPtr(new RenderbufferData()));
    }

    int globalBufferName = (renderbuffer != 0) ? ctx->shareGroup()->getGlobalName(RENDERBUFFER,renderbuffer) : 0;
    ctx->dispatcher().glBindRenderbufferEXT(target,globalBufferName);

    // update renderbuffer binding state
    ctx->setRenderbufferBinding(renderbuffer);
}

KHR_API(void, glDeleteRenderbuffersOES)(GLsizei n, const GLuint *renderbuffers) {
    GET_CTX()
    SET_ERROR_IF(!ctx->getCaps()->GL_EXT_FRAMEBUFFER_OBJECT,GL_INVALID_OPERATION);
    for (int i=0;i<n;++i) {
        GLuint globalBufferName = ctx->shareGroup()->getGlobalName(RENDERBUFFER,renderbuffers[i]);
        ctx->dispatcher().glDeleteRenderbuffersEXT(1,&globalBufferName);
    }
}

KHR_API(void, glGenRenderbuffersOES)(GLsizei n, GLuint *renderbuffers) {
    GET_CTX()
    SET_ERROR_IF(!ctx->getCaps()->GL_EXT_FRAMEBUFFER_OBJECT,GL_INVALID_OPERATION);
    SET_ERROR_IF(n<0,GL_INVALID_VALUE);
    if(ctx->shareGroup().Ptr()) {
        for(int i=0; i<n ;i++) {
            renderbuffers[i] = ctx->shareGroup()->genName(RENDERBUFFER, 0, true);
            ctx->shareGroup()->setObjectData(RENDERBUFFER,
                                             renderbuffers[i],
                                         ObjectDataPtr(new RenderbufferData()));
        }
    }
}

KHR_API(void, glRenderbufferStorageOES)(GLenum target, GLenum internalformat, GLsizei width, GLsizei height){
    GET_CTX()
    SET_ERROR_IF(!ctx->getCaps()->GL_EXT_FRAMEBUFFER_OBJECT,GL_INVALID_OPERATION);
    SET_ERROR_IF(!GLEScmValidate::renderbufferTarget(target) || !GLEScmValidate::renderbufferInternalFrmt(ctx,internalformat) ,GL_INVALID_ENUM);
    if (internalformat==GL_RGB565_OES) //RGB565 not supported by GL
        internalformat = GL_RGB8_OES;

    // Get current bounded renderbuffer
    // raise INVALID_OPERATIOn if no renderbuffer is bounded
    GLuint rb = ctx->getRenderbufferBinding();
    SET_ERROR_IF(rb == 0,GL_INVALID_OPERATION);
    ObjectDataPtr objData = ctx->shareGroup()->getObjectData(RENDERBUFFER,rb);
    RenderbufferData *rbData = (RenderbufferData *)objData.Ptr();
    SET_ERROR_IF(!rbData,GL_INVALID_OPERATION);

    //
    // if the renderbuffer was an eglImage target, detach from
    // the eglImage.
    //
    if (rbData->sourceEGLImage != 0) {
        if (rbData->eglImageDetach) {
            (*rbData->eglImageDetach)(rbData->sourceEGLImage);
        }
        rbData->sourceEGLImage = 0;
        rbData->eglImageGlobalTexName = 0;
    }

    ctx->dispatcher().glRenderbufferStorageEXT(target,internalformat,width,height);
}

KHR_API(void, glGetRenderbufferParameterivOES)(GLenum target, GLenum pname, GLint* params) {
    GET_CTX()
    SET_ERROR_IF(!ctx->getCaps()->GL_EXT_FRAMEBUFFER_OBJECT,GL_INVALID_OPERATION);
    SET_ERROR_IF(!GLEScmValidate::renderbufferTarget(target) || !GLEScmValidate::renderbufferParams(pname) ,GL_INVALID_ENUM);

    //
    // If this is a renderbuffer which is eglimage's target, we
    // should query the underlying eglimage's texture object instead.
    //
    GLuint rb = ctx->getRenderbufferBinding();
    if (rb) {
        ObjectDataPtr objData = ctx->shareGroup()->getObjectData(RENDERBUFFER,rb);
        RenderbufferData *rbData = (RenderbufferData *)objData.Ptr();
        if (rbData && rbData->sourceEGLImage != 0) {
            GLenum texPname;
            switch(pname) {
                case GL_RENDERBUFFER_WIDTH_OES:
                    texPname = GL_TEXTURE_WIDTH;
                    break;
                case GL_RENDERBUFFER_HEIGHT_OES:
                    texPname = GL_TEXTURE_HEIGHT;
                    break;
                case GL_RENDERBUFFER_INTERNAL_FORMAT_OES:
                    texPname = GL_TEXTURE_INTERNAL_FORMAT;
                    break;
                case GL_RENDERBUFFER_RED_SIZE_OES:
                    texPname = GL_TEXTURE_RED_SIZE;
                    break;
                case GL_RENDERBUFFER_GREEN_SIZE_OES:
                    texPname = GL_TEXTURE_GREEN_SIZE;
                    break;
                case GL_RENDERBUFFER_BLUE_SIZE_OES:
                    texPname = GL_TEXTURE_BLUE_SIZE;
                    break;
                case GL_RENDERBUFFER_ALPHA_SIZE_OES:
                    texPname = GL_TEXTURE_ALPHA_SIZE;
                    break;
                case GL_RENDERBUFFER_DEPTH_SIZE_OES:
                    texPname = GL_TEXTURE_DEPTH_SIZE;
                    break;
                case GL_RENDERBUFFER_STENCIL_SIZE_OES:
                default:
                    *params = 0; //XXX
                    return;
                    break;
            }

            GLint prevTex;
            ctx->dispatcher().glGetIntegerv(GL_TEXTURE_BINDING_2D, &prevTex);
            ctx->dispatcher().glBindTexture(GL_TEXTURE_2D,
                                            rbData->eglImageGlobalTexName);
            ctx->dispatcher().glGetTexLevelParameteriv(GL_TEXTURE_2D, 0,
                                                       texPname,
                                                       params);
            ctx->dispatcher().glBindTexture(GL_TEXTURE_2D, prevTex);
            return;
        }
    }

    ctx->dispatcher().glGetRenderbufferParameterivEXT(target,pname,params);
}

KHR_API(GLboolean, glIsFramebufferOES)(GLuint framebuffer) {
    GET_CTX_RET(GL_FALSE)
    RET_AND_SET_ERROR_IF(!ctx->getCaps()->GL_EXT_FRAMEBUFFER_OBJECT,GL_INVALID_OPERATION,GL_FALSE);
    if (framebuffer && ctx->shareGroup().Ptr()) {
        return ctx->shareGroup()->isObject(FRAMEBUFFER,framebuffer) ? GL_TRUE : GL_FALSE;
    }
    return ctx->dispatcher().glIsFramebufferEXT(framebuffer);
}

KHR_API(void, glBindFramebufferOES)(GLenum target, GLuint framebuffer) {
    GET_CTX()
    SET_ERROR_IF(!ctx->getCaps()->GL_EXT_FRAMEBUFFER_OBJECT,GL_INVALID_OPERATION);
    SET_ERROR_IF(!GLEScmValidate::framebufferTarget(target) ,GL_INVALID_ENUM);
    if (framebuffer && ctx->shareGroup().Ptr() && !ctx->shareGroup()->isObject(FRAMEBUFFER,framebuffer)) {
        ctx->shareGroup()->genName(FRAMEBUFFER,framebuffer);
        ctx->shareGroup()->setObjectData(FRAMEBUFFER, framebuffer,
                                         ObjectDataPtr(new FramebufferData(framebuffer)));
    }
    int globalBufferName = (framebuffer!=0) ? ctx->shareGroup()->getGlobalName(FRAMEBUFFER,framebuffer) : 0;
    ctx->dispatcher().glBindFramebufferEXT(target,globalBufferName);

    // update framebuffer binding state
    ctx->setFramebufferBinding(framebuffer);
}

KHR_API(void, glDeleteFramebuffersOES)(GLsizei n, const GLuint *framebuffers) {
    GET_CTX()
    SET_ERROR_IF(!ctx->getCaps()->GL_EXT_FRAMEBUFFER_OBJECT,GL_INVALID_OPERATION);
    for (int i=0;i<n;++i) {
        GLuint globalBufferName = ctx->shareGroup()->getGlobalName(FRAMEBUFFER,framebuffers[i]);
        ctx->dispatcher().glDeleteFramebuffersEXT(1,&globalBufferName);
    }
}

KHR_API(void, glGenFramebuffersOES)(GLsizei n, GLuint *framebuffers) {
    GET_CTX()
    SET_ERROR_IF(!ctx->getCaps()->GL_EXT_FRAMEBUFFER_OBJECT,GL_INVALID_OPERATION);
    SET_ERROR_IF(n<0,GL_INVALID_VALUE);
    if (ctx->shareGroup().Ptr()) {
        for (int i=0;i<n;i++) {
            framebuffers[i] = ctx->shareGroup()->genName(FRAMEBUFFER, 0, true);
            ctx->shareGroup()->setObjectData(FRAMEBUFFER, framebuffers[i],
                                             ObjectDataPtr(new FramebufferData(framebuffers[i])));
        }
    }
}

KHR_API(GLenum, glCheckFramebufferStatusOES)(GLenum target) {
    GET_CTX_RET(0)
    RET_AND_SET_ERROR_IF(!ctx->getCaps()->GL_EXT_FRAMEBUFFER_OBJECT,GL_INVALID_OPERATION,0);
    RET_AND_SET_ERROR_IF(!GLEScmValidate::framebufferTarget(target) ,GL_INVALID_ENUM,0);
    return ctx->dispatcher().glCheckFramebufferStatusEXT(target);
}

KHR_API(void, glFramebufferTexture2DOES)(GLenum target, GLenum attachment, GLenum textarget, GLuint texture, GLint level) {
    GET_CTX()
    SET_ERROR_IF(!ctx->getCaps()->GL_EXT_FRAMEBUFFER_OBJECT,GL_INVALID_OPERATION);
    SET_ERROR_IF(!GLEScmValidate::framebufferTarget(target) || !GLEScmValidate::framebufferAttachment(attachment) ||
                 !GLEScmValidate::textureTargetEx(textarget),GL_INVALID_ENUM);
    SET_ERROR_IF(!ctx->shareGroup().Ptr(), GL_INVALID_OPERATION);

    GLuint globalTexName = 0;
    if(texture) {
        if (!ctx->shareGroup()->isObject(TEXTURE,texture)) {
            ctx->shareGroup()->genName(TEXTURE,texture);
        }
        ObjectLocalName texname = TextureLocalName(textarget,texture);
        globalTexName = ctx->shareGroup()->getGlobalName(TEXTURE,texname);
    }

    ctx->dispatcher().glFramebufferTexture2DEXT(target,attachment,textarget,globalTexName,level);

    // Update the the current framebuffer object attachment state
    GLuint fbName = ctx->getFramebufferBinding();
    ObjectDataPtr fbObj = ctx->shareGroup()->getObjectData(FRAMEBUFFER,fbName);
    if (fbObj.Ptr() != NULL) {
        FramebufferData *fbData = (FramebufferData *)fbObj.Ptr();
        fbData->setAttachment(attachment, textarget, 
                              texture, ObjectDataPtr(NULL));
    }
}

KHR_API(void, glFramebufferRenderbufferOES)(GLenum target, GLenum attachment,GLenum renderbuffertarget, GLuint renderbuffer) {
    GET_CTX()
    SET_ERROR_IF(!ctx->getCaps()->GL_EXT_FRAMEBUFFER_OBJECT,GL_INVALID_OPERATION);
    SET_ERROR_IF(!GLEScmValidate::framebufferTarget(target) || 
                 !GLEScmValidate::framebufferAttachment(attachment) ||
                 !GLEScmValidate::renderbufferTarget(renderbuffertarget), GL_INVALID_ENUM);

    SET_ERROR_IF(!ctx->shareGroup().Ptr(), GL_INVALID_OPERATION);

    GLuint globalBufferName = 0;
    ObjectDataPtr obj;

    // generate the renderbuffer object if not yet exist
    if (renderbuffer) {
        if (!ctx->shareGroup()->isObject(RENDERBUFFER,renderbuffer)) {
            ctx->shareGroup()->genName(RENDERBUFFER,renderbuffer);
            obj = ObjectDataPtr(new RenderbufferData());
            ctx->shareGroup()->setObjectData(RENDERBUFFER,
                                         renderbuffer,
                                         ObjectDataPtr(new RenderbufferData()));
        }
        else {
            obj = ctx->shareGroup()->getObjectData(RENDERBUFFER,renderbuffer);
        }
        globalBufferName = ctx->shareGroup()->getGlobalName(RENDERBUFFER,renderbuffer);
    }

    // Update the the current framebuffer object attachment state
    GLuint fbName = ctx->getFramebufferBinding();
    ObjectDataPtr fbObj = ctx->shareGroup()->getObjectData(FRAMEBUFFER,fbName);
    if (fbObj.Ptr() != NULL) {
        FramebufferData *fbData = (FramebufferData *)fbObj.Ptr();
        fbData->setAttachment(attachment, renderbuffertarget, renderbuffer, obj);
    }

    if (renderbuffer && obj.Ptr() != NULL) {
        RenderbufferData *rbData = (RenderbufferData *)obj.Ptr();
        if (rbData->sourceEGLImage != 0) {
            //
            // This renderbuffer object is an eglImage target
            // attach the eglimage's texture instead the renderbuffer.
            //
            ctx->dispatcher().glFramebufferTexture2DEXT(target,
                                                    attachment,
                                                    GL_TEXTURE_2D,
                                                    rbData->eglImageGlobalTexName,0);
            return;
        }
    }

    ctx->dispatcher().glFramebufferRenderbufferEXT(target,attachment,renderbuffertarget,globalBufferName);
}

KHR_API(void, glGetFramebufferAttachmentParameterivOES)(GLenum target, GLenum attachment, GLenum pname, GLint *params) {
    GET_CTX()
    SET_ERROR_IF(!ctx->getCaps()->GL_EXT_FRAMEBUFFER_OBJECT,GL_INVALID_OPERATION);
    SET_ERROR_IF(!GLEScmValidate::framebufferTarget(target) || !GLEScmValidate::framebufferAttachment(attachment) ||
                 !GLEScmValidate::framebufferAttachmentParams(pname), GL_INVALID_ENUM);

    //
    // Take the attachment attribute from our state - if available
    //
    GLuint fbName = ctx->getFramebufferBinding();
    if (fbName) {
        ObjectDataPtr fbObj = ctx->shareGroup()->getObjectData(FRAMEBUFFER,fbName);
        if (fbObj.Ptr() != NULL) {
            FramebufferData *fbData = (FramebufferData *)fbObj.Ptr();
            GLenum target;
            GLuint name = fbData->getAttachment(attachment, &target, NULL);
            if (pname == GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE_OES) {
                *params = target;
                return;
            }
            else if (pname == GL_FRAMEBUFFER_ATTACHMENT_OBJECT_NAME_OES) {
                *params = name;
                return;
            }
        }
    }

    ctx->dispatcher().glGetFramebufferAttachmentParameterivEXT(target,attachment,pname,params);
}

KHR_API(void, glGenerateMipmapOES)(GLenum target) {
    GET_CTX()
    SET_ERROR_IF(!ctx->getCaps()->GL_EXT_FRAMEBUFFER_OBJECT,GL_INVALID_OPERATION);
    SET_ERROR_IF(!GLEScmValidate::textureTargetLimited(target),GL_INVALID_ENUM);
    ctx->dispatcher().glGenerateMipmapEXT(target);
}

KHR_API(void, glCurrentPaletteMatrixOES)(GLuint index) {
    GET_CTX()
    SET_ERROR_IF(!(ctx->getCaps()->GL_ARB_MATRIX_PALETTE && ctx->getCaps()->GL_ARB_VERTEX_BLEND),GL_INVALID_OPERATION);
    ctx->dispatcher().glCurrentPaletteMatrixARB(index);
}

KHR_API(void, glLoadPaletteFromModelViewMatrixOES)() {
    GET_CTX()
    SET_ERROR_IF(!(ctx->getCaps()->GL_ARB_MATRIX_PALETTE && ctx->getCaps()->GL_ARB_VERTEX_BLEND),GL_INVALID_OPERATION);
    GLint matrix[16];
    ctx->dispatcher().glGetIntegerv(GL_MODELVIEW_MATRIX,matrix);
    ctx->dispatcher().glMatrixIndexuivARB(1,(GLuint*)matrix);

}

KHR_API(void, glMatrixIndexPointerOES)(GLint size, GLenum type, GLsizei stride, const GLvoid *pointer) {
    GET_CTX()
    SET_ERROR_IF(!(ctx->getCaps()->GL_ARB_MATRIX_PALETTE && ctx->getCaps()->GL_ARB_VERTEX_BLEND),GL_INVALID_OPERATION);
    ctx->dispatcher().glMatrixIndexPointerARB(size,type,stride,pointer);
}

KHR_API(void, glWeightPointerOES)(GLint size, GLenum type, GLsizei stride, const GLvoid *pointer) {
    GET_CTX()
    SET_ERROR_IF(!(ctx->getCaps()->GL_ARB_MATRIX_PALETTE && ctx->getCaps()->GL_ARB_VERTEX_BLEND),GL_INVALID_OPERATION);
    ctx->dispatcher().glWeightPointerARB(size,type,stride,pointer);

}

KHR_API(void, glTexGenfOES)(GLenum coord, GLenum pname, GLfloat param) {
    GET_CTX()
    SET_ERROR_IF(!GLEScmValidate::texGen(coord,pname),GL_INVALID_ENUM);
    if (coord == GL_TEXTURE_GEN_STR_OES) {
        ctx->dispatcher().glTexGenf(GL_S,pname,param);
        ctx->dispatcher().glTexGenf(GL_T,pname,param);
        ctx->dispatcher().glTexGenf(GL_R,pname,param);
    }
    else
        ctx->dispatcher().glTexGenf(coord,pname,param);
}

KHR_API(void, glTexGenfvOES)(GLenum coord, GLenum pname, const GLfloat *params) {
    GET_CTX()
    SET_ERROR_IF(!GLEScmValidate::texGen(coord,pname),GL_INVALID_ENUM);
    if (coord == GL_TEXTURE_GEN_STR_OES) {
        ctx->dispatcher().glTexGenfv(GL_S,pname,params);
        ctx->dispatcher().glTexGenfv(GL_T,pname,params);
        ctx->dispatcher().glTexGenfv(GL_R,pname,params);
    }
    else
        ctx->dispatcher().glTexGenfv(coord,pname,params);
}
KHR_API(void, glTexGeniOES)(GLenum coord, GLenum pname, GLint param) {
    GET_CTX()
    SET_ERROR_IF(!GLEScmValidate::texGen(coord,pname),GL_INVALID_ENUM);
    if (coord == GL_TEXTURE_GEN_STR_OES) {
        ctx->dispatcher().glTexGeni(GL_S,pname,param);
        ctx->dispatcher().glTexGeni(GL_T,pname,param);
        ctx->dispatcher().glTexGeni(GL_R,pname,param);
    }
    else
        ctx->dispatcher().glTexGeni(coord,pname,param);
}
KHR_API(void, glTexGenivOES)(GLenum coord, GLenum pname, const GLint *params) {
    GET_CTX()
    SET_ERROR_IF(!GLEScmValidate::texGen(coord,pname),GL_INVALID_ENUM);
    if (coord == GL_TEXTURE_GEN_STR_OES) {
        ctx->dispatcher().glTexGeniv(GL_S,pname,params);
        ctx->dispatcher().glTexGeniv(GL_T,pname,params);
        ctx->dispatcher().glTexGeniv(GL_R,pname,params);
    }
    else
        ctx->dispatcher().glTexGeniv(coord,pname,params);
}
KHR_API(void, glTexGenxOES)(GLenum coord, GLenum pname, GLfixed param) {
    GET_CTX()
    SET_ERROR_IF(!GLEScmValidate::texGen(coord,pname),GL_INVALID_ENUM);
    if (coord == GL_TEXTURE_GEN_STR_OES) {
        ctx->dispatcher().glTexGenf(GL_S,pname,X2F(param));
        ctx->dispatcher().glTexGenf(GL_T,pname,X2F(param));
        ctx->dispatcher().glTexGenf(GL_R,pname,X2F(param));
    }
    else
        ctx->dispatcher().glTexGenf(coord,pname,X2F(param));
}
KHR_API(void, glTexGenxvOES)(GLenum coord, GLenum pname, const GLfixed *params) {
    GLfloat tmpParams[1];
    GET_CTX()
    SET_ERROR_IF(!GLEScmValidate::texGen(coord,pname),GL_INVALID_ENUM);
    tmpParams[0] = X2F(params[0]);
    if (coord == GL_TEXTURE_GEN_STR_OES) {
        ctx->dispatcher().glTexGenfv(GL_S,pname,tmpParams);
        ctx->dispatcher().glTexGenfv(GL_T,pname,tmpParams);
        ctx->dispatcher().glTexGenfv(GL_R,pname,tmpParams);
    }
    else
        ctx->dispatcher().glTexGenfv(coord,pname,tmpParams);
}

KHR_API(void, glGetTexGenfvOES)(GLenum coord, GLenum pname, GLfloat *params) {
    GET_CTX()
    if (coord == GL_TEXTURE_GEN_STR_OES)
    {
        GLfloat state_s = GL_FALSE;
        GLfloat state_t = GL_FALSE;
        GLfloat state_r = GL_FALSE;
        ctx->dispatcher().glGetTexGenfv(GL_S,pname,&state_s);
        ctx->dispatcher().glGetTexGenfv(GL_T,pname,&state_t);
        ctx->dispatcher().glGetTexGenfv(GL_R,pname,&state_r);
        *params = state_s && state_t && state_r ? GL_TRUE: GL_FALSE;
    }
    else
        ctx->dispatcher().glGetTexGenfv(coord,pname,params);

}
KHR_API(void, glGetTexGenivOES)(GLenum coord, GLenum pname, GLint *params) {
    GET_CTX()
    if (coord == GL_TEXTURE_GEN_STR_OES)
    {
        GLint state_s = GL_FALSE;
        GLint state_t = GL_FALSE;
        GLint state_r = GL_FALSE;
        ctx->dispatcher().glGetTexGeniv(GL_S,pname,&state_s);
        ctx->dispatcher().glGetTexGeniv(GL_T,pname,&state_t);
        ctx->dispatcher().glGetTexGeniv(GL_R,pname,&state_r);
        *params = state_s && state_t && state_r ? GL_TRUE: GL_FALSE;
    }
    else
        ctx->dispatcher().glGetTexGeniv(coord,pname,params);
}

KHR_API(void, glGetTexGenxvOES)(GLenum coord, GLenum pname, GLfixed *params) {
    GET_CTX()
    GLfloat tmpParams[1];

    if (coord == GL_TEXTURE_GEN_STR_OES)
    {
        GLfloat state_s = GL_FALSE;
        GLfloat state_t = GL_FALSE;
        GLfloat state_r = GL_FALSE;
        ctx->dispatcher().glGetTexGenfv(GL_TEXTURE_GEN_S,pname,&state_s);
        ctx->dispatcher().glGetTexGenfv(GL_TEXTURE_GEN_T,pname,&state_t);
        ctx->dispatcher().glGetTexGenfv(GL_TEXTURE_GEN_R,pname,&state_r);
        tmpParams[0] = state_s && state_t && state_r ? GL_TRUE: GL_FALSE;
    }
    else
        ctx->dispatcher().glGetTexGenfv(coord,pname,tmpParams);

    params[0] = F2X(tmpParams[0]);
}

template <class T, GLenum TypeName>
void glDrawTexOES (T x, T y, T z, T width, T height) {
    GET_CTX()

    SET_ERROR_IF((width<=0 || height<=0),GL_INVALID_VALUE);

    ctx->drawValidate();

    int numClipPlanes;

    GLint viewport[4];
    z = (z>1 ? 1 : (z<0 ?  0 : z));

    T     vertices[4*3] = {x , y, z,
                             x , y+height, z,
                             x+width, y+height, z,
                             x+width, y, z};
    GLfloat texels[ctx->getMaxTexUnits()][4*2];
    memset((void*)texels, 0, ctx->getMaxTexUnits()*4*2*sizeof(GLfloat));

    ctx->dispatcher().glPushClientAttrib(GL_CLIENT_VERTEX_ARRAY_BIT);
    ctx->dispatcher().glPushAttrib(GL_TRANSFORM_BIT);

    //setup projection matrix to draw in viewport aligned coordinates
    ctx->dispatcher().glMatrixMode(GL_PROJECTION);
    ctx->dispatcher().glPushMatrix();
    ctx->dispatcher().glLoadIdentity();
    ctx->dispatcher().glGetIntegerv(GL_VIEWPORT,viewport);
    ctx->dispatcher().glOrtho(viewport[0],viewport[0] + viewport[2],viewport[1],viewport[1]+viewport[3],0,-1);
    //setup texture matrix
    ctx->dispatcher().glMatrixMode(GL_TEXTURE);
    ctx->dispatcher().glPushMatrix();
    ctx->dispatcher().glLoadIdentity();
    //setup modelview matrix
    ctx->dispatcher().glMatrixMode(GL_MODELVIEW);
    ctx->dispatcher().glPushMatrix();
    ctx->dispatcher().glLoadIdentity();
    //backup vbo's
    int array_buffer,element_array_buffer;
    glGetIntegerv(GL_ARRAY_BUFFER_BINDING,&array_buffer);
    glGetIntegerv(GL_ELEMENT_ARRAY_BUFFER_BINDING,&element_array_buffer);
    ctx->dispatcher().glBindBuffer(GL_ARRAY_BUFFER,0);
    ctx->dispatcher().glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,0);

    //disable clip planes
    ctx->dispatcher().glGetIntegerv(GL_MAX_CLIP_PLANES,&numClipPlanes);
    for (int i=0;i<numClipPlanes;++i)
        ctx->dispatcher().glDisable(GL_CLIP_PLANE0+i);

    int nTexPtrs = 0;
    for (int i=0;i<ctx->getMaxTexUnits();++i) {
        if (ctx->isTextureUnitEnabled(GL_TEXTURE0+i)) {
            TextureData * texData = NULL;
            unsigned int texname = ctx->getBindedTexture(GL_TEXTURE0+i,GL_TEXTURE_2D);
            ObjectLocalName tex = TextureLocalName(GL_TEXTURE_2D,texname);
            ctx->dispatcher().glClientActiveTexture(GL_TEXTURE0+i);
            ObjectDataPtr objData = ctx->shareGroup()->getObjectData(TEXTURE,tex);
            if (objData.Ptr()) {
                texData = (TextureData*)objData.Ptr();
                //calculate texels
                texels[i][0] = (float)(texData->crop_rect[0])/(float)(texData->width);
                texels[i][1] = (float)(texData->crop_rect[1])/(float)(texData->height);

                texels[i][2] = (float)(texData->crop_rect[0])/(float)(texData->width);
                texels[i][3] = (float)(texData->crop_rect[3]+texData->crop_rect[1])/(float)(texData->height);

                texels[i][4] = (float)(texData->crop_rect[2]+texData->crop_rect[0])/(float)(texData->width);
                texels[i][5] = (float)(texData->crop_rect[3]+texData->crop_rect[1])/(float)(texData->height);

                texels[i][6] = (float)(texData->crop_rect[2]+texData->crop_rect[0])/(float)(texData->width);
                texels[i][7] = (float)(texData->crop_rect[1])/(float)(texData->height);

                ctx->dispatcher().glTexCoordPointer(2,GL_FLOAT,0,texels[i]);
                nTexPtrs++;
             }
        }
    }

    if (nTexPtrs>0) {
        //draw rectangle - only if we have some textures enabled & ready
        ctx->dispatcher().glEnableClientState(GL_VERTEX_ARRAY);
        ctx->dispatcher().glVertexPointer(3,TypeName,0,vertices);
        ctx->dispatcher().glEnableClientState(GL_TEXTURE_COORD_ARRAY);
        ctx->dispatcher().glDrawArrays(GL_TRIANGLE_FAN,0,4);
    }

    //restore vbo's
    ctx->dispatcher().glBindBuffer(GL_ARRAY_BUFFER,array_buffer);
    ctx->dispatcher().glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,element_array_buffer);

    //restore matrix state

    ctx->dispatcher().glMatrixMode(GL_MODELVIEW);
    ctx->dispatcher().glPopMatrix();
    ctx->dispatcher().glMatrixMode(GL_TEXTURE);
    ctx->dispatcher().glPopMatrix();
    ctx->dispatcher().glMatrixMode(GL_PROJECTION);
    ctx->dispatcher().glPopMatrix();

    ctx->dispatcher().glPopAttrib();
    ctx->dispatcher().glPopClientAttrib();
}

KHR_API(void, glDrawTexsOES)(GLshort x, GLshort y, GLshort z, GLshort width, GLshort height) {
    glDrawTexOES<GLshort,GL_SHORT>(x,y,z,width,height);
}

KHR_API(void, glDrawTexiOES)(GLint x, GLint y, GLint z, GLint width, GLint height) {
    glDrawTexOES<GLint,GL_INT>(x,y,z,width,height);
}

KHR_API(void, glDrawTexfOES)(GLfloat x, GLfloat y, GLfloat z, GLfloat width, GLfloat height) {
    glDrawTexOES<GLfloat,GL_FLOAT>(x,y,z,width,height);
}

KHR_API(void, glDrawTexxOES)(GLfixed x, GLfixed y, GLfixed z, GLfixed width, GLfixed height) {
    glDrawTexOES<GLfloat,GL_FLOAT>(X2F(x),X2F(y),X2F(z),X2F(width),X2F(height));
}

KHR_API(void, glDrawTexsvOES)(const GLshort * coords) {
    glDrawTexOES<GLshort,GL_SHORT>(coords[0],coords[1],coords[2],coords[3],coords[4]);
}

KHR_API(void, glDrawTexivOES)(const GLint * coords) {
    glDrawTexOES<GLint,GL_INT>(coords[0],coords[1],coords[2],coords[3],coords[4]);
}

KHR_API(void, glDrawTexfvOES)(const GLfloat * coords) {
    glDrawTexOES<GLfloat,GL_FLOAT>(coords[0],coords[1],coords[2],coords[3],coords[4]);
}

KHR_API(void, glDrawTexxvOES)(const GLfixed * coords) {
    glDrawTexOES<GLfloat,GL_FLOAT>(X2F(coords[0]),X2F(coords[1]),X2F(coords[2]),X2F(coords[3]),X2F(coords[4]));
}

/* GL addons */

KHR_API(void, glVertexPointerOffset)(GLint size, GLenum type, GLsizei stride, GLuint offset) {
    KHR_SYMBOL(glVertexPointer)(size, type, stride, (void *)offset);
}

KHR_API(void, glColorPointerOffset)(GLint size, GLenum type, GLsizei stride, GLuint offset) {
    KHR_SYMBOL(glColorPointer)(size, type, stride, (void *)offset);
}

KHR_API(void, glTexCoordPointerOffset)(GLint size, GLenum type, GLsizei stride, GLuint offset) {
    KHR_SYMBOL(glTexCoordPointer)(size, type, stride, (void *) offset);
}

KHR_API(void, glNormalPointerOffset)(GLenum type, GLsizei stride, GLuint offset) {
    KHR_SYMBOL(glNormalPointer)(type, stride, (void *)offset);
}

//KHR_API(void, glPointSizePointerOffset)(GLenum type, GLsizei stride, GLuint offset) {
//    KHR_SYMBOL(glPointSizePointerOES)(type, stride, (void *)offset);
//}

KHR_API(void, glWeightPointerOffset)(void * self, GLint size, GLenum type, GLsizei stride, GLuint offset) {
    KHR_SYMBOL(glWeightPointerOES)(size, type, stride, (void*)offset);
}

KHR_API(void, glMatrixIndexPointerOffset)(void * self, GLint size, GLenum type, GLsizei stride, GLuint offset) {
    KHR_SYMBOL(glMatrixIndexPointerOES)(size, type, stride, (void*)offset);
}

KHR_API(void, glVertexPointerData)(GLint size, GLenum type, GLsizei stride, void *data, GLuint datalen) {
    KHR_SYMBOL(glVertexPointer)(size, type, stride, data);
}

KHR_API(void, glColorPointerData)(GLint size, GLenum type, GLsizei stride, void *data, GLuint datalen) {
    KHR_SYMBOL(glColorPointer)(size, type, stride, data);
}

KHR_API(void, glTexCoordPointerData)(GLint unit, GLint size, GLenum type, GLsizei stride, void *data, GLuint datalen) {
    KHR_SYMBOL(glTexCoordPointer)(size, type, stride, data);
}

KHR_API(void, glNormalPointerData)(GLenum type, GLsizei stride, void *data, GLuint datalen) {
    KHR_SYMBOL(glNormalPointer)(type, stride, data);
}

//KHR_API(void, glPointSizePointerData)(GLenum type, GLsizei stride, void *data, GLuint datalen) {
//    KHR_SYMBOL(glPointSizePointerOES)(type, stride, data);
//}

KHR_API(void, glWeightPointerData)(void * self, GLint size, GLenum type, GLsizei stride, void * data, GLuint datalen) {
    KHR_SYMBOL(glWeightPointerOES)(size, type, stride, data);
}

KHR_API(void, glMatrixIndexPointerData)(void * self, GLint size, GLenum type, GLsizei stride, void * data, GLuint datalen) {
    KHR_SYMBOL(glMatrixIndexPointerOES)(size, type, stride, data);
}

KHR_API(void, glDrawElementsOffset)(GLenum mode, GLsizei count, GLenum type, GLuint offset) {
    KHR_SYMBOL(glDrawElements)(mode, count, type, (void *)offset);
}

KHR_API(void, glDrawElementsData)(GLenum mode, GLsizei count, GLenum type, void * data, GLuint datalen) {
    KHR_SYMBOL(glDrawElements)(mode, count, type, data);
}

/* GL_OES_vertex_array_object */

KHR_API(void, glBindVertexArrayOES)(GLuint array) {
    UNIMPLEMENTED();
}

KHR_API(void, glDeleteVertexArraysOES)(GLsizei n, const GLuint *arrays) {
    UNIMPLEMENTED();
}

KHR_API(void, glGenVertexArraysOES)(GLsizei n, GLuint *arrays) {
    UNIMPLEMENTED();
}

KHR_API(GLboolean, glIsVertexArrayOES)(GLuint array) {
    UNIMPLEMENTED(GL_FALSE);
}

/* GL_OES_mapbuffer */

KHR_API(void*, glMapBufferOES)(GLenum target, GLenum access) {
    UNIMPLEMENTED(NULL);
}

KHR_API(GLboolean, glUnmapBufferOES)(GLenum target) {
    UNIMPLEMENTED(GL_FALSE);
}

KHR_API(void, glGetBufferPointervOES)(GLenum target, GLenum pname, GLvoid ** params) {
    UNIMPLEMENTED();
}

/* GL_IMG_multisampled_render_to_texture */

KHR_API(void, glRenderbufferStorageMultisampleIMG)(GLenum target, GLsizei samples, GLenum internalformat, GLsizei width, GLsizei height) {
    UNIMPLEMENTED();
}

KHR_API(void, glFramebufferTexture2DMultisampleIMG)(GLenum target, GLenum attachment, GLenum textarget, GLuint texture, GLint level, GLsizei samples) {
    UNIMPLEMENTED();
}

/* GL_EXT_texture_storage */

KHR_API(void, glTexStorage1DEXT)(GLenum target, GLsizei levels, GLenum internalformat, GLsizei width) {
    UNIMPLEMENTED();
}

KHR_API(void, glTexStorage2DEXT)(GLenum target, GLsizei levels, GLenum internalformat, GLsizei width, GLsizei height) {
    UNIMPLEMENTED();
}

KHR_API(void, glTexStorage3DEXT)(GLenum target, GLsizei levels, GLenum internalformat, GLsizei width, GLsizei height, GLsizei depth) {
    UNIMPLEMENTED();
}

KHR_API(void, glTextureStorage1DEXT)(GLuint texture, GLenum target, GLsizei levels, GLenum internalformat, GLsizei width) {
    UNIMPLEMENTED();
}

KHR_API(void, glTextureStorage2DEXT)(GLuint texture, GLenum target, GLsizei levels, GLenum internalformat, GLsizei width, GLsizei height) {
    UNIMPLEMENTED();
}

KHR_API(void, glTextureStorage3DEXT)(GLuint texture, GLenum target, GLsizei levels, GLenum internalformat, GLsizei width, GLsizei height, GLsizei depth) {
    UNIMPLEMENTED();
}

/* GL_EXT_discard_framebuffer */

KHR_API(void, glDiscardFramebufferEXT)(GLenum target, GLsizei numAttachments, const GLenum *attachments) {
    UNIMPLEMENTED();
}

/* GL_EXT_multisampled_render_to_texture */

KHR_API(void, glRenderbufferStorageMultisampleEXT)(GLenum target, GLsizei samples, GLenum internalformat, GLsizei width, GLsizei height) {
    UNIMPLEMENTED();
}

KHR_API(void, glFramebufferTexture2DMultisampleEXT)(GLenum target, GLenum attachment, GLenum textarget, GLuint texture, GLint level, GLsizei samples) {
    UNIMPLEMENTED();
}

/**************************************************************************************************/

static void initContext(GLEScontext* ctx,ShareGroupPtr grp) {
    if (!ctx->isInitialized()) {
        ctx->setShareGroup(grp);
        ctx->init();
        ctx->dispatcher().glBindTexture(GL_TEXTURE_2D,0);
        ctx->dispatcher().glBindTexture(GL_TEXTURE_CUBE_MAP_OES,0);
    }
}

static GLEScontext* createGLESContext() {
    return new GLEScmContext();
}

static void deleteGLESContext(GLEScontext* ctx) {
    delete ctx;
}

static void setShareGroup(GLEScontext* ctx,ShareGroupPtr grp) {
    if (ctx) {
        ctx->setShareGroup(grp);
    }
}

static __translatorMustCastToProperFunctionPointerType getProcAddress(const char* procName) {
    GET_CTX_RET(NULL)

    __translatorMustCastToProperFunctionPointerType proc = NULL;

    #define COMPARE_PROC(ProcName, Proc) \
        if (!strcmp(procName, ProcName)) { \
            proc = (__translatorMustCastToProperFunctionPointerType)&Proc; \
            break; \
        }

    ctx->getGlobalLock();
    do {
        COMPARE_PROC("glEGLImageTargetTexture2DOES", glEGLImageTargetTexture2DOES);
        COMPARE_PROC("glEGLImageTargetRenderbufferStorageOES", glEGLImageTargetRenderbufferStorageOES);
        COMPARE_PROC("glBlendEquationSeparateOES", glBlendEquationSeparateOES);
        COMPARE_PROC("glBlendFuncSeparateOES", glBlendFuncSeparateOES);
        COMPARE_PROC("glBlendEquationOES", glBlendEquationOES);

       if (ctx->getCaps()->GL_ARB_MATRIX_PALETTE && ctx->getCaps()->GL_ARB_VERTEX_BLEND) {
           COMPARE_PROC("glCurrentPaletteMatrixOES", glCurrentPaletteMatrixOES);
           COMPARE_PROC("glLoadPaletteFromModelViewMatrixOES", glLoadPaletteFromModelViewMatrixOES);
           COMPARE_PROC("glMatrixIndexPointerOES", glMatrixIndexPointerOES);
           COMPARE_PROC("glWeightPointerOES", glWeightPointerOES);
       }

       COMPARE_PROC("glDepthRangefOES", glDepthRangef);
       COMPARE_PROC("glFrustumfOES", glFrustumf);
       COMPARE_PROC("glOrthofOES", glOrthof);
       COMPARE_PROC("glClipPlanefOES", glClipPlanef);
       COMPARE_PROC("glGetClipPlanefOES", glGetClipPlanef);
       COMPARE_PROC("glClearDepthfOES", glClearDepthf);
       COMPARE_PROC("glPointSizePointerOES", glPointSizePointerOES);
       COMPARE_PROC("glTexGenfOES", glTexGenfOES);
       COMPARE_PROC("glTexGenfvOES", glTexGenfvOES);
       COMPARE_PROC("glTexGeniOES", glTexGeniOES);
       COMPARE_PROC("glTexGenivOES", glTexGenivOES);
       COMPARE_PROC("glTexGenxOES", glTexGenxOES);
       COMPARE_PROC("glTexGenxvOES", glTexGenxvOES);
       COMPARE_PROC("glGetTexGenfvOES", glGetTexGenfvOES);
       COMPARE_PROC("glGetTexGenivOES", glGetTexGenivOES);
       COMPARE_PROC("glGetTexGenxvOES", glGetTexGenxvOES);

       if (ctx->getCaps()->GL_EXT_FRAMEBUFFER_OBJECT) {
           COMPARE_PROC("glIsRenderbufferOES", glIsRenderbufferOES);
           COMPARE_PROC("glBindRenderbufferOES", glBindRenderbufferOES);
           COMPARE_PROC("glDeleteRenderbuffersOES", glDeleteRenderbuffersOES);
           COMPARE_PROC("glGenRenderbuffersOES", glGenRenderbuffersOES);
           COMPARE_PROC("glRenderbufferStorageOES", glRenderbufferStorageOES);
           COMPARE_PROC("glGetRenderbufferParameterivOES", glGetRenderbufferParameterivOES);
           COMPARE_PROC("glIsFramebufferOES", glIsFramebufferOES);
           COMPARE_PROC("glBindFramebufferOES", glBindFramebufferOES);
           COMPARE_PROC("glDeleteFramebuffersOES", glDeleteFramebuffersOES);
           COMPARE_PROC("glGenFramebuffersOES", glGenFramebuffersOES);
           COMPARE_PROC("glCheckFramebufferStatusOES", glCheckFramebufferStatusOES);
           COMPARE_PROC("glFramebufferTexture2DOES", glFramebufferTexture2DOES);
           COMPARE_PROC("glFramebufferRenderbufferOES", glFramebufferRenderbufferOES);
           COMPARE_PROC("glGetFramebufferAttachmentParameterivOES", glGetFramebufferAttachmentParameterivOES);
           COMPARE_PROC("glGenerateMipmapOES", glGenerateMipmapOES);
       }

       COMPARE_PROC("glDrawTexsOES", glDrawTexsOES);
       COMPARE_PROC("glDrawTexiOES", glDrawTexiOES);
       COMPARE_PROC("glDrawTexfOES", glDrawTexfOES);
       COMPARE_PROC("glDrawTexxOES", glDrawTexxOES);
       COMPARE_PROC("glDrawTexsvOES", glDrawTexsvOES);
       COMPARE_PROC("glDrawTexivOES", glDrawTexivOES);
       COMPARE_PROC("glDrawTexfvOES", glDrawTexfvOES);
       COMPARE_PROC("glDrawTexxvOES", glDrawTexxvOES);
   } while(0);
   ctx->releaseGlobalLock();

   #undef COMPARE_PROC

   return proc;
}

/*
typedef struct {
    GLEScontext*                                    (*createGLESContext)();
    void                                            (*initContext)(GLEScontext*,ShareGroupPtr);
    void                                            (*deleteGLESContext)(GLEScontext*);
    void                                            (*flush)();
    void                                            (*finish)();
    void                                            (*setShareGroup)(GLEScontext*,ShareGroupPtr);
    __translatorMustCastToProperFunctionPointerType (*getProcAddress)(const char*);
}GLESiface;
*/

static void flush() {
    GLEScontext::dispatcher().glFlush();
}

static void finish() {
    GLEScontext::dispatcher().glFinish();
}

static GLESiface s_glesIface = {0};

extern "C" GLESiface* __translator_getIfaces(EGLiface* eglIface){
    s_eglIface = eglIface;

    s_glesIface.createGLESContext = &createGLESContext;
    s_glesIface.initContext = &initContext;
    s_glesIface.deleteGLESContext = &deleteGLESContext;
    s_glesIface.flush = &flush;
    s_glesIface.finish = &finish;
    s_glesIface.setShareGroup = &setShareGroup;
    s_glesIface.getProcAddress = &getProcAddress;

    return &s_glesIface;
}
