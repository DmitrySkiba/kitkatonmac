{
  'includes': [
    'common.gypi',
  ],

  'variables': {
    'rc_root': '<(platform_root)/opengl/rc',
    'local_root': '<(rc_root)/RenderControl',
  },

  'targets': [
    {
      'target_name': 'opengl-rc-RenderControl',
      'product_name': 'rc',
      'type': 'static_library',

      'dependencies': [
        '<!(<(dependency) frameworks-opengl)',
        '<!(<(dependency) opengl-rc-shared)',
        '<!(<(dependency) opengl-rc-utils)',
      ],

      'include_dirs': [
        '<(local_root)/include',
      ],

      'defines': [
        'HOST_GLES_PREFIX="<(host_gles_prefix)"',
      ],

      'link_settings': {
        'libraries': [
          '$(SDKROOT)/System/Library/Frameworks/Cocoa.framework',
          '$(SDKROOT)/System/Library/Frameworks/OpenGL.framework',
        ],
      },

      'sources': [
        '<(local_root)/ColorBuffer.cpp',
        '<(local_root)/EGLDispatch.cpp',
        '<(local_root)/FBConfig.cpp',
        '<(local_root)/FrameBuffer.cpp',
        '<(local_root)/GL2Dispatch.cpp',
        '<(local_root)/GLDispatch.cpp',
        '<(local_root)/NativeMacSubWindow.m',
        '<(local_root)/RenderContext.cpp',
        '<(local_root)/RenderContext.h',
        '<(local_root)/RenderControl.cpp',
        '<(local_root)/RenderControl.h',
        '<(local_root)/ThreadInfo.cpp',
        '<(local_root)/WindowSurface.cpp',
      ],
    },
  ],
}
