{
  'includes': [
    'common.gypi',
  ],

  'variables': {
    'local_root': '<(platform_root)/frameworks/native/libs/ui',
  },

  'targets': [
    {
      'target_name': 'frameworks-ui',
      'product_name': 'ui',
      'type': 'static_library',

      'dependencies': [
        '<!(<(dependency) frameworks-include)',
        '<!(<(dependency) frameworks-opengl)',
        '<!(<(dependency) system-liblog)',
        '<!(<(dependency) system-libutils)',
        '<!(<(dependency) system-libcutils)',
        '<!(<(dependency) system-libsync)',
        '<!(<(dependency) hardware-libhardware)',
      ],

      'export_dependent_settings': [
        '<!(<(dependency) frameworks-include)',
        '<!(<(dependency) hardware-libhardware)',
      ],

      'sources': [
        '<(local_root)/Fence.cpp',
        '<(local_root)/FramebufferNativeWindow.cpp',
        '<(local_root)/GraphicBuffer.cpp',
        '<(local_root)/GraphicBufferAllocator.cpp',
        '<(local_root)/GraphicBufferMapper.cpp',
        '<(local_root)/PixelFormat.cpp',
        '<(local_root)/Rect.cpp',
        '<(local_root)/Region.cpp',
        '<(local_root)/UiConfig.cpp',
      ],
    },
  ],
}
