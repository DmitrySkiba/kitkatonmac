/*
* Copyright (C) 2011 The Android Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#ifndef _RENDER_CONTROL_H
#define _RENDER_CONTROL_H

#include <GLES/gl.h>
#include <EGL/egl.h>
#include <stdint.h>

// values for 'param' argument of rcGetFBParam
#define FB_WIDTH    1
#define FB_HEIGHT   2
#define FB_XDPI     3
#define FB_YDPI     4
#define FB_FPS      5
#define FB_MIN_SWAP_INTERVAL 6
#define FB_MAX_SWAP_INTERVAL 7

#ifdef __cplusplus
extern "C" {
#endif

GLint rcGetRendererVersion(void);
EGLint rcGetEGLVersion(EGLint* major, EGLint* minor);
EGLint rcQueryEGLString(EGLenum name, void* buffer, EGLint bufferSize);
EGLint rcGetGLString(EGLenum name, void* buffer, EGLint bufferSize);
EGLint rcGetNumConfigs(uint32_t* numAttribs);
EGLint rcGetConfigs(uint32_t bufSize, GLuint* buffer);
EGLint rcChooseConfig(EGLint* attribs, uint32_t attribs_size, uint32_t* configs, uint32_t configs_size);
EGLint rcGetFBParam(EGLint param);
uint32_t rcCreateContext(uint32_t config, uint32_t share, uint32_t glVersion);
void rcDestroyContext(uint32_t context);
uint32_t rcCreateWindowSurface(uint32_t config, uint32_t width, uint32_t height);
void rcDestroyWindowSurface(uint32_t windowSurface);
uint32_t rcCreateColorBuffer(uint32_t width, uint32_t height, GLenum internalFormat);
void rcOpenColorBuffer(uint32_t colorbuffer);
void rcCloseColorBuffer(uint32_t colorbuffer);
void rcSetWindowColorBuffer(uint32_t windowSurface, uint32_t colorBuffer);
int rcFlushWindowColorBuffer(uint32_t windowSurface);
EGLint rcMakeCurrent(uint32_t context, uint32_t drawSurf, uint32_t readSurf);
void rcFBPost(uint32_t colorBuffer);
void rcFBSetSwapInterval(EGLint interval);
void rcBindTexture(uint32_t colorBuffer);
void rcBindRenderbuffer(uint32_t colorBuffer);
EGLint rcColorBufferCacheFlush(uint32_t colorbuffer, EGLint postCount, int forRead);
void rcReadColorBuffer(uint32_t colorbuffer, GLint x, GLint y, GLint width, GLint height, GLenum format, GLenum type, void* pixels);
int rcUpdateColorBuffer(uint32_t colorbuffer, GLint x, GLint y, GLint width, GLint height, GLenum format, GLenum type, void* pixels);

#ifdef __cplusplus
}
#endif

#endif // _RENDER_CONTROL_H
