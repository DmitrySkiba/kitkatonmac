/*
* Copyright (C) 2011 The Android Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
#include "GLDispatch.h"
#include <stdio.h>
#include <stdlib.h>
#include "osDynLibrary.h"
#include <memory>

GLDispatch s_gl;

bool init_gl_dispatch()
{
    std::unique_ptr<osUtils::dynLibrary> glLibrary(osUtils::dynLibrary::open(
        "/System/Library/Frameworks/OpenGL.framework/OpenGL"));

    std::unique_ptr<osUtils::dynLibrary> thisBinary(osUtils::dynLibrary::open(
        nullptr));
    thisBinary->setPrefix(HOST_GLES_PREFIX);

    auto findSymbol = [&] (const char* name) -> void* {
        osUtils::dynFuncPtr value = thisBinary->findSymbol(name);
        if (!value) {
            value = glLibrary->findSymbol(name);
        }
        return reinterpret_cast<void*>(value);
    };

    // s_gl.glAlphaFunc = (void(*)(GLenum func, GLclampf ref))findSymbol("glAlphaFunc");
    // ...
    #define GLES1_ENUMERATOR_F(ReturnType, Name, Arguments, ArgumentNames) \
        s_gl.Name = (ReturnType(*)Arguments)findSymbol(#Name);
    GLES1_ENUMERATE_FUNCTIONS(GLES1_ENUMERATOR_F)
    #undef GLES1_ENUMERATOR_F

    return true;
}
