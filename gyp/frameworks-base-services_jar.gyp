{
  'includes': [
    'common.gypi',
    'java-common.gypi',
  ],

  'variables': {
    'local_root': '<(platform_root)/frameworks/base/services',
    'java_root': '<(local_root)/java',
  },

  'targets': [
    {
      'target_name': 'frameworks-base-services_jar',
      'type': 'none',

      'dependencies': [
        '<!(<(dependency) libcore-rt_jar)',
        '<!(<(dependency) libcore-conscrypt_jar)',
        '<!(<(dependency) frameworks-android.policy_jar)',
        '<!(<(dependency) frameworks-base-framework_jar)',
        '<!(<(dependency) frameworks-opt-telephony_jar)',
        '<!(<(dependency) external-ext_jar)',
      ],

      'variables': {
        'jar_path': '<!(<(jar_path_v) services)',
        'dex_path': '<!(<(dex_path_v) services)',

        #logtags
        'logtags.inputs': [ '<(java_root)' ],

        # compile_jar
        'compile_jar.source_paths': [
          '<(logtags.output_path)',
          '<(java_root)',
        ],
        'compile_jar.boot_class_paths': '>(dependencies.jar_files)',
        'compile_jar.jar_file': '<(jar_path)',
        'compile_jar.extra_action_inputs': [
          '<(logtags.stamp_file)',
        ],

        # dex
        'dex.jar_file': '<(jar_path)',
        'dex.dex_file': '<(dex_path)',
      },
      'includes': [
        'utils/logtags.gypi',
        'utils/compile_jar.gypi',
        'utils/dex.gypi'
      ],
    }
  ]
}
