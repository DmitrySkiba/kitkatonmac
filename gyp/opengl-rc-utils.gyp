{
  'includes': [
    'common.gypi',
  ],

  'variables': {
    'rc_root': '<(platform_root)/opengl/rc',
    'local_root': '<(rc_root)/utils',
  },

  'targets': [
    {
      'target_name': 'opengl-rc-utils',
      'product_name': 'rc-utils',
      'type': 'static_library',

      'dependencies': [
        '<!(<(dependency) frameworks-opengl)',
        '<!(<(dependency) system-libcutils)',
      ],

      'export_dependent_settings': [
        '<!(<(dependency) system-libcutils)',
      ],

      'direct_dependent_settings': {
        'include_dirs': [
          '<(local_root)',
        ],
      },

      'sources': [
        '<(local_root)/osDynLibrary.cpp',
        '<(local_root)/TimeUtils.cpp',
        '<(local_root)/glUtils.cpp',
      ],
    },
  ],
}
