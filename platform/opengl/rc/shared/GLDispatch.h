/*
* Copyright (C) 2011 The Android Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
#ifndef _GLES_DISPATCH_H
#define _GLES_DISPATCH_H

#include "GLESFunctions.h"

struct GLDispatch {
    // void (*glAlphaFunc)(GLenum func, GLclampf ref);
    // ...
    #define GLES1_ENUMERATOR_F(ReturnType, Name, Arguments, ArgumentNames) \
        ReturnType (*Name) Arguments;
    GLES1_ENUMERATE_FUNCTIONS(GLES1_ENUMERATOR_F)
    #undef GLES1_ENUMERATOR_F
};

bool init_gl_dispatch();
extern GLDispatch s_gl;

#endif // _GLES_DISPATCH_H
