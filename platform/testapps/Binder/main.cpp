#define LOG_TAG "HelloWorld"

#include <binder/Parcel.h>
#include <binder/IInterface.h>
#include <binder/ProcessState.h>
#include <utils/String8.h>
#include <string>

using namespace android;

class IHelloWorld: public IInterface {
public:
    DECLARE_META_INTERFACE(HelloWorld)
    virtual void sayHello(const char* what) = 0;
    virtual sp<IHelloWorld> clone() = 0;
    virtual sp<IBinder> getToken() = 0;
    virtual bool compareToken(sp<IBinder> token) = 0;
    virtual sp<IBinder> getCleanup() = 0;
    virtual void callImpl(sp<IHelloWorld> self) = 0;
protected:
    enum {
        HW_SAYHELLO = IBinder::FIRST_CALL_TRANSACTION,
        HW_CLONE,
        HW_GETTOKEN,
        HW_COMPARETOKEN,
        HW_GETCLEANUP,
        HW_CALLIMPL,
    };
};

class BpHelloWorld : public BpInterface<IHelloWorld>
{
public:
    BpHelloWorld(const sp<IBinder>& remote): BpInterface(remote) {
    }

    void sayHello(const char* what) override {
        Parcel data;
        data.writeInterfaceToken(getInterfaceDescriptor());
        data.writeString8(String8(what));
        remote()->transact(HW_SAYHELLO, data, NULL, 0);
    }

    sp<IHelloWorld> clone() override {
        Parcel data, reply;
        data.writeInterfaceToken(getInterfaceDescriptor());
        status_t error = remote()->transact(HW_CLONE, data, &reply, 0);
        ALOG_ASSERT(error == NO_ERROR);
        return interface_cast<IHelloWorld>(reply.readStrongBinder());
    }

    sp<IBinder> getToken() override {
        Parcel data, reply;
        data.writeInterfaceToken(getInterfaceDescriptor());
        status_t error = remote()->transact(HW_GETTOKEN, data, &reply, 0);
        ALOG_ASSERT(error == NO_ERROR);
        return reply.readStrongBinder();
    }

    bool compareToken(sp<IBinder> token) override {
        Parcel data, reply;
        data.writeInterfaceToken(getInterfaceDescriptor());
        data.writeStrongBinder(token);
        status_t error = remote()->transact(HW_COMPARETOKEN, data, &reply, 0);
        ALOG_ASSERT(error == NO_ERROR);
        return reply.readInt32() != 0;
    }

    sp<IBinder> getCleanup() override {
        Parcel data, reply;
        data.writeInterfaceToken(getInterfaceDescriptor());
        status_t error = remote()->transact(HW_GETCLEANUP, data, &reply, 0);
        ALOG_ASSERT(error == NO_ERROR);
        return reply.readStrongBinder();
    }

    void callImpl(sp<IHelloWorld> hello) override {
        Parcel data;
        data.writeInterfaceToken(getInterfaceDescriptor());
        data.writeStrongBinder(hello->asBinder());
        status_t error = remote()->transact(HW_CALLIMPL, data, nullptr, 0);
        ALOG_ASSERT(error == NO_ERROR);
    }
};

IMPLEMENT_META_INTERFACE(HelloWorld, "test.HelloWorld");

class HelloWorldService : public BnInterface<IHelloWorld> {
public:
    HelloWorldService(): mStatus(STATUS_ALIVE) {
        ALOGD("%p HelloWorldService constructed", static_cast<IBinder*>(this));
    }
    ~HelloWorldService() override {
        mStatus = !STATUS_ALIVE;
        ALOGD("%p HelloWorldService destructed", static_cast<IBinder*>(this));
    }

    void sayHello(const char* what) override {
        ALOG_ASSERT(what);
        ALOGI("Hello %s", what);
        mHello = what;
    }

    static const std::string& getHello() {
        return mHello;
    }

    sp<IHelloWorld> clone() override {
        return new HelloWorldService();
    }

    sp<IBinder> getToken() override {
        if (mToken == nullptr) {
            mToken = new BBinder();
        }
        return mToken;
    }

    bool compareToken(sp<IBinder> token) override {
        ALOGW("Comparing %p to %p", mToken.get(), token.get());
        return mToken == token;
    }

    class Cleanup: public BBinder {
    public:
        Cleanup() {
            mHappened = false;
        }
        ~Cleanup() override {
            mHappened = true;
            ALOGW("Cleanup!");
        }

        static bool happened() {
            return mHappened;
        }

    private:
        static bool mHappened;
    };

    sp<IBinder> getCleanup() override {
        return new Cleanup();
    }

    void callImpl(sp<IHelloWorld> hello) override {
        sp<HelloWorldService> impl(static_cast<HelloWorldService*>(hello.get()));
        impl->checkAlive();
    }

private:

    status_t onTransact(uint32_t code, const Parcel& data, Parcel* reply, uint32_t flags) override {
        ALOGW("------ onTransact(%u) -----", code);

        CHECK_INTERFACE(IHelloWorld, data, reply);
        switch(code) {
            case HW_SAYHELLO: {
                sayHello(data.readString8().string());
                return NO_ERROR;
            }
            case HW_CLONE: {
                if (!reply) {
                    return BAD_VALUE;
                }
                reply->writeStrongBinder(clone()->asBinder());
                return NO_ERROR;
            }
            case HW_GETTOKEN: {
                if (!reply) {
                    return BAD_VALUE;
                }
                reply->writeStrongBinder(getToken());
                return NO_ERROR;
            }
            case HW_COMPARETOKEN: {
                if (!reply) {
                    return BAD_VALUE;
                }
                reply->writeInt32(compareToken(data.readStrongBinder()));
                return NO_ERROR;
            }
            case HW_GETCLEANUP: {
                if (!reply) {
                    return BAD_VALUE;
                }
                reply->writeStrongBinder(getCleanup());
                return NO_ERROR;
            }
            case HW_CALLIMPL: {
                sp<IHelloWorld> hello = interface_cast<IHelloWorld>(data.readStrongBinder());
                callImpl(hello);
                return NO_ERROR;
            }
        }
        return BBinder::onTransact(code, data, reply, flags);
    }

    void checkAlive() {
        ALOGW("%p is %s", this, (mStatus == STATUS_ALIVE ? "ALIVE" : "DEAD"));
        ALOG_ASSERT(mStatus == STATUS_ALIVE);
    }

private:
    sp<IBinder> mToken;
    int mStatus;
    static std::string mHello;

    const static int STATUS_ALIVE = 0x1DEAD;
};

std::string HelloWorldService::mHello;
bool HelloWorldService::Cleanup::mHappened = false;

// =================================================================================================

static void* tgid_checker_thread(void* param) {
    pthread_tgid_t expected_tgid = (pthread_tgid_t)param;
    assert(pthread_get_tgid_np() == expected_tgid);
    ALOGI("TGID: thread %d got group %d", gettid(), expected_tgid);
    return nullptr;
}

static void* tgid_spawner_thread(void* param) {
    tgid_checker_thread(param);

    ALOGI("TGID: thread %d spawns a thread", gettid());

    pthread_t thread;
    pthread_create(&thread, nullptr, &tgid_checker_thread, (void*)pthread_get_tgid_np());
    pthread_detach(thread);
    return nullptr;

}

static void test_tgid(void) {
    assert(pthread_get_tgid_np() == PTHREAD_ROOT_TGID);

    for (int i = 0; i != 5; ++i) {
        pthread_t thread;
        pthread_create(&thread, nullptr, &tgid_spawner_thread, (void*)(i + 1));
        pthread_detach(thread);
    }

    sleep(1);
}

// =================================================================================================

#include <binder/TextOutput.h>
#include <utils/Log.h>
#include <private/binder/binder_module.h>
#include <utils/KeyedVector.h>
#include <binder/IPCThreadState.h>
#include <binder/IServiceManager.h>

#define SEPARATOR "========================================================="

static void tests() {

//    sp<IBinder> proxy;
//    {
//        Parcel parcel;
//
//        {
//            String8 name = String8("helloworld");
//            sp<IBinder> service = new HelloWorldService();
//
//            parcel.writeString8(name);
//            parcel.writeStrongBinder(service);
//        }
//
//        parcel.print(alog);
//
//        Parcel fixedParcel;
//        IPCThreadState::translateObjects(parcel, fixedParcel);
//
//        fixedParcel.setDataPosition(0);
//        {
//
//            String8 name = fixedParcel.readString8();
//            ALOGW("Name: %s", name.string());
//
//            proxy = fixedParcel.readStrongBinder();
//            ALOGW("Proxy: %p", proxy.get());
//        }
//
//        sp<IHelloWorld> hello = interface_cast<IHelloWorld>(proxy);
//        hello->sayHello("Woohoo!");
//    }

#if 0
    ALOGW(SEPARATOR);

    {
        sp<IBinder> binder = ProcessState::self()->convert(new BBinder());
    }

    ALOGW(SEPARATOR);

    return;
#endif

#if 0
    ALOGW(SEPARATOR);

    {
        sp<BBinder> bbinder = new BBinder();
    }

    ALOGW(SEPARATOR);

    {
        sp<BBinder> bbinder = new BBinder();
        bbinder->asIBinder();
    }

    ALOGW(SEPARATOR);

    {
        sp<BBinder> bbinder = new BBinder();
        {
            sp<IBinder> ibinder = bbinder->asIBinder();
        }
        ALOGW("{block finished}");
    }

    ALOGW(SEPARATOR);

    {
        sp<IBinder> ibinder;
        {
            sp<BBinder> bbinder = new BBinder();
            ibinder = bbinder->asIBinder();
        }

        ALOGW("{block finished}");

        ibinder = nullptr;
    }

    ALOGW(SEPARATOR);

    return;

#endif

    ALOGW(SEPARATOR);

    sp<HelloWorldService> helloImpl = new HelloWorldService();
    defaultServiceManager()->addService(String16("helloworld"), helloImpl);

    ALOGW(SEPARATOR);

    sp<IHelloWorld> hello;
    {
        status_t error = getService(String16("helloworld"), &hello);
        ALOG_ASSERT(error == NO_ERROR);
        ALOG_ASSERT(helloImpl == hello);
    }

    hello->sayHello("Service!");
    ALOG_ASSERT(HelloWorldService::getHello() == "Service!");

    ALOGW(SEPARATOR);

    hello->callImpl(hello);

    ALOGW(SEPARATOR);

    {
        sp<IBinder> token = hello->getToken();

        bool sameToken = hello->compareToken(token);
        ALOGW("Same token? %s", sameToken ? "YES" : "NO");
        ALOG_ASSERT(sameToken);
    }

    ALOGW(SEPARATOR);

    {
        ALOGW("Expecting cleanup...");
        sp<IBinder> cleanup = hello->getCleanup();
        ALOG_ASSERT(!HelloWorldService::Cleanup::happened());
    }
    ALOG_ASSERT(HelloWorldService::Cleanup::happened());

    ALOGW(SEPARATOR);

    for (size_t i = 0; i != 2; ++i)
    {
        sp<IHelloWorld> helloClone = hello->clone();
        helloClone->sayHello("Clone!");
        ALOG_ASSERT(HelloWorldService::getHello() == "Clone!");

        Parcel parcel, translatedParcel;
        parcel.writeStrongBinder(helloClone->asBinder());
        IPCThreadState::translateObjects(parcel, translatedParcel);
        sp<IHelloWorld> helloCloneProxy = interface_cast<IHelloWorld>(translatedParcel.readStrongBinder());
        helloCloneProxy->sayHello("Clone Proxy!");
        ALOG_ASSERT(HelloWorldService::getHello() == "Clone Proxy!");
    }

    ALOGW(SEPARATOR);

    ALOGI("Done");
}

int main() {
    //test_tgid();

    pthread_t thread;
    pthread_create(&thread, nullptr, [](void*) -> void* {
        tests();
        return nullptr;
    }, nullptr);
    pthread_join(thread, nullptr);

    return 0;
}
