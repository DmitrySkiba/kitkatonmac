/*
 * Copyright (C) 2015 Dmitry Skiba
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <pthread.h>
#include <errno.h>
#include <stdbool.h>
#include <assert.h>
#include <stdlib.h>
#include "real_functions.h"
#include "utils.h"

static pthread_key_t tgidv_key;
static pthread_mutex_t last_tgid_lock = PTHREAD_MUTEX_INITIALIZER;
static pthread_tgid_t last_tgid = PTHREAD_ROOT_TGID;

typedef struct tgid_value {
    pthread_tgid_t value;
    bool was_read;
} tgid_value;

typedef struct thread_run_info {
    tgid_value* tgidv;
    void *(*start_routine)(void*);
    void* arg;
} thread_run_info;

static tgid_value* get_tgidv(void) {
    return (tgid_value*)pthread_getspecific(tgidv_key);
}

static void set_tgidv(tgid_value* tgidv) {
    assert(!get_tgidv());
    pthread_setspecific(tgidv_key, tgidv);
}

static tgid_value* allocate_tgidv(pthread_tgid_t tgid) {
    tgid_value* tgidv = (tgid_value*)malloc(sizeof(tgid_value));
    if (tgidv) {
        tgidv->value = tgid;
        tgidv->was_read = false;
    }
    return tgidv;
}

static void set_tgid(pthread_tgid_t tgid) {
    tgid_value* tgidv = get_tgidv();
    if (tgidv) {
        assert(!tgidv->was_read);
        tgidv->value = tgid;
    } else {
        tgidv = allocate_tgidv(tgid);
        set_tgidv(tgidv);
    }
}

static pthread_tgid_t next_tgid(void) {
    pthread_mutex_lock(&last_tgid_lock);
    pthread_tgid_t tgid = (last_tgid += 1);
    pthread_mutex_unlock(&last_tgid_lock);
    return tgid;
}

static void* thread_run(void* arg) {
    thread_run_info info = *(thread_run_info*)arg;
    free(arg);

    set_tgidv(info.tgidv);
    return info.start_routine(info.arg);
}

static void init_root_tgid(void) __attribute__((constructor));
static void init_root_tgid(void) {
    pthread_set_root_tgid_np();
}

DEFINE_ONCE_INIT_FUNCTION(static void init_pthread(), {
    init_real_functions();

    int error = pthread_key_create(&tgidv_key, free);
    assert(!error);
})

/* API */

int pthread_getcpuclockid(pthread_t thread, clockid_t *clock_id) {
    return ENOENT;
}

void pthread_set_root_tgid_np(void) {
    init_pthread();

    set_tgid(PTHREAD_ROOT_TGID);
}

void pthread_set_next_tgid_np(void) {
    init_pthread();

    set_tgid(next_tgid());
}

pthread_tgid_t pthread_get_tgid_np(void) {
    init_pthread();

    tgid_value* tgidv = get_tgidv();
    assert(tgidv);
    if (tgidv) {
        tgidv->was_read = true;
        return tgidv->value;
    } else {
        return PTHREAD_ROOT_TGID;
    }
}

int pthread_create(pthread_t* thread, pthread_attr_t const* attr,
                   void *(*start_routine)(void*), void* arg) {
    init_pthread();

    thread_run_info* info = (thread_run_info*)malloc(sizeof(thread_run_info));
    if (!info) {
        return ENOMEM;
    }

    pthread_tgid_t tgid = pthread_get_tgid_np();
    if (tgid == PTHREAD_ROOT_TGID) {
        tgid = next_tgid();
    }
    info->tgidv = allocate_tgidv(tgid);
    if (!info->tgidv) {
        free(info);
        return ENOMEM;
    }

    info->start_routine = start_routine;
    info->arg = arg;

    int error = real_pthread_create(thread, attr, thread_run, info);
    if (error) {
        free(info->tgidv);
        free(info);
    }
    return error;
}
