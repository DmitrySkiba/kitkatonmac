{
  'includes': [
    'common.gypi',
  ],

  'variables': {
    'local_root': '<(platform_root)/frameworks/native/libs/binder',
  },

  'targets': [
    {
      'target_name': 'frameworks-binder',
      'product_name': 'binder',
      'type': 'static_library',

      'dependencies': [
        '<!(<(dependency) frameworks-include)',
        '<!(<(dependency) system-liblog)',
        '<!(<(dependency) system-libutils)',
        '<!(<(dependency) system-libcutils)',
      ],

      'export_dependent_settings': [
        '<!(<(dependency) frameworks-include)',
        '<!(<(dependency) system-libcutils)',
        '<!(<(dependency) system-libutils)',
      ],

      'sources': [
        '<(local_root)/AppOpsManager.cpp',
        '<(local_root)/Binder.cpp',
        '<(local_root)/BpBinder.cpp',
        '<(local_root)/BufferedTextOutput.cpp',
        '<(local_root)/Debug.cpp',
        '<(local_root)/IAppOpsCallback.cpp',
        '<(local_root)/IAppOpsService.cpp',
        '<(local_root)/IInterface.cpp',
        '<(local_root)/IMemory.cpp',
        '<(local_root)/IPCThreadState.cpp',
        '<(local_root)/IPermissionController.cpp',
        '<(local_root)/IServiceManager.cpp',
        '<(local_root)/MemoryDealer.cpp',
        '<(local_root)/MemoryBase.cpp',
        '<(local_root)/MemoryHeapBase.cpp',
        '<(local_root)/Parcel.cpp',
        '<(local_root)/PermissionCache.cpp',
        '<(local_root)/ProcessState.cpp',
        '<(local_root)/Static.cpp',
        '<(local_root)/TextOutput.cpp',
      ],
    },
  ],
}
