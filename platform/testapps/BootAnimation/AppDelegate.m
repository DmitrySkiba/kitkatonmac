#import "AppDelegate.h"

extern void initFrameBuffer(void* window, int width, int height);
extern int init_surfaceflinger(void);
extern void start_bootanimation(void);

@implementation AppDelegate

@synthesize window = _window;

- (void)applicationDidFinishLaunching:(NSNotification *)notification
{
    NSSize size = self.window.frame.size;
    initFrameBuffer(self.window, (int)size.width, (int)size.height);
    init_surfaceflinger();

    NSString* androidRoot = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"android_root"];
    setenv("ANDROID_ROOT", [androidRoot UTF8String], 1);

    setenv("PROP_ro.sf.lcd_density", "240", 1);

    start_bootanimation();
}

- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)sender
{
    return YES;
}

@end
