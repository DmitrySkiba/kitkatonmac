#!/usr/bin/env python

import sys
import os
import optparse

script_path = os.path.dirname(os.path.realpath(__file__))

sys.path.append(os.path.join(script_path, 'java'))
import build_utils


def main(arguments):
  parser = optparse.OptionParser()
  build_utils.add_required_option(parser, '--llvm-path')
  build_utils.add_required_option(parser, '--build-path')
  build_utils.add_required_option(parser, '--build-type')

  options, _ = parser.parse_args(arguments)
  build_utils.check_required_options(options, parser)

  llvm_path = os.path.abspath(options.llvm_path)
  build_path = os.path.abspath(options.build_path)
  build_type = options.build_type

  stamp_file = os.path.join(build_path, '.stamp')

  if os.path.exists(stamp_file):
    print 'Nothing to do.'
    return


  build_utils.delete_directory(build_path)
  os.makedirs(build_path)

  build_utils.check_call_die([
    'cmake',
    '-G', 'Ninja',
    '-DLLVM_BUILD_32_BITS=ON',
    '-DLLVM_ENABLE_CXX11=ON',
    '-DLLVM_ENABLE_LIBCXX=ON',
    '-DLLVM_REQUIRES_RTTI=ON',
    '-DLLVM_TARGETS_TO_BUILD=ARM;X86;Mips',
    '-DCMAKE_BUILD_TYPE=' + build_type,
    llvm_path,
  ], cwd = build_path)

  os.chdir(build_path)
  build_utils.check_call_die(['ninja'])

  build_utils.touch(stamp_file)


if __name__ == '__main__':
  main(sys.argv)
