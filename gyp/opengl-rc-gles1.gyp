{
  'includes': [
    'common.gypi',
  ],

  'variables': {
    'rc_root': '<(platform_root)/opengl/rc',
    'local_root': '<(rc_root)/gles1',
  },

  'targets': [
    {
      'target_name': 'opengl-rc-gles1',
      'product_name': 'rc-gles1',
      'type': 'static_library',

      'dependencies': [
        '<!(<(dependency) host-gles)',
        '<!(<(dependency) system-libutils)',
        '<!(<(dependency) system-liblog)',
        '<!(<(dependency) frameworks-include)',
        '<!(<(dependency) frameworks-opengl)',
        '<!(<(dependency) opengl-rc-shared)',
        '<!(<(dependency) opengl-rc-utils)',

        'hardware-libhardware.gyp:hardware-libhardware-common', #TODO move out
      ],

      'defines': [
        'GL_GLEXT_PROTOTYPES',
        'LOG_TAG="GLES_emulation"',
      ],

      'sources': [
        '<(local_root)/GLEncoder.cpp',
        '<(local_root)/GLClientState.cpp',
        '<(local_root)/GLSharedGroup.cpp',
      ],
    },
  ],
}
