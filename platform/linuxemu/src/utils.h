#include <pthread.h>

#define JOIN_TOKENS(a, b) DO_JOIN_TOKENS(a, b)
#define DO_JOIN_TOKENS(a, b) a ## b

#define DEFINE_ONCE_INIT_FUNCTION(declaration, code...) \
    static pthread_once_t JOIN_TOKENS(initialize_once_, __LINE__) = PTHREAD_ONCE_INIT; \
    static void JOIN_TOKENS(do_initialization, __LINE__)(void) { \
        code \
    } \
    declaration { \
        pthread_once(&JOIN_TOKENS(initialize_once_, __LINE__), \
            JOIN_TOKENS(do_initialization, __LINE__)); \
    }
