#!/bin/sh

platform_root=/Volumes/E/NewWork/KitKat/platform
out_root=/Volumes/E/NewWork/KitKat/out/manual

if [ -f "$out_root/framework.jar" ]; then
    echo "Already exists: $out_root/framework.jar"
else
    ./gyp/java/compile_jar.py \
        --bootclasspath="
            out/host/jars/rt.jar
            out/host/jars/conscrypt.jar
            out/host/jars/ext.jar
            out/host/jars/okhttp.jar
            out/host/jars/junit.jar
            out/host/jars/bouncycastle.jar
        " \
        --sources="
            platform/frameworks/base/core/java
            platform/frameworks/base/graphics/java
            platform/frameworks/base/location/java
            platform/frameworks/base/media/java
            platform/frameworks/base/media/mca/effect/java
            platform/frameworks/base/media/mca/filterfw/java
            platform/frameworks/base/media/mca/filterpacks/java
            platform/frameworks/base/drm/java
            platform/frameworks/base/opengl/java
            platform/frameworks/base/sax/java
            platform/frameworks/base/telephony/java
            platform/frameworks/base/wifi/java
            platform/frameworks/base/keystore/java

            $out_root/aidl
            $out_root/logtags

            $out_root/aapt/android/R.java
            $out_root/aapt/android/Manifest.java
            $out_root/aapt/com/android/internal/R.java
        " \
        --source-exclude-filters="
            */android/net/arp/ArpPeer.java
        " \
        --jar-file=$out_root/framework.jar \
        --jar-include-filters="
            android/*
        " \
        --intermediate-path=$out_root/framework \
        --stamp-file=$out_root/framework/.stamp \

fi

if [ -f "$out_root/framework2.jar" ]; then
    echo "Already exists: $out_root/framework2.jar"
else
    ./gyp/java/compile_jar.py \
        --bootclasspath="
            out/host/jars/rt.jar
            out/host/jars/conscrypt.jar
            out/host/jars/ext.jar
            out/host/jars/okhttp.jar
            out/host/jars/junit.jar
            out/host/jars/bouncycastle.jar
        " \
        --sources="
            platform/frameworks/base/core/java
            platform/frameworks/base/graphics/java
            platform/frameworks/base/location/java
            platform/frameworks/base/media/java
            platform/frameworks/base/media/mca/effect/java
            platform/frameworks/base/media/mca/filterfw/java
            platform/frameworks/base/media/mca/filterpacks/java
            platform/frameworks/base/drm/java
            platform/frameworks/base/opengl/java
            platform/frameworks/base/sax/java
            platform/frameworks/base/telephony/java
            platform/frameworks/base/wifi/java
            platform/frameworks/base/keystore/java

            $out_root/aidl
            $out_root/logtags

            $out_root/aapt/android/R.java
            $out_root/aapt/android/Manifest.java
            $out_root/aapt/com/android/internal/R.java
        " \
        --source-exclude-filters="
            */android/net/arp/ArpPeer.java
        " \
        --jar-file=$out_root/framework2.jar \
        --jar-include-filters="
            com/*
            javax/*
        " \
        --intermediate-path=$out_root/framework2 \
        --stamp-file=$out_root/framework2/.stamp \

fi
