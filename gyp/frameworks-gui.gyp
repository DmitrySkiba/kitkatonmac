{
  'includes': [
    'common.gypi',
  ],

  'variables': {
    'local_root': '<(platform_root)/frameworks/native/libs/gui',
  },

  'targets': [
    {
      'target_name': 'frameworks-gui',
      'product_name': 'gui',
      'type': 'static_library',

      'dependencies': [
        '<!(<(dependency) frameworks-include)',
        '<!(<(dependency) frameworks-opengl)',
        '<!(<(dependency) frameworks-binder)',
        '<!(<(dependency) frameworks-ui)',
        '<!(<(dependency) system-liblog)',
        '<!(<(dependency) system-libutils)',
        '<!(<(dependency) system-libcutils)',
        '<!(<(dependency) system-libsync)',
      ],

      'sources': [
        '<(local_root)/IGraphicBufferConsumer.cpp',
        '<(local_root)/IConsumerListener.cpp',
        '<(local_root)/BitTube.cpp',
        '<(local_root)/BufferItemConsumer.cpp',
        '<(local_root)/BufferQueue.cpp',
        '<(local_root)/ConsumerBase.cpp',
        '<(local_root)/CpuConsumer.cpp',
        '<(local_root)/DisplayEventReceiver.cpp',
        '<(local_root)/GLConsumer.cpp',
        '<(local_root)/GraphicBufferAlloc.cpp',
        '<(local_root)/GuiConfig.cpp',
        '<(local_root)/IDisplayEventConnection.cpp',
        '<(local_root)/IGraphicBufferAlloc.cpp',
        '<(local_root)/IGraphicBufferProducer.cpp',
        '<(local_root)/ISensorEventConnection.cpp',
        '<(local_root)/ISensorServer.cpp',
        '<(local_root)/ISurfaceComposer.cpp',
        '<(local_root)/ISurfaceComposerClient.cpp',
        '<(local_root)/LayerState.cpp',
        '<(local_root)/Sensor.cpp',
        '<(local_root)/SensorEventQueue.cpp',
        '<(local_root)/SensorManager.cpp',
        '<(local_root)/Surface.cpp',
        '<(local_root)/SurfaceControl.cpp',
        '<(local_root)/SurfaceComposerClient.cpp',
        '<(local_root)/SyncFeatures.cpp',
      ],
    },
  ],
}
