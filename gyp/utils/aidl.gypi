# Arguments:
# aidl.inputs               REQUIRED, array
# aidl.output_path          REQUIRED
# aidl.includes             array
# aidl.fail_on_parcelables  0/1
# aidl.stamp_file

{
  'variables': {
    'my_path': '.',
    'aidl.includes%': [],
    'aidl.fail_on_parcelables%': 0,
    'aidl.stamp_file%': '',
  },

  'actions': [
    {
      'variables': {
        'invocation': [
            'python', '<(my_path)/aidl.py',
            '--aidl-tool=<(BUILD_ANDROID_AIDL)',
            '--inputs=<(aidl.inputs)',
            '--output-path=<(aidl.output_path)',
        ],
        'conditions': [
          [ '<!(<(len) <(aidl.includes)) > 0', {
            'invocation+': [ '--includes', '<(aidl.includes)' ],
          }],
          [ '"<(aidl.stamp_file)" != ""', {
            'invocation+': [ '--stamp-file', '<(aidl.stamp_file)' ],
          }],
          [ '<(aidl.fail_on_parcelables) == 1', {
            'invocation+': [ '--fail-on-parcelables' ],
          }],
        ]
      },

      'action_name': 'aidl-<(_target_name)',
      'message': 'Processing .aidl files...',

      'inputs': [ '<!@(<(invocation) --print-gyp-inputs)' ],
      'outputs': [ '<!@(<(invocation) --print-gyp-outputs)' ],

      'action': [ '<@(invocation)' ],
    },
  ],
}
