{
  'includes': [
    'common.gypi',
    'java-common.gypi',
  ],

  'variables': {
    'local_root': '<(platform_root)/external/okhttp',
  },

  'targets': [
    {
      'target_name': 'external-okhttp_jar',
      'type': 'none',

      'dependencies': [
        '<!(<(dependency) libcore-rt_jar)',
        '<!(<(dependency) libcore-conscrypt_jar)',
      ],

      'variables': {
        'jar_path': '<!(<(jar_path_v) okhttp)',
        'dex_path': '<!(<(dex_path_v) okhttp)',

        'compile_jar.source_paths': [
          '<(local_root)/src/main/java',
          '<(local_root)/android/main/java',
        ],
        'compile_jar.source_exclude_filters': [
          '*/src/main/java/com/squareup/okhttp/internal/Platform.java',
        ],
        'compile_jar.boot_class_paths': '>(dependencies.jar_files)',
        'compile_jar.jar_file': '<(jar_path)',
        'compile_jar.jarjar_tool': '<(jarjar_tool)',
        'compile_jar.jarjar_rules_file': '<(local_root)/jarjar-rules.txt',

        'dex.jar_file': '<(jar_path)',
        'dex.dex_file': '<(dex_path)',
      },
      'includes': [ 'utils/compile_jar.gypi', 'utils/dex.gypi' ],
    }
  ]
}
