#ifndef _EVENT_HUB_OSX_H
#define _EVENT_HUB_OSX_H

#include "EventHub.h"

namespace android {

class EventHub : public EventHubInterface
{
public:
    EventHub();
    ~EventHub() override;

    uint32_t getDeviceClasses(int32_t deviceId) const override;

    InputDeviceIdentifier getDeviceIdentifier(int32_t deviceId) const override;

    int32_t getDeviceControllerNumber(int32_t deviceId) const override;

    void getConfiguration(int32_t deviceId, PropertyMap* outConfiguration) const override;

    status_t getAbsoluteAxisInfo(int32_t deviceId, int axis,
            RawAbsoluteAxisInfo* outAxisInfo) const override;

    bool hasRelativeAxis(int32_t deviceId, int axis) const override;

    bool hasInputProperty(int32_t deviceId, int property) const override;

    status_t mapKey(int32_t deviceId, int32_t scanCode, int32_t usageCode,
            int32_t* outKeycode, uint32_t* outFlags) const override;

    status_t mapAxis(int32_t deviceId, int32_t scanCode,
            AxisInfo* outAxisInfo) const override;

    void setExcludedDevices(const Vector<String8>& devices) override;

    int32_t getScanCodeState(int32_t deviceId, int32_t scanCode) const override;
    int32_t getKeyCodeState(int32_t deviceId, int32_t keyCode) const override;
    int32_t getSwitchState(int32_t deviceId, int32_t sw) const override;
    status_t getAbsoluteAxisValue(int32_t deviceId, int32_t axis, int32_t* outValue) const override;

    bool markSupportedKeyCodes(int32_t deviceId, size_t numCodes,
            const int32_t* keyCodes, uint8_t* outFlags) const override;

    size_t getEvents(int timeoutMillis, RawEvent* buffer, size_t bufferSize) override;

    bool hasScanCode(int32_t deviceId, int32_t scanCode) const override;
    bool hasLed(int32_t deviceId, int32_t led) const override;
    void setLedState(int32_t deviceId, int32_t led, bool on) override;

    void getVirtualKeyDefinitions(int32_t deviceId,
            Vector<VirtualKeyDefinition>& outVirtualKeys) const override;

    sp<KeyCharacterMap> getKeyCharacterMap(int32_t deviceId) const override;
    bool setKeyboardLayoutOverlay(int32_t deviceId, const sp<KeyCharacterMap>& map) override;

    void vibrate(int32_t deviceId, nsecs_t duration) override;
    void cancelVibrate(int32_t deviceId) override;

    void requestReopenDevices() override;

    void wake() override;

    void dump(String8& dump) override;
    void monitor() override;
};

}; // namespace android

#endif // _EVENT_HUB_OSX_H
