{
  'includes': [
    'common.gypi',
    'testapps-common.gypi',
  ],

  'variables': {
    'local_root': '<(platform_root)/testapps/SanAngeles',
  },

  'targets': [
    {
      'target_name': 'testapps-SanAngeles',
      'product_name': 'SanAngeles',
      'type': 'executable',

      'dependencies': [
        '<!(<(dependency) frameworks-opengl)',
        '<!(<(dependency) frameworks-surfaceflinger)',
      ],

      'xcode_settings': {
        'INFOPLIST_FILE': '<(local_root)/Info.plist',
      },

      'mac_bundle_resources': [
        '<(local_root)/Window.xib',
      ],

      'sources': [
        '<(local_root)/src/demo.c',
        '<(local_root)/src/app-linux.cpp',
        '<(local_root)/main.m',
        '<(local_root)/AppDelegate.m',
      ],
    },
  ],
}
