{
  'includes': [
    'common.gypi',
    'java-common.gypi',
  ],

  'variables': {
    'local_root': '<(platform_root)/external/junit',
  },

  'targets': [
    {
      'target_name': 'external-junit_jar',
      'type': 'none',

      'dependencies': [
        '<!(<(dependency) libcore-rt_jar)',
      ],

      'variables': {
        'jar_path': '<!(<(jar_path_v) junit)',
        'dex_path': '<!(<(dex_path_v) junit)',

        'compile_jar.source_paths': [
          '<(local_root)/src/junit/framework/Assert.java',
          '<(local_root)/src/junit/framework/AssertionFailedError.java',
          '<(local_root)/src/junit/framework/ComparisonCompactor.java',
          '<(local_root)/src/junit/framework/ComparisonFailure.java',
          '<(local_root)/src/junit/framework/Protectable.java',
          '<(local_root)/src/junit/framework/Test.java',
          '<(local_root)/src/junit/framework/TestCase.java',
          '<(local_root)/src/junit/framework/TestFailure.java',
          '<(local_root)/src/junit/framework/TestListener.java',
          '<(local_root)/src/junit/framework/TestResult.java',
          '<(local_root)/src/junit/framework/TestSuite.java',
          '<(local_root)/src/junit/extensions',
        ],
        'compile_jar.boot_class_paths': '>(dependencies.jar_files)',
        'compile_jar.jar_file': '<(jar_path)',

        'dex.jar_file': '<(jar_path)',
        'dex.dex_file': '<(dex_path)',
      },
      'includes': [ 'utils/compile_jar.gypi', 'utils/dex.gypi' ],
    }
  ]
}
